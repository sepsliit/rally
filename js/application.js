$(document).ready(function () {
    // alert('hi')
    $('.plus-button').click(function () {
////       alert('hi')
////        alert($(this).siblings().first().html())
//        $(this).siblings().first().next().delay(100).slideToggle(300);
        $(this).siblings().first().delay(100).slideToggle(300);
//        $(".timeline-item").delay(100).slideToggle(600);
    })

//-----------alert---------------------------------------------------------
    $('#successUser-alert').hide();
    $('#esuccessUser-alert').hide();
//-----------end of alert--------------------------------------------------


//-----------data_table-----------------------------------------------------
    user_table = $('#user-table').dataTable();
    defect_table = $('#defect-table').dataTable();
//-----------end of data_table----------------------------------------------


//------------------------register------------------------------------------

//-----------insert------------------------------
    $('#register').click(function () {

        dd = 1;
        $(".form-group").each(function () {
            if ($(this).hasClass('has-error')) {

                dd = 0;

                return false;
            } else {
                dd = 1
            }
        })

        if ($('#pass').val() != "" && $('#office').val() != "" && $('#dept').val() != "" && $('#phone').val() != "" && $('#email').val() != "" && $('#fname').val() && $('#title').val() != "" && dd == 1) {

            $.ajax({
                type: "POST",
                url: "index.php?r=user/regSave",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    fname: $('#fname').val(),
                    email: $('#email').val(),
                    phone: $('#phone').val(),
                    dept: $('#dept').val(),
                    office: $('#office').val(),
                    pass: $('#pass').val(),
                    title: $('#title').val()


                },
                dataType: "json",
                // contentType: "application/json; charset=utf-8",

                success: function (data) {

                    if (data == 'success') {
                        window.location.href = 'index.php?r=site/login';
                    } else {
                        $('#email').val('')
                        $('#email').parent().addClass('has-error')


                        $('#email').focus()
                        alert("Email already have");
                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        } else {
            alert("Fill All fields or hsa field errors")
        }


    })
    $('#teamregister').click(function () {

        dd = 1;
        $(".form-group").each(function () {
            if ($(this).hasClass('has-error')) {

                dd = 0;

                return false;
            } else {
                dd = 1
            }
        })

        if ($('#pass').val() != "" && $('#office').val() != "" && $('#dept').val() != "" && $('#phone').val() != "" && $('#email').val() != "" && $('#fname').val() && $('#title').val() != "" && dd == 1) {

            $.ajax({
                type: "POST",
                url: "index.php?r=user/regSaveteam",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    fname: $('#fname').val(),
                    email: $('#email').val(),
                    phone: $('#phone').val(),
                    dept: $('#dept').val(),
                    office: $('#office').val(),
                    pass: $('#pass').val(),
                    title: $('#title').val(),
                    userid: $('#userid').val(),
                    wkid: $('#workid').val()
                },
                dataType: "json",
                // contentType: "application/json; charset=utf-8",

                success: function (data) {

                    if (data == 'success') {
                        window.location.href = 'index.php?r=site/login';
                    } else {
                        $('#email').val('')
                        $('#email').parent().addClass('has-error')


                        $('#email').focus()
                        alert("Email already have");
                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        } else {
            alert("Fill All fields or hsa field errors")
        }


    })

//--------team register-------------------------------
    $('#tregister').click(function () {

//    alert('dsdsds')

        $.ajax({
            type: "POST",
            url: "index.php?r=user/regTeam",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                email: $('#temail').val()
            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {

                alert(data);
                $('#temail').val("")
                $('#myModal').modal('hide')

//                user_table.fnAddData([
//                    data.id,
//                    data.disn_name,
//                    data.email,
//                    data.role,
//                    data.deptt,
//                    data.img,
//                    data.phone,
//                    data.officeLoc,
//                    '<button class="btn btn-outline btn-default delete-user"><i class="fa fa-trash-o"></i></button>',
//                    '<button class="btn btn-outline btn-default edit-user" data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'
//
//
//
//
//
//                ])
                $('#successUser-alert').show();
                setTimeout(function () {
                    //$('#businessEdit').modal('hide')
                    $('#successUser-alert').hide();
                }, 2000);

            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    })

//---------delete team--------------------------------
    $('#user-table').on('click', '.delete-user', function (e) {
        tableEditRowIndex = $(this).parent().parent().index();
        b = confirm("Are you sure?")
        id = $(this).parent().siblings().first()
        //type= id.next().next().html()

        if (b) {
            $.ajax({
                type: "POST",
                url: "index.php?r=user/delete_user",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    id: id.html()

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    user_table.fnDeleteRow(tableEditRowIndex)
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

    })


//---------update team--------------------------------
    $('#user-table').on('click', '.edit-user', function (e) {

        i = 0


        tableEditRowIndex = $(this).parent().parent().index();

        id = $(this).parent().siblings().first()
        uid = id.html()
        uname = id.next().html()
        email = id.next().next().html()
        role = id.next().next().next().html()
        dept = id.next().next().next().next().html()
        title = id.next().next().next().next().next().html()
        phone = id.next().next().next().next().next().next().html()
        office = id.next().next().next().next().next().next().next().html()

        $('#etfname').val(uname)
        $('#etemail').val(email)
        $('#etphone').val()
        $('#etdept').val(dept)
        $('#etoffice').val(office)
        $('#etitle').val(title)
        $('#etphone').val(phone)


        $('#edit_register').click(function () {
            if (i == 0) {
                $.ajax({
                    type: "POST",
                    url: "index.php?r=user/update",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: {
                        id: id.html(),
                        uname: $('#etfname').val(),
                        email: $('#etemail').val(),
                        office: $('#etoffice').val(),
                        dept: $('#etdept').val(),
                        phone: $('#etphone').val(),
                        role: role,
                        title: $('#etitle').val()


                    },

                    dataType: "json",
                    success: function (data) {

                        if (data.role != 'admin') {
                            user_table.fnUpdate([
                                data.id,
                                data.disn_name,
                                data.email,
                                data.role,
                                data.deptt,
                                data.img,
                                data.phone,
                                data.officeLoc,

                                '<button class="btn btn-outline btn-default delete-user"><i class="fa fa-trash-o"></i></button>',
                                '<button class="btn btn-outline btn-default edit-user" data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'


                            ], tableEditRowIndex);
                        } else {
                            user_table.fnUpdate([
                                data.id,
                                data.dis_name,
                                data.email,
                                data.role,
                                data.deptt,
                                data.title,
                                data.phone,
                                data.office,
                                '<button class="btn btn-outline btn-default delete-user" disabled><i class="fa fa-trash-o"></i></button>',
                                '<button class="btn btn-outline btn-default edit-user"  data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'


                            ], tableEditRowIndex);

                        }


                        $('#esuccess-alert').show();
                        setTimeout(function () {
                            //$('#businessEdit').modal('hide')
                            $('#esuccess-alert').hide();
                        }, 2000);


                        $('#etfname').val("")
                        $('#etemail').val("")
                        $('#etphone').val("")
                        $('#etdept').val("")
                        $('#etoffice').val("")
                        $('#etitle').val("")

                        role = ""
                        utype = ""

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });


            }
            i++
        })


    })


//    $('#edit_register').click(function(){
//
//        alert('sasa')
//    })
//-----------------------end register-----------------------------------------

    $('#tsuccess-alert').hide()
    $('#edit-success-alert').hide()

    //-------tooltips------------------------
    options = {
        trigger: 'focus',
        html: true,
//        title:"Add Categories",
        content: "<div class='row'> <div class='hov col-lg-3' ><span data-target='#myModal' data-toggle='modal'  class=' fa fa-lg fa-plus'></span></div> " +
            "<div class='hov col-lg-3' ><span  class=' fa fa-lg fa-clipboard'></span></div> " +
            "<div class='hov col-lg-3' ><span  class=' fa fa-lg  fa-volume-up'></span></div> " +
            "<div class='hov col-lg-3'><span  class=' fa fa-lg fa-exclamation-triangle'></span></div>  " +
            "<div class='hov col-lg-3' ><span  class=' fa fa-lg  fa-check'></span></div> </div>  "

    }
    options1 = {
        html: true,
//        title:"Add Categories",
        content: "<div class='row'> <div class='hov col-lg-3' ><span data-target='#myModal' data-toggle='modal'  class=' fa fa-lg fa-plus'></span></div> " +
            "<div class='hov col-lg-3' ></div> " +
            "<div class='hov col-lg-3' ><span  class=' fa fa-lg  fa-volume-up'></span></div> " +
            "<div class='hov col-lg-3'><span  class=' fa fa-lg fa-exclamation-triangle'></span></div>  " +
            "<div class='hov col-lg-3' ><span  class=' fa fa-lg  fa-check'></span></div> </div>  "

    }


    $('.settings').popover(options);
    identifier = '';
    i = 0


    $('.settings').on('show.bs.popover', function () {
        ids = $(this).attr('data-id')
    })

//    $(".task-btn").popover({
//        trigger: 'focus'
//    })
    $(".task-btn").each(function () {
        var $pElem = $(this);

        $pElem.popover('hide');
    });
//task delete and update
    $('.task-btn').on('shown.bs.popover', function () {

        $('#tab' + ids).on('click', '.delete-task', function (e) {


            tableEditRowIndex = $(this).parent().parent().index();
            taskid = $(this).parent().siblings().first().html();

            b = confirm("Are you sure?")
//              alert(taskid+tableEditRowIndex);
//
//            type= id.next().next().html()

            if (b) {
                $.ajax({
                    type: "POST",
                    url: "index.php?r=task/deleteTask",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: {
                        tid: taskid


                    },
                    //  contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('#' + taskid).remove();

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });


            }

        })
//'#'+ids
        $('#' + ids).on('click', '.edit-task', function (e) {

            usname = $('.box-info').parent().find('h4').html();

            taskid = $(this).parent().siblings().first().html();
            des = $('#des' + taskid).val();
            taskname = $(this).parent().siblings().first().next().html();
            owner = $(this).parent().siblings().first().next().next().html();
            stage = $(this).parent().siblings().first().next().next().next().html();
            state = $(this).parent().siblings().first().next().next().next().next().html();
            block = $(this).parent().siblings().first().next().next().next().next().next().html();
            todo = $(this).parent().siblings().first().next().next().next().next().next().next().html();
            est = $(this).parent().siblings().first().next().next().next().next().next().next().next().text();


            $('#tename').val(taskname);
            $('#tedescription').val(des);
            $('#etowner').val(owner);
            $('#testate').val(state);
            $('#teiteration').val(stage);
            $('#etodo').val(todo);
            $('#tereason').val(block);
            $('#eusid').val(usname);
            // $('#eusid').val(usid);
            $('#tid').val(taskid);
            $('#teestimate').val(est);

            $('#updatetask').click(function () {

                $.ajax({
                    type: "POST",
                    url: "index.php?r=task/update",
                    // The key needs to match your method's input parameter (case-sensitive).

                    data: {
                        id: taskid,
                        uid: ids,
                        name: $('#tename').val(),
                        description: $('#tedescription').val(),
                        owner: $('#etowner').val(),
                        state: $('#testate').val(),
                        iteration: $('#teiteration').val(),
                        reason: $('#tereason').val(),
                        estimate: $('#teestimate').val(),
                        todo: $('#etodo').val()


                    },
                    dataType: "json",
                    // contentType: "application/json; charset=utf-8",

                    success: function (data) {


//
                        $('#teiteration').val("Unsheduled")
                        $('#testate').val('defined')
                        $('#tename').val("")
                        $('#tedescription').val("")
                        $('#teowner').val("")
                        $('#tereason').val("")
                        $('#teestimate').val("")
                        $('#etodo').val("")
                        $('#eusid').val("")
                        $('#tid').val("")


                        $('#edit-success-alert').show();
                        $('#edit-success-alert').html("Successfully updated");
                        setTimeout(function () {
                            //$('#businessEdit').modal('hide')
                            $('#edit-success-alert').hide();
                        }, 2000);


                        $("#" + data.id).find("td").eq(1).html(data.tname);
                        $("#" + data.id).find("td").eq(2).html(data.owner);
                        $("#" + data.id).find("td").eq(3).html(data.stage);
                        $("#" + data.id).find("td").eq(4).html(data.state);
                        $("#" + data.id).find("td").eq(5).html(' ');
                        $("#" + data.id).find("td").eq(6).html(data.todo);
                        $("#" + data.id).find("td").eq(7).html("<i class='fa fa-clock-o' id='hh2'></i>" + data.est);
                        $("#" + data.id).find("td").eq(8).html("<button class='btn btn-xs delete-task fa fa-trash-o' ></button>");
                        $("#" + data.id).find("td").eq(9).html("<button class='btn btn-xs fa fa-edit edit-task' data-toggle='modal' data-target='#myModaledit' ></button>");

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });


            });
        });

    })

    $('.task-btn').on('show.bs.popover', function () {
//        alert(this.id)
        id = this.id
        ids = this.id

    })

    ids = 0

    //------- end of tooltips------------------------

//-----------task----------------------
    //insert
    $('#save_task').click(function () {

        $.ajax({
            type: "POST",
            url: "index.php?r=task/savetask",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                uid: ids,

                name: $('#tname').val(),
                description: $('#tdescription').val(),
                owner: $('#towner').val(),
                state: $('#tstate').val(),
                iteration: $('#titeration').val(),
                reason: $('#treason').val(),
                estimate: $('#testimate').val(),
                todo: $('#todo').val()


            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {


//

                $('#tname').val("")
                $('#tdescription').val("")
                $('#towner').val("")
                $('#treason').val("")
                $('#testimate').val("")
                $('#todo').val("")

                $('#tsuccess-alert').show();
                $('#tsuccess-alert').html("Successfully saved");
                setTimeout(function () {
                    //$('#businessEdit').modal('hide')
                    $('#tsuccess-alert').hide();
                }, 2000);


                var row_data = "";
                row_data += "<tr data-toggle='tooltip' data-placement='left' data-html=true id='des' data-content=' <div> Description </br>" + data.dis + "</div> '>" +
                    "<td>" + data.id + "</td>" +
                    "<td>" + data.tname + "</td>" +
                    "<td>" + data.owner + "</td>" +
                    "<td>" + data.stage + "</td>" +
                    "<td>" + data.state + "</td>" +
                    "<td>" + ' ' + "</td>" +
                    "<td>" + data.todo + "</td>" +
                    "<td> <i class='fa fa-clock-o' id='hh2'></i>" + data.est + "</td>" +
                    "<td> <button class='btn btn-xs delete-task fa fa-trash-o' ></button></td>" +
                    "<td> <button class='btn btn-xs fa fa-edit edit-task' data-toggle='modal' data-target='#myModaledit' ></button> </td>" +
                    "</tr>";


                $("#tab" + data.user_storiesid).append(row_data);
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    });

    //delete
    // id  = $(this).parent().siblings().first()


//-----------end of task----------------------

//mail---------------

    $('#sendEmail').click(function () {

        $.ajax({
            type: "POST",
            url: "index.php?r=mail/sendmail",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                to: 'chanadulani@gmail.com',
                subject: $('#mail_subject').val(),
                content: $('#name').val() + " :" + $('#mail_content').val()
            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {

                if (data = 'success') {

//                    $('#mail_to').val('')
                    $('#mail_subject').val('')
                    $('.wysihtml5-sandbox').html(' ')
//                    $('#mail_content').text(' ')
                    alert('successfully send')

                }

            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    })


//end of mail--------

    $('#myteamReg').modal({
        show: true,
        backdrop: false
    });

//    $('#myteamReg').on('show.bs.modal', function (e) {
//        // do something...
//    })

    $('#regForm').bootstrapValidator();
    $('#attributeForm').bootstrapValidator();


// USER STORY VALIDATION GOES HERE
    $('#myForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'The user story name is required'
                    }
                }
            },
            description: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },

            estimate: {
                validators: {
                    digits: {
                        message: 'Enter only digits'
                    },
                    notEmpty: {
                        message: 'The Hours is required'
                    }
                }
            }

        }
    });

//userstory validation ends here


    // userstory edit validation goes here

    $('#myFormEdit').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ename: {
                validators: {
                    notEmpty: {
                        message: 'The user story name is required'
                    }
                }
            },
            edescription: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },

            eestimate: {
                validators: {
                    digits: {
                        message: 'Enter only digits'
                    },
                    notEmpty: {
                        message: 'The Hours is required'
                    }
                }
            }

        }
    });


    //end

    //user story

    //task form validation goes here


    $('#myTaskForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            tname: {
                validators: {
                    notEmpty: {
                        message: 'The user story name is required'
                    }
                }
            },
            tdescription: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },

            testimate: {
                validators: {
                    digits: {
                        message: 'Enter only digits'
                    },
                    notEmpty: {
                        message: 'The Hours is required'
                    }
                }
            },
            todo: {
                validators: {
                    digits: {
                        message: 'Enter only digits'
                    },
                    notEmpty: {
                        message: 'The Hours is required'
                    }
                }
            }

        }
    });


    $('#myTaskFormEdit').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            tename: {
                validators: {
                    notEmpty: {
                        message: 'The user story name is required'
                    }
                }
            },
            tedescription: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },

            teestimate: {
                validators: {
                    digits: {
                        message: 'Enter only digits'
                    },
                    notEmpty: {
                        message: 'The Hours is required'
                    }
                }
            },
            etodo: {
                validators: {
                    digits: {
                        message: 'Enter only digits'
                    },
                    notEmpty: {
                        message: 'The Hours is required'
                    }
                }
            }

        }
    });


//userform edit


    $('#user-form-edit').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            etfname: {
                validators: {
                    notEmpty: {
                        message: 'The Name is required'
                    }
                }
            },
            etemail: {
                validators: {
                    emailAddress: {
                        message: 'The email is invalid'
                    },
                    notEmpty: {
                        message: 'Email is required'
                    }

                }
            },

            etphone: {
                validators: {
                    phone: {
                        message: 'Enter a valid phone number'
                    },
                    notEmpty: {
                        message: 'The phone is required'
                    }
                }
            },
            etitle: {
                validators: {
                    notEmpty: {
                        message: 'The title is required'
                    }
                }
            }

        }
    });


    //tSK VALIDATION ENDS


//remove error class in task add form

    $('.row').on('click', '.addts', function (e) {


        $('.form-group').removeClass('has-error')
        $('.form-control-feedback').hide();
        $('.form-group').removeClass('has-success')
        $('.help-block').hide()
        $('#tname').val("");
        $('#tdescription').val("");
        $('#todo').val("");
        $('#testimate').val("");
        $('#treason').val("");


        $('#success-alert').html('');

//remove error class in task edit

    });
    $('.row').on('click', '.edit-task', function (e) {
        $('.form-group').removeClass('has-error')
        $('.form-control-feedback').hide();
        $('.form-group').removeClass('has-success')
        $('.help-block').hide()
        ;


        $('#success-alert').html('');


    });
//userstory add form remove errors
    $('#addUS').click(function () {

        $('#name').val('')
        $('.form-group').removeClass('has-error')
        $('.form-control-feedback').hide();
        $('.form-group').removeClass('has-success')
        $('.help-block').hide()

        $('#description').val('')
        $('#reason').val('')
        $('#estimate').val('');
        $('#success-alert').html('');

        max = 500;

    });
    //userstory edit form remove errors
    $('#story-table').on('click', '.edit-product', function (e) {
        $('.form-group').removeClass('has-error')
        $('.form-control-feedback').hide();
        $('.form-group').removeClass('has-success')
        $('.help-block').hide()
        $('#success-alert').html('');
    });

    //adduser to the team member - form validation


    var availableTags = [

    ];

    $('#addteam').click(function () {
        $('.form-group').removeClass('has-error')
        $('.form-control-feedback').hide();
        $('.form-group').removeClass('has-success')
        $('.help-block').hide()
        $('#tfname').val("");
        $('#tphone').val("");
        $('#temail').val("");
        $('#title').val("");
        $('#tdept').val("");
        $('#toffice').val("");

        $.getJSON("index.php?r=user/emails",
            function (data) {
                $.each(data, function (key, val) {

                    availableTags[key] = val;
                })
            });


    });

    $("#temail").autocomplete({
        source: availableTags
    });

    $(".addresspicker").autocomplete("option", "appendTo", "#addUserForm");

    function validateUSForm() {
        $('#addUserForm2').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                tfname: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        }
                    }
                },
//                temail: {
//                    validators: {
//                        notEmpty: {
//                            message: 'The Email is required'
//                        },
//                        emailAddress:{
//                            message:'Enter valid email'
//                        }
//
//                    }
//                },

                tphone: {
                    validators: {
                        phone: {
                            message: 'Enter a valid phone number'
                        },
                        notEmpty: {
                            message: 'The phone number is required'
                        }
                    }
                }


            }
        });
    }

    validateUSForm()


    $('#admin').click(function () {
        $('.form-group').removeClass('has-error')
        $('.form-control-feedback').hide();
        $('.form-group').removeClass('has-success')
        $('.help-block').hide()
    });

    //    project
    var id = '';
    $('.headerp').on('click', '.head', function () {

        id = this.id;

        content = $(this).html();
        $(this).parent().html('<input type="text" value="' + content + '"  class="form-control" id="ss' + id + '" name="tname" placeholder="Task Name" />')
        $('#ss' + id).focus();
        $('.headerp').on('focusout', '#ss' + id, function () {
            content = $(this).val()
            $(this).parent().html('<h3 id="' + id + '" class="head box-title">' + content + '</h3>' +
                '<div class="box-tools pull-right">' +
                '<button class="btn bg-light-blue btn-sm delete" style="background-color: transparent;" data-widget="remove">' +
                '<i class="fa fa-times"></i>' +
                '</button>' +
                '</div>')

            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/changename",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    id: id,
                    name: content,
                    filter: 'name'

                },
                dataType: "json",
                // contentType: "application/json; charset=utf-8",

                success: function (data) {

                    if (data = 'success') {

//                        $('#mail_to').val('')
//                        $('#mail_subject').val('')
////                    $('#mail_content').val('')
//                        $('#mail_content').html('')
//                        alert('successfully send')

                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        })
    })

    $('.parap').on('click', '.para', function () {

        id = this.id;


        content = $(this).html();


        dataid = $(this).attr('data-id')
//        alert(dataid)
        $(this).parent().html('<textarea cols="50"   id="' + id + '" name="description" rows="5" data-validation="required" maxlength="500" class="form-control" >' + content + '</textarea>')
        $('#' + id).focus();

        $('.parap').on('focusout', '#' + id, function () {
            content = $(this).val()
            $(this).parent().html('<p class="para" data-id="' + dataid + '" id="' + id + '">' + content + '</p>')

            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/changename",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    id: dataid,
                    des: content,
                    filter: 'des'

                },
                dataType: "json",
                // contentType: "application/json; charset=utf-8",

                success: function (data) {

                    if (data = 'success') {

//                        $('#mail_to').val('')
//                        $('#mail_subject').val('')
////                    $('#mail_content').val('')
//                        $('#mail_content').html('')
//                        alert('successfully send')

                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        })
    })

    $('#myprojectForm').bootstrapValidator({
        message: 'This value is not valid',
//        feedbackIcons: {
//            valid: 'glyphicon glyphicon-ok',
//            invalid: 'glyphicon glyphicon-remove',
//            validating: 'glyphicon glyphicon-refresh'
//        },
        // Set default threshold for all fields. It is null by default

        fields: {
            name: {

                validators: {
                    notEmpty: {
                        message: 'The project name is required'
                    }
                }
            },
            description: {
                // The threshold option does not effect to the elements
                // which user cannot type in, such as radio, checkbox, select, etc.

                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            }

        }
    });

    $('#proj').click(function () {

        $('#proname').val('')
        $('.form-group').removeClass('has-error')
        $('.form-group').removeClass('has-success')
        $('.help-block').hide()

        $('#prodescription').val('')


    })

    $('#save_project').click(function () {

        if ($('#proname').val() != '' && $('#prodescription').val() != '') {
            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/addproject",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    name: $('#proname').val(),
                    des: $('#prodescription').val(),
                    uid: $('#userid').val()


                },
                dataType: "json",
                // contentType: "application/json; charset=utf-8",

                success: function (data) {

//                    $('#rowproject').append('<div class="col-xs-3 project">' +
//                        '<div class="box box-solid bg-navy ">' +
//                        '<div class="box-header headerp">' +
//                        '<h3 id="'+data.id+'" class="head box-title">'+data.name+'</h3> ' +
//                        ' </div>' +
//                        '<div class="box-body parap">' +
//                        '<p class="para" data-id="'+data.id+'" id="para'+data.id+'">'+data.des+' </p>')

                    window.location.href = 'index.php?r=site/index';


                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        } else {
            alert('Can not be blank ')

        }


    })

    $('.delete').click(function () {

        b = confirm("Are you sure?")

        id = $(this).parent().siblings().first().prop('id');

        if (b) {

            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/delete",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    id: id

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

    })

    $('.project').on('click', function () {


        id = this.id;
        projectname = $(this).children().first().children().first().children('h3').html()

        $('#proj_title').text(projectname)

        $('.project').each(function () {
            $(this).children().first().removeClass('bg-green')
            $(this).children().first().addClass('bg-navy')
        })

        $(this).children().first().removeClass('bg-navy')
        $(this).children().first().addClass('bg-green')


        $.ajax({
            type: "POST",
            url: "index.php?r=workspace/change",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {
                id: id

            },
//            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {


            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });

    })
// end of project


//   iteration dates----------------------
    $('#itera-success-alert').hide();
    $('#date').daterangepicker({format: 'YYYY/MM/DD'});


//   end of dates----------------


    //manage iteration
    $('ul#items').easyPaginate({
        step: 2
    });


    $('#additeration').on('click', function () {

        date = $('#date').val().split('-')
        sdate = date[0].replace('/', '-')
        sdate = sdate.replace('/', '-')
        edate = date[1].replace('/', '-')
        edate = edate.replace('/', '-')

        $.ajax({
            type: "POST",
            url: "index.php?r=iteration/create",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {
                name: $('#itename').val(),
                description: $('#itdescription').val(),
                enddate: edate,
                startdate: sdate,
                state: $('#itstate').val()
            },
//            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

//                content='<div class="box-header" title="" data-toggle="tooltip" data-original-title="'+data.dis+'">' +
//                    '<h3 class="box-title">'+data.name+'</h3></div><ol id="drop" class=" drop box box-success iteration" style=" " ></ol>'
//
//                $('#items').append('<li style="display: list-item;" id="">'+content+'</li>')

                window.location.href = 'index.php?r=task/index'


            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });

    })

    $('#items').on('click', '.itdelete', function () {
        id = $(this).parent().parent().parent().attr('it-id')

        b = confirm("Are you sure?")
        if (b) {
            $.ajax({
                type: "POST",
                url: "index.php?r=iteration/delete",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    id: id
                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

//                content='<div class="box-header" title="" data-toggle="tooltip" data-original-title="'+data.dis+'">' +
//                    '<h3 class="box-title">'+data.name+'</h3></div><ol id="drop" class=" drop box box-success iteration" style=" " ></ol>'
//
//                $('#items').append('<li style="display: list-item;" id="">'+content+'</li>')

                    window.location.href = 'index.php?r=task/index'


                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
            b = confirm("Are you sure?")
        }
    })

    $('.iteCont').on('click', '.iterationH', function () {

        content = $(this).html();
        dataid = $(this).attr('header-id')

//            alert(dataid )
//        alert(dataid)
        $(this).parent().html('<input id="con' + dataid + '" value="' + content + '" type="text" class="form-control">')
        $('#con' + dataid).focus();


        $('#con' + dataid).on('focusout', function () {
            content = $(this).val()


            $(this).parent().html('<h3 header-id="' + dataid + '" class="box-title iterationH ">' + content + '</h3>')

            $.ajax({
                type: "POST",
                url: "index.php?r=iteration/nameedit",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    id: dataid,
                    des: content


                },
                dataType: "json",
                // contentType: "application/json; charset=utf-8",

                success: function (data) {

                    if (data = 'success') {

//                        $('#mail_to').val('')
//                        $('#mail_subject').val('')
////                    $('#mail_content').val('')
//                        $('#mail_content').html('')
//                        alert('successfully send')

                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        })


    })

    $('.itsettings').on('click', function () {
        id = $(this).parent().parent().parent().attr('it-id')

        $.ajax({
            type: "POST",
            url: "index.php?r=iteration/data_all",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {
                id: id
            },
//            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                sdate = data.startd.replace('-', '/')
                sdate = sdate.replace('-', '/')

                edate = data.endd.replace('-', '/')
                edate = edate.replace('-', '/')


                $('#edititeration').modal('show')

                $('#eitename').val(data.name)
                $('#eitdescription').val(data.dis)
                $('#eitstate').val(data.state)
                $('#edate').val(sdate + '-' + edate)
                $('#iteration').val(data.id)

//                $('#edate').daterangepicker({startDate: sdate ,endDate: edate});
                $('#edate').daterangepicker({ startDate: sdate, endDate: edate, format: 'YYYY/MM/DD' })

            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });

//        alert(id)
    })

    $('#eiteration').on('click', function () {

        date = $('#edate').val().split('-')
        sdate = date[0].replace('/', '-')
        sdate = sdate.replace('/', '-')
        edate = date[1].replace('/', '-')
        edate = edate.replace('/', '-')

        $.ajax({
            type: "POST",
            url: "index.php?r=iteration/edit",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {
                name: $('#eitename').val(),
                description: $('#eitdescription').val(),
                enddate: edate,
                startdate: sdate,
                state: $('#eitstate').val(),
                id: $('#iteration').val()
            },
//            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

//                content='<div class="box-header" title="" data-toggle="tooltip" data-original-title="'+data.dis+'">' +
//                    '<h3 class="box-title">'+data.name+'</h3></div><ol id="drop" class=" drop box box-success iteration" style=" " ></ol>'
//
//                $('#items').append('<li style="display: list-item;" id="">'+content+'</li>')

                window.location.href = 'index.php?r=task/index'


            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    })


//end of manage iteration

//    release backlog

    $('.reteCont').on('click', '.iterationH', function () {

        content = $(this).html();
        dataid = $(this).attr('header-id')

//            alert(dataid )
//        alert(dataid)
        $(this).parent().html('<input id="con' + dataid + '" value="' + content + '" type="text" class="form-control">')
        $('#con' + dataid).focus();


        $('#con' + dataid).on('focusout', function () {
            content = $(this).val()
            if (content == "") {
                content = "Default"
            }


            $(this).parent().html('<h3 header-id="' + dataid + '" class="box-title iterationH ">' + content + '</h3>')


            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/nameedit",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    id: dataid,
                    des: content


                },
                dataType: "json",
                // contentType: "application/json; charset=utf-8",

                success: function (data) {

                    if (data = 'success') {

//                        $('#mail_to').val('')
//                        $('#mail_subject').val('')
////                    $('#mail_content').val('')
//                        $('#mail_content').html('')
//                        alert('successfully send')

                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        })


    })


//  end of release backlog


    //chart -------------------------------------
    var defaultOption = 'null';
    $('.toolm').tooltip('show');

    $('#unameSelect').change(function () {
        defaultOption = $('#unameSelect').val();
    });

    var color1;
    var color2;
    var color3;
    var color4;
    var color5;

    $('.defineColor').colorpicker().on('changeColor', function (ev) {
        color1 = ev.color.toHex();

        $('#dcolor').val(color1);

    });
    $('.inprogressColor').colorpicker().on('changeColor', function (ev) {
        color2 = ev.color.toHex();
        $('#icolor').val(color2);
    });
    $('.acceptedColor').colorpicker().on('changeColor', function (ev) {
        color3 = ev.color.toHex();
        $('#acolor').val(color3);
    });
    $('.completedColor').colorpicker().on('changeColor', function (ev) {
        color4 = ev.color.toHex();
        $('#ccolor').val(color4);
    });
    $('.blockedColor').colorpicker().on('changeColor', function (ev) {
        color5 = ev.color.toHex();
        $('#bcolor').val(color5);
    });
    $('#change').click(function () {
        $.ajax({
            type: "POST",
            url: "index.php?r=task/drawcharts",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                dcolor: $('#dcolor').val(),
                icolor: $('#icolor').val(),
                acolor: $('#acolor').val(),
                ccolor: $('#ccolor').val(),
                bcolor: $('#bcolor').val(),
                usid: defaultOption


            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {
                colorArray = [data.dcolor, data.acolor, data.icolor, data.ccolor, data.bcolor]

                if (data.allTask == 0) {
                    alert('You don\'t have any task for this user story');
                }


                var mchart2 = Morris.Donut({
                    element: 'taskgraph',
                    data: [
                        {value: data.dTask / data.allTask, label: 'Defined'},
                        {value: data.aTask / data.allTask, label: 'Accepted'},
                        {value: data.iTask / data.allTask, label: 'In progress'},
                        {value: data.cTask / data.allTask, label: 'Completed'},
                        {value: data.bTask / data.allTask, label: 'Blocked'}
                    ],
                    backgroundColor: '#ffffff',
                    labelColor: '#060',
                    colors: colorArray,

                    formatter: function (x) {
                        return x + "%"
                    }

                });


                if (data == 'error') {
                    alert('Please select a user story');
                }


            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    });

// end of  chart -------------------------------------


//    $('#holder').sweetPages({perPage:3});
//// The default behaviour of the plugin is to insert the
//// page links in the ul, but we need them in the main container:
//    var controls = $('.swControls').detach();
//    controls.appendTo('#main');


    $('#saveDefect').click(function () {
        if (i == 0) {
            $.ajax({
                type: "POST",
                url: "index.php?r=myDefect/addDefects",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    dname: $('#dname').val(),
                    description: $('#description').val(),
                    ownerd: $('#ownerd').val(),
                    state: $('#state').val(),
                    iteration: $('#iteration').val(),
                    priority: $('#priority').val(),
                    stage: $('#stage').val(),
                    usid: $('#usid').val(),
                    reason: $('#dreason').val(),
                    best: $('#best').val()


                },

                dataType: "json",
                success: function (data) {

                    defect_table.fnAddData([
                        data.id,
                        data.dname,
                        data.dis,
                        data.breason,
                        data.priority,
                        data.est,
                        data.stage,
                        data.state,
                        data.owner,
                        data.iteration,
                        '<button class="btn btn-outline btn-default delete-defect" disabled><i class="fa fa-trash-o"></i></button>',
                        '<button class="btn btn-outline btn-default edit-defect"  data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'
                    ]);

                    $('#esuccess-alert').show();
                    setTimeout(function () {
                        //$('#businessEdit').modal('hide')
                        $('#esuccess-alert').hide();
                    }, 2000);


                    $('#dname').val("");
                    $('#description').val("");
                    $('#ownerd').val("");
                    $('#state').val("");
                    $('#iteration').val("");
                    $('#priority').val("");
                    $('#stage').val("");
                    $('#usid').val("");
                    $('#dreason').val("");
                    $('#best').val("");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });


        }
        i++
    });


    /*track iteration starts here*/
    function editState() {
        $(".usMain").draggable({
            appendTo: "body",
            revert: "invalid",
            stack: ".usMain"

        });
        $('.stateCol').droppable(
            {
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ".usMain",
//        connectToSortable: ".sort",
                drop: function (event, ui) {
//                alert(ui.draggable.html())
//                $( this ).find( ".placeholder" ).remove();
//                $( this ).appendTo('<li>'+ui.draggable.html()+'<li/>' );
                    id = $(ui.draggable).prop('id');
                    var colid = $(event.target).attr('id');
                    var newid = parseInt(id);

                    //   alert(colid);
                    $("<div id='" + id + "' class='box usMain' ></div>").html(ui.draggable.html()).appendTo(this).draggable({cursor: 'move', revert: 'invalid' });

//                $(ui.droppable).prop('.connectedSortable');
                    $(ui.draggable).remove();
//


                    $.ajax({
                        type: "POST",
                        url: "index.php?r=workspace/changeState",
                        // The key needs to match your method's input parameter (case-sensitive).
                        data: {
                            uState: colid,
                            uID: newid

                        },
//            contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                        },
                        failure: function (errMsg) {
                            alert(errMsg);
                        }
                    });

                }
            });

    }


    $('#listUS').click(function (e) {
        // alert('ff');
        var iteration = $('#trackIteration').val();
        // alert(iteration);
        $(this).css("color", "white");
        $(this).data('clicked', true);
        $('#detailUS').data('clicked', false);
        $(this).parent().css("background-color", "#36bce1");
        $('#detailUS').css("color", "#a4a4a4");
        $('#detailUS').parent().css("background-color", "white");
        var url = encodeURI("index.php?r=workspace/lView&stage=" + iteration);
        $("#main").load(url + " #content", function () {
            // $('#listUS').tooltip();


        });

    });


    $('#detailUS').click(function (e) {
        // alert('ff');
        var iteration = $('#trackIteration').val();
        // alert(iteration);
        $(this).css("color", "white");
        $(this).data('clicked', true);
        $('#listUS').data('clicked', false);
        $(this).parent().css("background-color", "#36bce1");
        $('#listUS').css("color", "#a4a4a4");
        $('#listUS').parent().css("background-color", "white");
        var url = encodeURI("index.php?r=workspace/dView&stage=" + iteration)
        $("#main").load(url + " #content"
            , function () {


                // $('#detailUS').tooltip();
                var boxid = 0;
                //  showMenu();

                editState();
                $('.usBox').on('mouseover', function (e) {

                    boxid = ($(this).attr('id'));
                });
            });


    });


    $('#trackIteration').change(function () {
        var iteration = $('#trackIteration').val();
        //alert(iteration);


        if ($('#listUS').data('clicked')) {

            var url = encodeURI("index.php?r=workspace/lView&stage=" + iteration)
            $("#main").load(url + " #content");

        }


        if ($('#detailUS').data('clicked')) {

            var url = encodeURI("index.php?r=workspace/dView&stage=" + iteration);
            $("#main").load(url + " #content", function () {
                //showMenu();
                $('.usBox').on('mouseover', function (e) {

                    boxid = ($(this).attr('id'));

                });

                /*drag and drop starts here*/

                editState();

                /*drag and drop ends here*/
            });

        }


    });


    $('#export').click(function () {
        var statge = $('#trackIteration').val();
        if (statge == '') {
            alert('Select an iteration');
        }
        // alert(statge);
        else {
            document.location.href = "index.php?r=workspace/excel&stage=" + statge + "";
        }


    });


    if ($('#detailUS').data('clicked') == true) {
        $("#main").load("index.php?r=workspace/dView #content", function () {
            //showMenu();
            /* draggable*/
            editState();
            /*draggable ends*/

        });
    }

    /*track iteration ends here*/


    /*discussion app starts here*/

    $('.add-discuss').click(function () {
        id = $(this).parent().siblings().first()
        var userstoryid = id.html();

        $.ajax({
            type: "POST",
            url: "index.php?r=discussion/saveSession",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {

                sid: userstoryid

            },
//            contentType: "application/json; charset=utf-8",
            dataType: "json",

            success: function (data) {
                //$("#contentP").load('index.php?r=discussion/index #content');
                window.location.href = 'index.php?r=discussion/index';
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    });

    $('#addDiscussion').click(function () {
        $.ajax({
            type: "POST",
            url: "index.php?r=discussion/savePost",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {

                usid: $('#usID').val(),
                postContent: $('#postContent').val()

            },
//            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function (xhr, opts) {

                //do something
                if ($('#postContent').val() == '') {
                    alert('Comment cannot be blank');
                    xhr.abort();
                }
                if ($('#usID').val() == 'select US') {
                    alert('Select a user story');
                    xhr.abort();
                }


            },
            success: function (data) {
                //$("#contentP").load('index.php?r=discussion/index #content');
                window.location.href = 'index.php?r=discussion/index';
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    });


    $('#cancelDiscussion').click(function () {
        $('#postContent').val('');
    });

    $('.editpost').click(function () {

        var id = $(this).closest("div").attr("id");
        $('#us' + id).attr('contenteditable', 'true');
        $('#' + id).append("<div style='padding-bottom: 5px; padding-left: 5px;'><i class='fa fa-check-circle fa-lg' style='margin-top: 10px;color: green;cursor: pointer; '></i><i class='fa fa-times-circle fa-lg' style='margin-top: 10px;margin-left:15px;color: green;cursor: pointer; '></i><div>");
        $(this).unbind('click');
        $('.fa-check-circle').click(function () {
            $newpost = $('#us' + id).html();
            $.ajax({
                type: "POST",
                url: "index.php?r=discussion/editPost",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    postContent: $newpost,
                    postid: id

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, opts) {

                    //do something
                    if ($newpost == '') {
                        alert('Comment cannot be blank');
                        xhr.abort();

                    }


                },
                success: function (data) {
                    //$("#contentP").load('index.php?r=discussion/index #content');
                    window.location.href = 'index.php?r=discussion/index';
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        });


        $('.fa-times-circle').click(function () {
            window.location.href = 'index.php?r=discussion/index';
        });


    });

    $('.deletepost').click(function () {
        b = confirm("Are you sure?")
        var id = $(this).closest("div").attr("id");
        //type= id.next().next().html()

        if (b) {
            $.ajax({
                type: "POST",
                url: "index.php?r=discussion/deletePost",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    uid: id

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (data == 'success') {
                        window.location.href = 'index.php?r=discussion/index';
                    }
                    else if (data == 'error') {
                        alert('First delete the comments related to the post');
                    }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

    });


    $('.replypost').one('click', function () {
        var state = true;
        if (state == true) {
            $('.replyPost').prop('disabled', true);
        }
        var id = $(this).closest("div").attr("id");
        var newid = parseInt(id);


        $('#' + id).after('<textarea cols="125" rows="2" id="pReply"></textarea><div style="padding-bottom: 5px; padding-left: 5px;"><i class="fa fa-check-circle fa-lg saveR" style="margin-top: 10px;color: green;cursor: pointer; "></i><i class="fa fa-times-circle fa-lg cancelR" style="margin-top: 10px;margin-left:15px;color: green;cursor: pointer;"></i><div>')


        $ownername = $('#' + id + ' span:nth-child(2)').html();
        //alert($ownername);
        $usID = $('#' + id + ' span:nth-child(2)').next().attr('usname');
        var newusid = parseInt($usID);


        $('.saveR').click(function () {
            var content = $('#pReply').val();

            $.ajax({
                type: "POST",
                url: "index.php?r=discussion/replyPost",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    replyContent: content,
                    pid: newid,
                    sendTo: $ownername,
                    usid: newusid

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, opts) {

                    //do something
                    if (content == '') {
                        alert('Comment cannot be blank');
                        xhr.abort();

                    }
                },
                success: function (data) {
                    //$("#contentP").load('index.php?r=discussion/index #content');
                    window.location.href = 'index.php?r=discussion/index';
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        });


        $('.cancelR').click(function () {
            window.location.href = 'index.php?r=discussion/index';
        });

    });

    $('#defect-table').on('click', '.delete-defect', function (e) {
        tableEditRowIndex = $(this).parent().parent().index();
       var b = confirm("Are you sure?");
       var id = $(this).parent().siblings().first();

        id = id.find('a');
        //type= id.next().next().html()

        if (b) {
            $.ajax({
                type: "POST",
                url: "index.php?r=mydefect/delete_defect",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    id: id.html()
                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    defect_table.fnDeleteRow(tableEditRowIndex)
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

    });

    $('#defect-table').on('click', '.edit-defect', function (e) {
        $('.form-group').removeClass('has-error');
        $('.form-control-feedback').hide();
        $('.form-group').removeClass('has-success');
        $('.help-block').hide();
        $('#esuccess-alert').html("");

        var tableEditRowIndex = $(this).parent().parent().index();
        var pid = $(this).parent().siblings().html();

        userStroyId = pid;

        $.ajax({
            type: "POST",
            url: "index.php?r=mydefect/setFields",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                id: pid
            },
            dataType: "json",
            success: function (data) {
                $('#edname').val(data.dname);
                $('#edescription').val(data.dis);
                $('#eownerd').val(data.owner);
                $('#estate').val(data.state);
                $('#eiteration').val(data.sstate);
                $('#epriority').val(data.priority);
                $('#estage').val(data.stage);
                $('#eusid').val(data.user_storiesid);
                $('#edreason').val(data.breason);
                $('#ebest').val(data.est);

            },

            success: function (data) {
                //$("#contentP").load('index.php?r=discussion/index #content');
                window.location.href = 'index.php?r=discussion/index';
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


        $('#update_defect').click(function () {

            $.ajax({
                type: "POST",
                url: "index.php?r=mydefect/EditDefect",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    id: userStroyId,
                    dname: $('#edname').val(),
                    description: $('#edescription').val(),
                    ownerd: $('#eownerd').val(),
                    state: $('#estate').val(),
                    iteration: $('#eiteration').val(),
                    priority: $('#epriority').val(),
                    stage: $('#estage').val(),
                    usid: $('#eusid').val(),
                    reason: $('#edreason').val(),
                    best: $('#ebest').val()

                },
                dataType: "json",
                beforeSend: function (xhr, opts) {

                    /*if ($("#ebcheck").prop('checked') == true) {
                     if ($('#ereason').val() == '') {
                     alert('Provide the blocked reason');
                     xhr.abort();
                     }
                     }*/

                },
                success: function (data) {
                    if (data == "error") {
                        $('#esuccess-alert').html("Please fill all the required fields");
                        return;
                    }

                    defect_table.fnUpdate([
                        data.id,
                        data.dname,
                        data.dis,
                        data.breason,
                        data.priority,
                        data.est,
                        data.stage,
                        data.state,
                        data.owner,
                        data.iteration,
                        '<button class="btn btn-outline btn-default delete-defect" disabled><i class="fa fa-trash-o"></i></button>',
                        '<button class="btn btn-outline btn-default edit-defect"  data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'
                    ], tableEditRowIndex);

                    if (data == 'success') {
                        $('#edname').val("");
                        $('#edescription').val("");
                        $('#eownerd').val("");
                        $('#estate').val("");
                        $('#eiteration').val("");
                        $('#epriority').val("");
                        $('#estage').val("");
                        $('#eusid').val("");
                        $('#edreason').val("");
                        $('#ebest').val("");

                        $('#esuccess-alert').html("Successfully Updated");
                        // alert('bhdffbhb');
                    }
                },

                failure: function (errMsg) {
                    alert(errMsg);
                }
            });


        });

    });


    /*
     //Batti Add Defects
     $('#saveDefect').click(function () {
     //  alert($('#dreason').val());
     $.ajax({
     type: "POST",
     url: "index.php?r=myDefect/addDefects",
     // The key needs to match your method's input parameter (case-sensitive).

     data: {
     dname: $('#dname').val(),
     description: $('#description').val(),
     ownerd: $('#ownerd').val(),
     state: $('#state').val(),
     iteration: $('#iteration').val(),
     priority: $('#priority').val(),
     stage: $('#stage').val(),
     usid: $('#usid').val(),
     reason: $('#dreason').val(),
     best: $('#best').val()

     },
     dataType: "json",
     // contentType: "application/json; charset=utf-8",

     success: function (data) {


     $('#dname').val("");
     $('#name').val('');
     $('.form-group').removeClass('has-error');
     $('.form-control-feedback').hide();
     $('.form-group').removeClass('has-success');
     $('.help-block').hide();
     $('#description').val("");
     $('#dreason').val("");
     $('#best').val("");
     $("select").prop("selectedIndex", 0);

     if (data == 'Please fill all the feilds') {
     alert('Please fill all the feilds');
     }
     if (data == 'Enter numeric value for estimaied hours') {
     alert('Enter numeric value for estimaied hours');
     }
     else if (data == 'ok') {
     alert('Successfully inserted');

     }


     },
     failure: function (errMsg) {
     alert(errMsg);
     }
     });


     });

     */

    $('#mydefect').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            dname: {
                validators: {
                    notEmpty: {
                        message: 'The defect name is required'
                    }
                }
            },
            description: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },

            dreason: {
                validators: {
                    notEmpty: {
                        message: 'The reason for the defect is required'
                    }
                }
            },
            best: {
                validators: {
                    digits: {
                        message: 'Enter numeric value for estimaied hours'
                    },
                    notEmpty: {
                        message: 'The Hours is required'
                    }
                }
            }

        }
    });


    $('.deleteRep').click(function () {
        b = confirm("Are you sure?")
        var id = $(this).closest("div").attr("id");
        //type= id.next().next().html()


        if (b) {
            $.ajax({
                type: "POST",
                url: "index.php?r=discussion/deleteRep",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    uid: id

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data == 'success') {
                        window.location.href = 'index.php?r=discussion/index';
                    }
                    else if (data == 'error') {
                        alert('first delete the reply releted to the post');
                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }
    });

    $('.editRep').click(function () {

        var id = $(this).closest("div").attr("id");
        var newid = parseInt(id);
        $('#usrep' + newid).attr('contenteditable', 'true');
        $('#' + id).append("<div style='padding-bottom: 5px; padding-left: 5px;'><i class='fa fa-check-circle fa-lg' style='margin-top: 10px;color: green;cursor: pointer; '></i><i class='fa fa-times-circle fa-lg' style='margin-top: 10px;margin-left:15px;color: green;cursor: pointer; '></i><div>");

        $(this).unbind('click');

        $('.fa-check-circle').click(function () {
            newpost = $('#usrep' + newid).html();
            $.ajax({
                type: "POST",
                url: "index.php?r=discussion/editRep",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    postContent: newpost,
                    postid: newid

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, opts) {

                    //do something
                    if (newpost == '') {
                        alert('Comment cannot be blank');
                        xhr.abort();

                    }


                },
                success: function (data) {
                    //$("#contentP").load('index.php?r=discussion/index #content');
                    window.location.href = 'index.php?r=discussion/index';
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        });


        $('.fa-times-circle').click(function () {
            window.location.href = 'index.php?r=discussion/index';
        });


    });

    $('.replyforReply').click(function () {

        var postid = $(this).parent().siblings('div:first-child').attr('pid');
        var pid = parseInt(postid);
        var repid = $(this).parent().siblings('div:first-child').attr('id');
        //alert(repid);

        $usID = $('#' + repid + ' span:nth-child(2)').next().attr('uName');
        $usname = $('#' + repid + ' span:nth-child(2)').next().attr('un');


        var newusid = parseInt($usID);


        $('#' + repid).parent().after('<textarea style="margin-left: 30px;" cols="125" rows="2" id="rReply"></textarea><div style="padding-bottom: 5px; padding-left: 30px;"><i class="fa fa-check-circle fa-lg saveR" style="margin-top: 10px;color: green;cursor: pointer; "></i><i class="fa fa-times-circle fa-lg cancelR" style="margin-top: 10px;margin-left:15px;color: green;cursor: pointer;"></i><div>')

        var user = $('#' + repid + ' span:nth-child(2)').html();
        var postOwner = $('#' + repid).attr('postowner');


        $(this).unbind('click');


        $('.saveR').click(function () {
            var content = $('#rReply').val();

            $.ajax({
                type: "POST",
                url: "index.php?r=discussion/replyforPost",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    replyContent: content,
                    pid: pid,
                    sendTo: user,
                    pOwner: postOwner,
                    usid: newusid,
                    usname: $usname

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, opts) {

                    //do something
                    if (content == '') {
                        alert('Comment cannot be blank');
                        xhr.abort();

                    }


                },
                success: function (data) {
                    //$("#contentP").load('index.php?r=discussion/index #content');
                    window.location.href = 'index.php?r=discussion/index';
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });


        });


        $('.cancelR').click(function () {
            window.location.href = 'index.php?r=discussion/index';
        });


    });


    /*discussion app ends here*/

    //    jstree
//    var tree;
////    $('#project').jstree();
    var trees = [
        { "id": "ajson1",
            "parent": "#",
            "text": "Own Project",
            'state': {
                opened: false  // is the node open
//                            disabled  : boolean  // is the node disabled
//            selected  : true  // is the node selected
            }},
        { "id": "ajson2",
            "parent": "#",
            "text": "Assigned Project",
            'state': {
                opened: false  // is the node open
//                            disabled  : boolean  // is the node disabled
//            selected  : true  // is the node selected
            }}
    ]

    $.getJSON("index.php?r=workspace/getnode",
        function (data) {
            wid = $('#wid').val();
            state = false;
            $.each(data, function (key, val) {

                if (val.id == wid) {
                    state = true;
                } else {
                    state = false;
                }
                if (val.type == 'owner') {

                    tree = {
                        'id': val.id,
                        'parent': "ajson1",
                        'text': val.text,
                        'state': {
                            opened: state,  // is the node open
//                            disabled  : boolean  // is the node disabled
                            selected: state  // is the node selected
                        }
                    }

                } else {
                    tree = {
                        'id': val.id,
                        'parent': "ajson2",
                        'text': val.text,
                        'state': {
                            opened: state,  // is the node open
//                            disabled  : boolean  // is the node disabled
                            selected: state  // is the node selected
                        }
                    }

                }
                trees.push(tree)

            })
//            $.each(trees,function(key,val){
//                alert(val.id)
//            })

            jsTree = $('#project')
                .jstree({
                    'core': {

                        'data': trees,

                        'check_callback': true,
                        'themes': {
                            'responsive': false
                        }
                    },
                    'plugins': ['dnd', 'contextmenu']
                })

        });

    jsTree = '';
//     $('#project').jstree("select_node", $('#wid').val());

//    a='';
    function detectLeftButton(evt) {
        evt = evt || window.event;
        var button = evt.which;
        return button;
//        if( button != 1 && ( typeof button != "undefined")){return false;}return true;
    }

    left = 0;
    $('#project').on('select_node.jstree',function (e, data) {
//        alert(data.selected);
//        var i, j, r = [];

        id = "";
//        for(i = 0, j = data.selected.length; i < j; i++) {
////            id=data.selected[i];
//        }

        id = data.node.id

//        alert('dsdsds')

        if (data.instance.is_parent(data.node)) {
            if (data.instance.is_closed(data.node)) {
                data.instance.open_node(data.node);
            } else {
                data.instance.close_node(data.node);
                data.instance.deselect_node(data.node);
            }
        } else {

//                var evt =  window.event || event;
//                var button = evt.which || evt.button;


//                if( event.which==1) {
//                    left=1;
//                };
            if (id != '' && left != 3) {
                $.ajax({
                    type: "POST",
                    url: "index.php?r=workspace/change",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: {
                        id: id
                    },
//            contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data == 'success') {
                            a = window.location.href;
                            window.location.href = a;
////                                }
//                                var auto_refresh = setInterval(function () {
//                                    $('.view').fadeOut('slow', function() {
//                                        $(this).load(a, function() {
//                                            $(this).fadeIn('slow');
//                                        });
//                                    });
//                                }, 15000);
                        }
                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
    }).on('create_node.jstree',function (e, data) {
            if (data.node.parent == 'ajson1') {
                id = 0
                if (data.node.text != "New node") {
                    id = data.node.id
                }
                $.get('index.php?r=workspace/create_node', {'id': id, 'text': data.node.text })
                    .done(function (d) {
                        data.instance.set_id(data.node, d.id);
                    })
                    .fail(function () {
                        data.instance.refresh();
                    });

            } else {
//                data.instance.refresh();
                alert('Invalid option')

            }

        }).on('rename_node.jstree',function (e, data) {
            if (data.node.parent == 'ajson1') {
                $.get('index.php?r=workspace/create_node', { 'id': data.node.id, 'text': data.text })
                    .fail(function () {
                        data.instance.refresh();
                    });
            } else {
//                data.instance.refresh();
                alert('Invalid option')

            }
        }).on('delete_node.jstree',function (e, data) {

            b = confirm("Are you sure delete project?")
            wid = $('#wid').val();
            if (data.node.id != wid) {
                if (b) {
                    $.get('index.php?r=workspace/delete', { 'id': data.node.id })
                        .done(function () {
//                            a = window.location.href;
//
//
//                            window.location.href = a;
                            left = 0;
                            data.instance.refresh();
                        });
                } else {
                    data.instance.refresh();
                    left = 0;
                }

            } else {
                alert('Please Change Workspace?')
                left = 0;

                data.instance.refresh();
            }

        }).on('mousedown.jstree', function (e, data) {


            if (e.which == 1) {
                left = 1
            } else if (e.which == 3) {
                left = 3
            }

//            alert(e.which)
        });

//  end of  jstree


    // user stories js


    /*defect starts*/


    $('#saveDefect').click(function () {
        //  alert($('#dreason').val());
        $.ajax({
            type: "POST",
            url: "index.php?r=myDefect/addDefects",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                dname: $('#dname').val(),
                description: $('#description').val(),
                ownerd: $('#ownerd').val(),
                state: $('#state').val(),
                iteration: $('#iteration').val(),
                priority: $('#priority').val(),
                stage: $('#stage').val(),
                usid: $('#usid').val(),
                reason: $('#dreason').val(),
                best: $('#best').val()

            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {


                $('#dname').val("");
                $('#name').val('');
                $('.form-group').removeClass('has-error');
                $('.form-control-feedback').hide();
                $('.form-group').removeClass('has-success');
                $('.help-block').hide();
                $('#description').val("");
                $('#dreason').val("");
                $('#best').val("");
                $("select").prop("selectedIndex", 0);

                if (data == 'Please fill all the feilds') {
                    alert('Please fill all the feilds');
                }
                if (data == 'Enter numeric value for estimaied hours') {
                    alert('Enter numeric value for estimaied hours');
                }
                else if (data == 'ok') {
                    alert('Successfully inserted');

                }


            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    });

    $('#mydefect').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            dname: {
                validators: {
                    notEmpty: {
                        message: 'The defect name is required'
                    }
                }
            },
            description: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            },

            dreason: {
                validators: {
                    notEmpty: {
                        message: 'The reason for the defect is required'
                    }
                }
            },
            best: {
                validators: {
                    digits: {
                        message: 'Enter numeric value for estimaied hours'
                    },
                    notEmpty: {
                        message: 'The Hours is required'
                    }
                }
            }

        }
    });


    $('#clearDefect').click(function () {

        $('#dname').val("");
        $('.form-group').removeClass('has-error');
        $('.form-control-feedback').hide();
        $('.form-group').removeClass('has-success');
        $('.help-block').hide();
        $('#description').val("");
        $('#dreason').val("");
        $('#best').val("");
        $("select").prop("selectedIndex", 0);


    });
    /*defects end*/
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');

        var x = 0;
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == "subid") {
                x = sParameterName[1];
            }
        }
        setTimeout(function () {
            if (document.getElementById(x + "kk") != null) {

                $("#" + x + "kk").parent().animate({"background-color": "#FFDBE0"}, 1000);
                $("#" + x + "kk").parent().css({'background-color': '#FFDBE0'});
            }
        }, 3000);
});