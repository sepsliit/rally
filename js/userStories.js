/**
 * Created by Samsung on 3/20/14.
 */

$(document).ready(function () {
        $('#bcheck').change(function () {
            if (this.checked) {
                $('#reason').prop('disabled', false);
            } else {
                $('#reason').prop('disabled', true);
            }
        });


        $('#ebcheck').change(function () {
            if (this.checked) {
                $('#ereason').prop('disabled', false);
            }
            else {
                $('#ereason').prop('disabled', true);
                $('#ereason').val("");

            }
        });
        var user_stories = $('#story-table').dataTable();

        $('#story-table').on('click', '.edit-product', function (e) {


            $('.form-group').removeClass('has-error');
            $('.form-control-feedback').hide();
            $('.form-group').removeClass('has-success');
            $('.help-block').hide();
            $('#esuccess-alert').html("");

            tableEditRowIndex = $(this).parent().parent().index();
            pid = $(this).parent().siblings().html();
            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/setFields",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    id: pid
                },
                dataType: "json",
                success: function (data) {
                    if (data.iterationid == null) {
                        $('#editeration').val(0);

                    }

                    else {
                        $('#editeration').val(data.iterationid);
                    }
                    $('#ename').val(data.uname),
                        $('#edescription').val(data.dis),
                        $('#eowner').val(data.owner),
                        $('#estate').val(data.state),
                        $('#ereason').val(data.breason),
                        $('#eestimate').val(data.est)

                    if ($('#ereason').val() != '') {
                        $('#ebcheck').prop('checked', true);
                        $('#ereason').prop('disabled', false);
                    }


                },

                failure: function (errMsg) {
                    alert(errMsg);


                }
            });

            $('#edit_userstory').click(function () {

                var iterationID = 0;
                // alert('dsds')
                if ($('#editeration').val() == '0') {
                    iterationID = "";
                } else {
                    iterationID = $('#editeration').val();
                }


                $.ajax({
                    type: "POST",
                    url: "index.php?r=workspace/editStory",
                    // The key needs to match your method's input parameter (case-sensitive).

                    data: {
                        id: pid,
                        name: $('#ename').val(),
                        description: $('#edescription').val(),
                        owner: $('#eowner').val(),
                        state: $('#estate').val(),
                        iteration: iterationID,
                        reason: $('#ereason').val(),
                        estimate: $('#eestimate').val()


                    },
                    dataType: "json",
                    beforeSend: function (xhr, opts) {

                        if ($("#ebcheck").prop('checked') == true) {
                            if ($('#ereason').val() == '') {
                                alert('Provide the blocked reason');
                                xhr.abort();
                            }
                        }

                    },
                    success: function (data) {
                        $('#ename').val(""),
                            $('#edescription').val(""),
                            $('#eowner').val(""),
                            $('#ereason').val(""),
                            $('#eestimate').val("")
                        $('#esuccess-alert').html("Edited successfully");

                        if (data == "error") {
                            $('#esuccess-alert').html("Please fill all the required fields");


                        }

                        // $('#userstoryNameEdit').val(result['name']);


                        user_stories.fnUpdate([
                            data.id,
                            data.uname,
                            data.dis,
                            data.owner,
                            data.state,
                            data.stage,
                            data.est,
                            '<button class="btn btn-outline btn-default delete-product"><i class="fa fa-trash-o"></i></button>',
                            '<button  class="btn btn-outline btn-default edit-product" data-toggle="modal" data-target="#edit" ><i class="fa fa-edit"></i></button>'
                        ], tableEditRowIndex);

                        if (data == 'success') {
                            $('#ename').val("")
                            $('#edescription').val("")
                            $('#eowner').val("")
                            $('#ereason').val("")
                            $('#eestimate').val("")
                            $('#esuccess-alert').html("Successfully Updated");
                            // alert('bhdffbhb');


                        }
                    },

                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });
            });
        });


//    $('#estate').empty().append($('<option>', {
//        value:'Defined',
//        text: 'Defined'
//    }));


        $('#editeration').change(function () {
            if ($('#editeration').val() == '0') {
                $('#estate').empty().append($('<option>', {
                    value: 'Defined',
                    text: 'Defined'
                }));

            }
            else {

                $('#estate').empty().append($('<option>', {
                    value: 'Defined',
                    text: 'Defined'
                }), $('<option>', {
                    value: 'In progress',
                    text: 'In progress'
                }),
                    $('<option>', {
                            value: 'Completed',
                            text: 'Completed'
                        }

                    ),
                    $('<option>', {
                            value: 'Accepted',
                            text: 'Accepted'
                        }

                    )

                );
            }
            // not });
        });


        var max = 500;
        $('#mydescription').html('You have ' + max + ' characters remaining')
        $('#description').keyup(function () {
                var currentLen = $('#description').val().length;
                var remain = max - currentLen;

                $('#mydescription').html('You have ' + remain + ' characters remaining');
            }
        );


        $('#emydescription').html('You have ' + max + ' characters remaining')
        $('#edescription').keyup(function () {
                var currentLen = $('#edescription').val().length;
                var remain = max - currentLen;

                $('#emydescription').html('You have ' + remain + ' characters remaining');
            }
        );


        $('#save_userstory').click(function () {
            var iterationID = 0;
            // alert('dsds')
            if ($('#iteration').val() == '0') {
                iterationID = "";
            }
            else {
                iterationID = $('#iteration').val();
            }

            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/saveStory",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                    name: $('#name').val(),
                    description: $('#description').val(),
                    owner: $('#owner').val(),
                    iterationid: iterationID,
//                    reason: $('#reason').val(),
                    estimate: $('#estimate').val()




                },
                dataType: "json",

                beforeSend: function (xhr, opts) {

                    if ($.isNumeric($('#estimate').val()) == false) {


                        alert('Provide a number for Estimated hours ');
                        xhr.abort();
                    }


                },


                success: function (data) {


                    if (data == "error") {
                        $('#success-alert').html("Please fill all the required fields");


                    }  // td.addClass('colorR');

                    // var result = $.parseJSON(data);
                    else {

                        user_stories.fnAddData([
                            data.id,
                            data.uname,
                            data.dis,
                            data.owner,
                            data.state,
                            data.stage,
                            data.est,
                            '<button class="btn btn-outline btn-default delete-product"><i class="fa fa-trash-o"></i></button>',
                            '<button  class="btn btn-outline btn-default edit-product" data-toggle="modal" data-target="#edit" ><i class="fa fa-edit"></i></button>'


                        ]);


                        $('#name').val("")
                        $('#description').val("")
                        $('#owner').val("")
                        $('#reason').val("")
                        $('#estimate').val("")
                        $('#success-alert').html("Saved Successfully");
                    }

                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        });

        // delete row


        $('#story-table').on('click', '.delete-product', function (e) {
            tableEditRowIndex = $(this).parent().parent().index();
            b = confirm("Are you sure?")
            id = $(this).parent().siblings().first();
            id = id.find('a');
            //type= id.next().next().html()

            if (b) {
                $.ajax({
                    type: "POST",
                    url: "index.php?r=workspace/userstorydelete",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: {
                        id: id.html()

                    },
//            contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        user_stories.fnDeleteRow(tableEditRowIndex)
                    },
                    failure: function (errMsg) {

                    }
                });
            }
        });


// task js


        $('#myrow2').on('click', '.add-task', function (e) {


            var boxid = $(this).parent().siblings().find("h6").text();
            var userstoryName = $(this).parent().siblings().find("h3").text();


            $('#work').html(userstoryName);

            $('#usid').val(boxid);


        });

        var max = 500;
        $('#tmydescription').html('You have ' + max + ' characters remaining')
        $('#tdescription').keyup(function () {
                var currentLen = $('#tdescription').val().length;
                var remain = max - currentLen;

                $('#tmydescription').html('You have ' + remain + ' characters remaining');
            }
        );


        $('#tmydescription').html('You have ' + max + ' characters remaining')
        $('#tdescription').keyup(function () {
                var currentLen = $('#tdescription').val().length;
                var remain = max - currentLen;

                $('#tmydescription').html('You have ' + remain + ' characters remaining');
            }
        );


//delete task
        $('#myrow2').on('click', '.fa-trash-o', function (e) {
            var taskId = $(this).parent().siblings().next().next().next().next().next().next().next().text();
            var usid = $(this).parent().parent().parent().parent().parent().find('h6').text();


            b = confirm("Are you sure?")
            //type= id.next().next().html()

            if (b) {
                $.ajax({
                    type: "POST",
                    url: "index.php?r=task/taskdelete",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: {
                        id: taskId

                    },
//            contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data == 'success') {
                            $('#' + $usid).load("index.php?r=task/createTask" + " #" + $usid);

                        }

                    },
                    failure: function (errMsg) {

                    }
                });
            }
        });
// end of task js
    }
);











