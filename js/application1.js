$(document).ready(function () {
    // alert('hi')
    $('.plus-button').click(function () {
////       alert('hi')
////        alert($(this).siblings().first().html())
//        $(this).siblings().first().next().delay(100).slideToggle(300);
        $(this).siblings().first().delay(100).slideToggle(300);
//        $(".timeline-item").delay(100).slideToggle(600);
    })

//-----------alert---------------------------------------------------------
    $('#successUser-alert').hide();
    $('#esuccessUser-alert').hide();
//-----------end of alert--------------------------------------------------


//-----------data_table-----------------------------------------------------
    user_table = $('#user-table').dataTable();
//-----------end of data_table----------------------------------------------


//------------------------register------------------------------------------

//-----------insert------------------------------
    $('#register').click(function () {

        $.ajax({
            type: "POST",
            url: "index.php?r=user/regSave",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                fname: $('#fname').val(),
                email: $('#email').val(),
                phone: $('#phone').val(),
                dept: $('#dept').val(),
                office: $('#office').val(),
                pass: $('#pass').val()


            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {

                if (data == 'success') {
                    $('#pass').val("")
                    $('#office').val("")
                    $('#dept').val("")
                    $('#phone').val("")
                    $('#email').val("")
                    $('#fname').val("")
                }

            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    })

//--------team register-------------------------------
    $('#tregister').click(function () {

        $.ajax({
            type: "POST",
            url: "index.php?r=user/regTeam",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                fname: $('#tfname').val(),
                email: $('#temail').val(),
                phone: $('#tphone').val(),
                dept: $('#tdept').val(),
                office: $('#toffice').val()



            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {


                $('#toffice').val("")
                $('#tdept').val("")
                $('#tphone').val("")
                $('#temail').val("")
                $('#tfname').val("")

                user_table.fnAddData([
                    data.id,
                    data.disn_name,
                    data.email,
                    data.role,
                    data.deptt,
                    "member",
                    '<button class="btn btn-outline btn-default delete-user"><i class="fa fa-trash-o"></i></button>',
                    '<button class="btn btn-outline btn-default edit-user" data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'





                ])
                $('#successUser-alert').show();
                setTimeout(function () {
                    //$('#businessEdit').modal('hide')
                    $('#successUser-alert').hide();
                }, 2000);

            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    })

//---------delete team--------------------------------
    $('#user-table').on('click', '.delete-user', function (e) {
        tableEditRowIndex = $(this).parent().parent().index();
        b = confirm("Are you sure?")
        id = $(this).parent().siblings().first()
        //type= id.next().next().html()

        if (b) {
            $.ajax({
                type: "POST",
                url: "index.php?r=user/delete_user",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    id: id.html()

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    user_table.fnDeleteRow(tableEditRowIndex)
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }

    })


//---------update team--------------------------------
    $('#user-table').on('click', '.edit-user', function (e) {

        i = 0


        tableEditRowIndex = $(this).parent().parent().index();

        id = $(this).parent().siblings().first()
        uid = id.html()
        uname = id.next().html()
        email = id.next().next().html()
        role = id.next().next().next().html()
        dept = id.next().next().next().next().html()
        utype = id.next().next().next().next().next().html()

        $('#etfname').val(uname)
        $('#etemail').val(email)
        $('#etphone').val()
        $('#etdept').val(dept)
        $('#etoffice').val(role)


        $('#edit_register').click(function () {
            if (i == 0) {
                $.ajax({
                    type: "POST",
                    url: "index.php?r=user/update",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: {
                        id: id.html(),
                        uname: $('#etfname').val(),
                        email: $('#etemail').val(),
                        office: $('#etoffice').val(),
                        dept: $('#etdept').val(),
                        phone: $('#etphone').val(),
                        role: role,
                        utype: utype


                    },

                    dataType: "json",
                    success: function (data) {

                        if (data.role != 'admin') {
                            user_table.fnUpdate([
                                data.id,
                                data.disn_name,
                                data.email,
                                data.role,
                                data.deptt,
                                'member',
                                '<button class="btn btn-outline btn-default delete-user"><i class="fa fa-trash-o"></i></button>',
                                '<button class="btn btn-outline btn-default edit-user" data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'


                            ], tableEditRowIndex);
                        } else {
                            user_table.fnUpdate([
                                data.id,
                                data.dis_name,
                                data.email,
                                data.role,
                                data.deptt,
                                data.utype,
                                '<button class="btn btn-outline btn-default delete-user" disabled><i class="fa fa-trash-o"></i></button>',
                                '<button class="btn btn-outline btn-default edit-user"  data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'


                            ], tableEditRowIndex);

                        }


                        $('#esuccess-alert').show();
                        setTimeout(function () {
                            //$('#businessEdit').modal('hide')
                            $('#esuccess-alert').hide();
                        }, 2000);


                        $('#etfname').val("")
                        $('#etemail').val("")
                        $('#etphone').val("")
                        $('#etdept').val("")
                        $('#etoffice').val("")
                        role = ""
                        utype = ""

                    },
                    failure: function (errMsg) {
                        alert(errMsg);
                    }
                });


            }
            i++
        })


    })


//    $('#edit_register').click(function(){
//
//        alert('sasa')
//    })
//-----------------------end register-----------------------------------------

    $('#tsuccess-alert').hide()

    //-------tooltips------------------------
    options = {
        html: true,
//        title:"Add Categories",
        content: "<div class='row'> <div class='hov col-lg-3' ><span data-target='#myModal' data-toggle='modal'  class=' fa fa-lg fa-plus'></span></div> " +
            "<div class='hov col-lg-3' ><span  class=' fa fa-lg fa-clipboard'></span></div> " +
            "<div class='hov col-lg-3' ><span  class=' fa fa-lg  fa-volume-up'></span></div> " +
            "<div class='hov col-lg-3'><span  class=' fa fa-lg fa-exclamation-triangle'></span></div>  " +
            "<div class='hov col-lg-3' ><span  class=' fa fa-lg  fa-check'></span></div> </div>  "

    }

    ids = 0

    $('.us').on('mouseover', '.bodys', function () {
        //$('#'+this.id).tooltip('show')

        $('#' + this.id).popover(options)

        ids = this.id;

        $('#tname').val("")
        $('#tdescription').val("")
        $('#towner').val("")
        $('#treason').val("")
        $('#testimate').val("")
        $('#todo').val("")


    });

    j = 0
    var tableid = 0;

    $('.bodys').on('mouseover', '.table', function () {
        //$('#'+this.id).tooltip('show')

        $('.tog').click(function () {

            alert(this.id)
        })

//        if(j==0){
//            tableid = this.id;
//            $('#'+tableid).on('click', '.delete-task',function (e) {
//                tableEditRowIndex =$(this).parent().parent().index();
//                b  = confirm("Are you sure?")
//                id  = $(this).parent().siblings().first()
//                //type= id.next().next().html()
//
//                if(b){
////            $.ajax({
////                type: "POST",
////                url: "index.php?r=user/delete_user",
////                // The key needs to match your method's input parameter (case-sensitive).
////                data: {
////                    id: id.html()
////
////                },
//////            contentType: "application/json; charset=utf-8",
////                dataType: "json",
////                success: function(data){
////                    user_table.fnDeleteRow(tableEditRowIndex)
////                },
////                failure: function(errMsg) {
////                    alert(errMsg);
////                }
////            });
//
//                    alert(id.html())
//
//                }
//
//            })
//        }
//
//        j++

    });


//    $(".us").on('click','.hov',function() {
//        alert('fdfd')
//
//    })


    //------- end of tooltips------------------------

//-----------task----------------------
    //insert
    $('#save_task').click(function () {

        $.ajax({
            type: "POST",
            url: "index.php?r=task/savetask",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                uid: ids,

                name: $('#tname').val(),
                description: $('#tdescription').val(),
                owner: $('#towner').val(),
                state: $('#tstate').val(),
                iteration: $('#titeration').val(),
                reason: $('#treason').val(),
                estimate: $('#testimate').val(),
                todo: $('#todo').val()


            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {


//                    $('#'+$usid).load("index.php?r=task/createTask"+" #"+$usid);
                if (data == "success") {
                    $('#tname').val("")
                    $('#tdescription').val("")
                    $('#towner').val("")
                    $('#treason').val("")
                    $('#testimate').val("")
                    $('#todo').val("")

                    $('#tsuccess-alert').show();
                    $('#tsuccess-alert').html("Successfully saved");
                    setTimeout(function () {
                        //$('#businessEdit').modal('hide')
                        $('#tsuccess-alert').hide();
                    }, 2000);
                    window.location.href = 'index.php?r=task/index';

                } else {
                    $('#tsuccess-alert').show();
                    $('#tsuccess-alert').html("Please fill all the fields");

                    setTimeout(function () {
                        //$('#businessEdit').modal('hide')
                        $('#tsuccess-alert').hide();
                    }, 2000);

                }


            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    });

    //delete
    // id  = $(this).parent().siblings().first()


//-----------end of task----------------------
//--------------test ----------
//    $('.dropdown-toggle').click(function(){
//        alert('dsdsd')
//    })
//--------------end test----------


})