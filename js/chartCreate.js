/**
 * Created by Samsung on 6/23/14.
 */
$(document).ready(function () {

    var defaultOption = 'null';
    $('.toolm').tooltip('show');

    $('#unameSelect').change(function () {
        defaultOption = $('#unameSelect').val();
    });

    var color1;
    var color2;
    var color3;
    var color4;
    var color5;

    $('.defineColor').colorpicker().on('changeColor', function (ev) {
        color1 = ev.color.toHex();

        $('#dcolor').val(color1);

    });
    $('.inprogressColor').colorpicker().on('changeColor', function (ev) {
        color2 = ev.color.toHex();
        $('#icolor').val(color2);
    });
    $('.acceptedColor').colorpicker().on('changeColor', function (ev) {
        color3 = ev.color.toHex();
        $('#acolor').val(color3);
    });
    $('.completedColor').colorpicker().on('changeColor', function (ev) {
        color4 = ev.color.toHex();
        $('#ccolor').val(color4);
    });
    $('.blockedColor').colorpicker().on('changeColor', function (ev) {
        color5 = ev.color.toHex();
        $('#bcolor').val(color5);
    });
    $('#change').click(function () {
        $.ajax({
            type: "POST",
            url: "index.php?r=task/drawcharts",
            // The key needs to match your method's input parameter (case-sensitive).

            data: {
                dcolor: $('#dcolor').val(),
                icolor: $('#icolor').val(),
                acolor: $('#acolor').val(),
                ccolor: $('#ccolor').val(),
                bcolor: $('#bcolor').val(),
                usid: defaultOption


            },
            dataType: "json",
            // contentType: "application/json; charset=utf-8",

            success: function (data) {
                colorArray = [data.dcolor, data.acolor, data.icolor, data.ccolor, data.bcolor]

                if (data.allTask == 0) {
                    alert('You don\'t have any task for this user story');
                }


                var mchart2 = Morris.Donut({
                    element: 'taskgraph',
                    data: [
                        {value: data.dTask / data.allTask, label: 'Defined'},
                        {value: data.aTask / data.allTask, label: 'Accepted'},
                        {value: data.iTask / data.allTask, label: 'In progress'},
                        {value: data.cTask / data.allTask, label: 'Completed'},
                        {value: data.bTask / data.allTask, label: 'Blocked'}
                    ],
                    backgroundColor: '#ffffff',
                    labelColor: '#060',
                    colors: colorArray,

                    formatter: function (x) {
                        return x + "%"
                    }

                });


                if (data == 'error') {
                    alert('Please select a user story');
                }


            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });


    });

});

