<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Rally',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'ext.mail.YiiMailMessage',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'root',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

        'skebbySms' => array(
            'class'=>'ext.YSkebbySms.YSkebbySms',
            'username'=>'user3184871',
            'password'=>'0114931511',
        ),
        'mail' => array(
            'class' => 'ext.mail.YiiMail',
            'transportType'=>'smtp',
            'transportOptions'=>array(
                'host'=>'smtp.mandrillapp.com',
                'username'=>'nanayakkaraoffice@gmail.com',
                'password'=>'3bBjRsgEtBr7BSWJpSCOIg',
                'port'=>'587',

            ),
            'logging' => true,
            'dryRun' => false,
            'viewPath' => 'application.views.mail',
        ),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=newRally',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
//        'db'=>array(
//			'connectionString' => 'mysql:host=mysql.1freehosting.com;dbname=u866841138_rally',
//			'emulatePrepare' => true,
//			'username' => 'u866841138_rally',
//			'password' => 'rally1',
//			'charset' => 'utf8',
//		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
        'GoogleApis' => array(
            'class' => 'ext.GoogleApis.GoogleApis',

            // See http://code.google.com/p/google-api-php-client/wiki/OAuth2
            'clientId' => '258927677200-fjodb9po5dfnsj9q5a6koa9gvfuqi9f0.apps.googleusercontent.com',
            'clientSecret' => 'HG_C4w6AFRBSi0I5U65qGQ-Q',
            'redirectUri' => 'http://localhost/rallyc/index.php?r=site/AouthLogin',
            // // This is the API key for 'Simple API Access'
            'developerKey' => '258927677200-fjodb9po5dfnsj9q5a6koa9gvfuqi9f0@developer.gserviceaccount.com',
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),


);