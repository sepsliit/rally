<?php

/**
 * This is the model class for table "notification_table".
 *
 * The followings are the available columns in table 'notification_table':
 * @property integer $notifi_id
 * @property integer $notifi_type
 * @property string $img
 * @property string $headertext
 * @property string $subtext
 * @property integer $subj_id
 * @property string $added_date
 * @property integer $wid
 *
 * The followings are the available model relations:
 * @property UserStories $subj
 * @property Workspace $w
 */
class NotificationTable extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notification_table';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('notifi_type, subj_id, wid', 'numerical', 'integerOnly'=>true),
			array('img, headertext, subtext, added_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('notifi_id, notifi_type, img, headertext, subtext, subj_id, added_date, wid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subj' => array(self::BELONGS_TO, 'UserStories', 'subj_id'),
			'w' => array(self::BELONGS_TO, 'Workspace', 'wid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'notifi_id' => 'Notifi',
			'notifi_type' => 'Notifi Type',
			'img' => 'Img',
			'headertext' => 'Headertext',
			'subtext' => 'Subtext',
			'subj_id' => 'Subj',
			'added_date' => 'Added Date',
			'wid' => 'Wid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('notifi_id',$this->notifi_id);
		$criteria->compare('notifi_type',$this->notifi_type);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('headertext',$this->headertext,true);
		$criteria->compare('subtext',$this->subtext,true);
		$criteria->compare('subj_id',$this->subj_id);
		$criteria->compare('added_date',$this->added_date,true);
		$criteria->compare('wid',$this->wid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NotificationTable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
