<?php

/**
 * This is the model class for table "defect".
 *
 * The followings are the available columns in table 'defect':
 * @property integer $id
 * @property integer $user_storiesid
 * @property string $dname
 * @property string $dis
 * @property string $owner
 * @property string $state
 * @property string $est
 * @property string $stage
 * @property string $breason
 * @property string $priority
 * @property integer $sstate
 * @property integer $wid
 *
 * The followings are the available model relations:
 * @property UserStories $userStories
 * @property Iteration $sstate0
 * @property Workspace $w
 */
class Defect extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'defect';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_storiesid, sstate, wid', 'numerical', 'integerOnly'=>true),
			array('dname, dis, owner, state, est, stage, breason, priority', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_storiesid, dname, dis, owner, state, est, stage, breason, priority, sstate, wid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userStories' => array(self::BELONGS_TO, 'UserStories', 'user_storiesid'),
			'sstate0' => array(self::BELONGS_TO, 'Iteration', 'sstate'),
			'w' => array(self::BELONGS_TO, 'Workspace', 'wid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_storiesid' => 'User Storiesid',
			'dname' => 'Dname',
			'dis' => 'Dis',
			'owner' => 'Owner',
			'state' => 'State',
			'est' => 'Est',
			'stage' => 'Stage',
			'breason' => 'Breason',
			'priority' => 'Priority',
			'sstate' => 'Sstate',
			'wid' => 'Wid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_storiesid',$this->user_storiesid);
		$criteria->compare('dname',$this->dname,true);
		$criteria->compare('dis',$this->dis,true);
		$criteria->compare('owner',$this->owner,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('est',$this->est,true);
		$criteria->compare('stage',$this->stage,true);
		$criteria->compare('breason',$this->breason,true);
		$criteria->compare('priority',$this->priority,true);
		$criteria->compare('sstate',$this->sstate);
		$criteria->compare('wid',$this->wid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Defect the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
