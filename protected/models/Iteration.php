<?php

/**
 * This is the model class for table "iteration".
 *
 * The followings are the available columns in table 'iteration':
 * @property integer $id
 * @property integer $login_workspaceid
 * @property integer $relid
 * @property string $name
 * @property string $dis
 * @property string $startd
 * @property string $endd
 * @property string $state
 *
 * The followings are the available model relations:
 * @property Defect[] $defects
 * @property LoginWorkspace $loginWorkspace
 * @property Release $rel
 * @property UserStories[] $userStories
 */
class Iteration extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'iteration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login_workspaceid', 'required'),
			array('login_workspaceid, relid', 'numerical', 'integerOnly'=>true),
			array('name, dis, state', 'length', 'max'=>255),
			array('startd, endd', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login_workspaceid, relid, name, dis, startd, endd, state', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'defects' => array(self::HAS_MANY, 'Defect', 'sstate'),
			'loginWorkspace' => array(self::BELONGS_TO, 'LoginWorkspace', 'login_workspaceid'),
			'rel' => array(self::BELONGS_TO, 'Release', 'relid'),
			'userStories' => array(self::HAS_MANY, 'UserStories', 'iterationid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login_workspaceid' => 'Login Workspaceid',
			'relid' => 'Relid',
			'name' => 'Name',
			'dis' => 'Dis',
			'startd' => 'Startd',
			'endd' => 'Endd',
			'state' => 'State',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login_workspaceid',$this->login_workspaceid);
		$criteria->compare('relid',$this->relid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('dis',$this->dis,true);
		$criteria->compare('startd',$this->startd,true);
		$criteria->compare('endd',$this->endd,true);
		$criteria->compare('state',$this->state,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Iteration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
