<?php

/**
 * This is the model class for table "team".
 *
 * The followings are the available columns in table 'team':
 * @property integer $id
 * @property integer $wid
 * @property integer $uid
 * @property string $role
 * @property string $phone
 * @property string $disn_name
 * @property string $img
 * @property string $deptt
 * @property string $officeLoc
 * @property string $email
 *
 * The followings are the available model relations:
 * @property Login[] $logins
 * @property LoginWorkspace $w
 * @property User $u
 */
class Team extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'team';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'required'),
			array('wid, uid', 'numerical', 'integerOnly'=>true),
			array('role, disn_name, img, deptt, officeLoc', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>20),
			array('email', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, wid, uid, role, phone, disn_name, img, deptt, officeLoc, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'logins' => array(self::HAS_MANY, 'Login', 'teamid'),
			'w' => array(self::BELONGS_TO, 'LoginWorkspace', 'wid'),
			'u' => array(self::BELONGS_TO, 'User', 'uid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'wid' => 'Wid',
			'uid' => 'Uid',
			'role' => 'Role',
			'phone' => 'Phone',
			'disn_name' => 'Disn Name',
			'img' => 'Img',
			'deptt' => 'Deptt',
			'officeLoc' => 'Office Loc',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('wid',$this->wid);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('disn_name',$this->disn_name,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('deptt',$this->deptt,true);
		$criteria->compare('officeLoc',$this->officeLoc,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Team the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
