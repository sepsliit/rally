<?php

/**
 * This is the model class for table "release".
 *
 * The followings are the available columns in table 'release':
 * @property integer $id
 * @property integer $wid
 * @property string $name
 * @property string $sdate
 * @property string $edate
 * @property string $color
 *
 * The followings are the available model relations:
 * @property Iteration[] $iterations
 * @property Workspace $w
 */
class Release extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'release';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('wid, name, sdate, edate, color', 'required'),
			array('wid', 'numerical', 'integerOnly'=>true),
			array('name, color', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, wid, name, sdate, edate, color', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iterations' => array(self::HAS_MANY, 'Iteration', 'relid'),
			'w' => array(self::BELONGS_TO, 'Workspace', 'wid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'wid' => 'Wid',
			'name' => 'Name',
			'sdate' => 'Sdate',
			'edate' => 'Edate',
			'color' => 'Color',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('wid',$this->wid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('sdate',$this->sdate,true);
		$criteria->compare('edate',$this->edate,true);
		$criteria->compare('color',$this->color,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Release the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
