<?php
/* @var $this TrackController */
/* @var $model Track */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_storiesid'); ?>
		<?php echo $form->textField($model,'user_storiesid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'def'); ?>
		<?php echo $form->textField($model,'def'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'inp'); ?>
		<?php echo $form->textField($model,'inp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'comp'); ?>
		<?php echo $form->textField($model,'comp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'acc'); ?>
		<?php echo $form->textField($model,'acc'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->