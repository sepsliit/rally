<?php
/* @var $this TrackController */
/* @var $model Track */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'track-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_storiesid'); ?>
		<?php echo $form->textField($model,'user_storiesid'); ?>
		<?php echo $form->error($model,'user_storiesid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'def'); ?>
		<?php echo $form->textField($model,'def'); ?>
		<?php echo $form->error($model,'def'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inp'); ?>
		<?php echo $form->textField($model,'inp'); ?>
		<?php echo $form->error($model,'inp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comp'); ?>
		<?php echo $form->textField($model,'comp'); ?>
		<?php echo $form->error($model,'comp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'acc'); ?>
		<?php echo $form->textField($model,'acc'); ?>
		<?php echo $form->error($model,'acc'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->