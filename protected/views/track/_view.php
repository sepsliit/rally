<?php
/* @var $this TrackController */
/* @var $data Track */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_storiesid')); ?>:</b>
	<?php echo CHtml::encode($data->user_storiesid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('def')); ?>:</b>
	<?php echo CHtml::encode($data->def); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inp')); ?>:</b>
	<?php echo CHtml::encode($data->inp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comp')); ?>:</b>
	<?php echo CHtml::encode($data->comp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('acc')); ?>:</b>
	<?php echo CHtml::encode($data->acc); ?>
	<br />


</div>