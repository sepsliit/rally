<?php
/* @var $this TaskController */
/* @var $model Task */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'task-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_storiesid'); ?>
		<?php echo $form->textField($model,'user_storiesid'); ?>
		<?php echo $form->error($model,'user_storiesid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tname'); ?>
		<?php echo $form->textField($model,'tname',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dis'); ?>
		<?php echo $form->textField($model,'dis',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'dis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'owner'); ?>
		<?php echo $form->textField($model,'owner',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'owner'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'est'); ?>
		<?php echo $form->textField($model,'est',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'est'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stage'); ?>
		<?php echo $form->textField($model,'stage',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'stage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'breason'); ?>
		<?php echo $form->textField($model,'breason',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'breason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'todo'); ?>
		<?php echo $form->textField($model,'todo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'todo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->