
<div class="col-lg-6">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
</div>
<section class="content">


    <div class="col-lg-12" >


        <div class="box col-lg-12" >

            <div class="row">
                <div class="col-xs-3" style="margin: 20px 0 20px 0;">
                    <button class="btn btn-warning" data-toggle="modal" data-target="#myModal"><i class="fa fa-lg fa-plus-square-o"></i>  Add User Stories</button>
                </div>
            </div>
            <?php foreach($dataProvider as $row):  ?>
            <div class="row">
                <div class="col-lg-5 col-md-5  col-lg-offset-1 col-md-offset-1">
                    <div  class="us box box-solid box-info">
                        <div class="box-header" style="height: 30px;" >

                            <div class="box-tools pull-right">

                                <button class="btn btn-info btn-sm" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button class="btn btn-info btn-sm" data-widget="remove">
                                    <i class="fa fa-times"></i>
                                </button>


                            </div>

                            <h3  class="box-title" data-toggle="tooltip" data-placement="right" html=true title="Description: <?php echo "US".$row->dis; ?> "><span class="fa fa-book"></span></h3><h4> <?php echo "US".$row->id; ?></h4>


                        </div>
                        <div class="bodys box-body " data-placement="bottom"  id="<?php echo $row->id; ?>" >
                            <h5><?php echo Yii::app()->user->getState("username"); ?></h5>
                           <span style="font-size: 11px;"><?php echo "US".$row->uname; ?></span>



                            <div class="row">
                                <div id="mn<?php echo $row->id; ?>" class="menu col-lg-10 col-lg-offset-1 " >
                                    <?php $task = Task::model()->findAllByAttributes(array('user_storiesid'=>$row->id));


                                    ?>
                                    <div class="row">
                                        <div class="col-lg-12">

                                            <div class="btn-group test1">
                                                <button class="btn btn-info btn-flat" type="button">Task</button>
                                                <button class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown" style="height: 34px;" type="button">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>



                                                        <ul class="test dropdown-menu" style="min-width: 100px;" >
                                                            <div class="box-body no-padding">
                                                            <table id='tab<?php echo $row->id; ?>' class='table'>
                                                                <tbody>
                                                                <tr>
                                                                    <th style='width: 10px'>#</th>
                                                                    <th>Task Name</th>
                                                                    <th>Owner</th>
                                                                    <th>Stage</th>
                                                                    <th>State</th>
                                                                    <th>Block</th>
                                                                    <th>Todo</th>
                                                                    <th>Estimate</th>
                                                                    <th>Delete</th>
                                                                    <th>Edit</th>

                                                                </tr>
                                                                <?php

                                                                foreach($task as $row2 ):
                                                                    $taskID=$row2->id;
                                                                    $taskname=$row2->tname;
                                                                    $taskDes=$row2->dis;
                                                                    $towner=$row2->owner;
                                                                    $tstage=$row2->stage;
                                                                    $test=$row2->est;
                                                                    $tstate=$row2->state;
                                                                    $tblock=$row2->breason;
                                                                    $todo=$row2->todo;

                                                                    ?>

                                                                <tr data-toggle='tooltip' data-placement='left' data-html='true' id='des' title=' <div class=> Description </br><?php echo $taskDes;  ?></div> '>
                                                                    <td><?php echo $taskID; ?></td>
                                                                    <td  ><?php echo $taskname;  ?> </td>
                                                                    <td><?php echo $towner; ?></td>
                                                                    <td><?php echo $tstage ?></td>
                                                                    <td><?php echo $tstate ?></td>
                                                                    <td><?php echo $tblock ?></td>
                                                                    <td><?php echo $todo ?></td>
                                                                    <td> <i class='fa fa-clock-o' id='hh2'></i> <?php echo $test; ?></td>
                                                                    <td> <button class='btn btn-xs delete-task fa fa-trash-o' ></button></td>
                                                                    <td> <button class='btn btn-xs fa fa-edit edit-task' data-toggle='modal' data-target='#myModaledit' ></button> </td>

                                                                </tr>
                                                                <?php  endforeach; ?>

                                                                </tbody>
                                                            </table>
                                                            </div>
                                                        </ul>


                                               <!-- list view ends-->


                                            </div>

                                         </div>
                                        </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;  ?>



<!--task add form  ------>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 800px;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Add Task</h4>
                        </div>
                        <div class="modal-body">
                            <div id="tsuccess-alert" class="alert alert-success alert-dismissable">
                                <!--                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>-->
                                <!--                        <a class="alert-link" href="#">Alert Link</a>-->
                            </div>

                            <form id="myTaskForm" class="form-horizontal my_modal" >
                                <!--Task name -->
                                <div class="form-group">



                                    <label id="lblname" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="tname" name="tname" placeholder="Task Name"   data-validation="required" data-validation-error-msg=" Userstory name is required ">
                                    </div>
                                </div>


                                <!--Description -->

                                <div class="form-group">
                                    <label id="lbldes" class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">

                                        <textarea cols="50"    id="tdescription" name="tdescription" rows="5" data-validation="required" maxlength="500"data-validation-error-msg=" User Story description is required " class="form-control" ></textarea>


                                    </div>
                                </div>


                                <!--my description-->

                                <label  id="tmydescription"  >You have 500 characters remaining</label>





                                <!--Owner -->
                                <div class="form-group">


                                    <label  id="lblowner" class="col-sm-2 control-label">Owner</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="towner"  name="towner"   placeholder="Owner"  data-validation="required" data-validation-error-msg=" Owner is required ">
                                    </div>
                                </div>


                                <!--State-->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">State</label>
                                    <div class="col-sm-10">
                                        <select id="tstate" class="form-control drop_button " name="tstate"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                            <option value="defined">Defined</option>
                                            <option value="In progress">In progress</option>
                                            <option value="Completed">Completed</option>
                                            <option value="Accepted">Accepted</option>


                                        </select>

                                    </div>

                                </div>

                                <!--Iteration -->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Iteration</label>
                                    <div class="col-sm-10">
                                        <select id="titeration" class="form-control drop_button " name="titeration"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                            <option value="Unsheduled">Unsheduled</option>
                                            <option value="First Iteration">First Iteration</option>
                                            <option value="Second Iteration">Second Iteration</option>
                                            <option value="Third Iteration">Third Iteration</option>


                                        </select>

                                    </div>

                                </div>


                                <!-- blocked -->

                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Blocked</label>
                                    <div class="col-sm-10">
                                        <input type="checkbox">
                                    </div>
                                </div>


                                <!-- blocked reason -->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Blocked Reason</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="treason" class="form-control" id="treason" placeholder="Blocked reason">
                                    </div>
                                </div>


                                <!--Estimated hours-->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Plan Estimate(Hrs)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="testimate" id="testimate" placeholder="Estimated hours">
                                    </div>
                                </div>


                                <!--to do-->

                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">To Do(Hrs)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="todo" id="todo" placeholder="To Do Hours">
                                    </div>
                                </div>

                                <!--  Work product-->

                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Work Product</label>
                                    <div >
                                        <label   id="work" name="work"></label>
                                    </div>
                                </div>
                                <!--   userstory id-->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">UserStory ID</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="usid" id="usid" >
                                    </div>
                                </div>



                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" id="save_task" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <!--end task add form  -->

<!--task edit-->
            <div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content" style="width: 800px;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Create Task</h4>
                        </div>
                        <div class="modal-body">
                            <div id="edit-success-alert" class="alert alert-success alert-dismissable">
                                <!--                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>-->
                                <!--                        <a class="alert-link" href="#">Alert Link</a>-->
                            </div>

                            <form id="myTaskFormEdit" class="form-horizontal my_modal" >
                                <!--Task name -->
                                <div class="form-group">



                                    <label id="lblname" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="tename" name="tename" placeholder="Task Name"   data-validation="required" data-validation-error-msg=" Userstory name is required ">
                                    </div>
                                </div>


                                <!--Description -->

                                <div class="form-group">
                                    <label id="lbldes" class="col-sm-2 control-label">Description</label>
                                    <div class="col-sm-10">

                                        <textarea cols="50"    id="tedescription" name="tedescription" rows="5" data-validation="required" maxlength="500"data-validation-error-msg=" User Story description is required " class="form-control" ></textarea>


                                    </div>
                                </div>


                                <!--my description-->

                                <label  id="tmydescription"  >You have 500 characters remaining</label>





                                <!--Owner -->
                                <div class="form-group">


                                    <label  id="lblowner" class="col-sm-2 control-label">Owner</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="teowner"  name="teowner"   placeholder="Owner"  data-validation="required" data-validation-error-msg=" Owner is required ">
                                    </div>
                                </div>


                                <!--State-->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">State</label>
                                    <div class="col-sm-10">
                                        <select id="testate" class="form-control drop_button " name="testate"  id="testate" style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                            <option value="defined">Defined</option>
                                            <option value="In progress">In progress</option>
                                            <option value="Completed">Completed</option>
                                            <option value="Accepted">Accepted</option>


                                        </select>

                                    </div>

                                </div>

                                <!--Iteration -->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Iteration</label>
                                    <div class="col-sm-10">
                                        <select id="teiteration" class="form-control drop_button " name="teiteration"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                            <option value="Unsheduled">Unsheduled</option>
                                            <option value="First Iteration">First Iteration</option>
                                            <option value="Second Iteration">Second Iteration</option>
                                            <option value="Third Iteration">Third Iteration</option>


                                        </select>

                                    </div>

                                </div>


                                <!-- blocked -->

                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Blocked</label>
                                    <div class="col-sm-10">
                                        <input type="checkbox">
                                    </div>
                                </div>


                                <!-- blocked reason -->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Blocked Reason</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="tereason" class="form-control" id="tereason" placeholder="Blocked reason">
                                    </div>
                                </div>


                                <!--Estimated hours-->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Plan Estimate(Hrs)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="teestimate" id="teestimate" placeholder="Estimated hours">
                                    </div>
                                </div>


                                <!--to do-->

                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">To Do(Hrs)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="etodo" id="etodo" placeholder="To Do Hours">
                                    </div>
                                </div>

                                <!--  Work product-->

                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Work Product</label>
                                    <div >
                                        <label   id="ework" name="ework"></label>
                                    </div>
                                </div>
                                <!--   userstory id-->
                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">UserStory ID</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="eusid" id="eusid" >
                                    </div>
                                </div>

                                <!--task id-->

                                <div class="form-group">


                                    <label  class="col-sm-2 control-label">Task ID</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="tid" id="tid" />
                                    </div>
                                </div>



                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" id="updatetask" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

<!-- end task edit-->
</section>

