<?php
/* @var $this TaskController */
/* @var $data Task */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_storiesid')); ?>:</b>
	<?php echo CHtml::encode($data->user_storiesid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tname')); ?>:</b>
	<?php echo CHtml::encode($data->tname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dis')); ?>:</b>
	<?php echo CHtml::encode($data->dis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner')); ?>:</b>
	<?php echo CHtml::encode($data->owner); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('est')); ?>:</b>
	<?php echo CHtml::encode($data->est); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('stage')); ?>:</b>
	<?php echo CHtml::encode($data->stage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('breason')); ?>:</b>
	<?php echo CHtml::encode($data->breason); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('todo')); ?>:</b>
	<?php echo CHtml::encode($data->todo); ?>
	<br />

	*/ ?>

</div>