
<?php $id=Yii::app()->user->getState('userId');

$connection=Yii::app()->db;   // assuming you have configured a "db" connection
// If not, you may explicitly create a connection:
// $connection=new CDbConnection($dsn,$username,$password);
$command=$connection->createCommand("select * from team where uid='".$id."'");
$command_admin=$connection->createCommand("select * from user where id='".$id."'");
$dataReader2_admin=$command_admin->query();
$command3=$connection->createCommand("select * from team where uid='".$id."'");
$command3_admin=$connection->createCommand("select * from user where id='".$id."'");
$dataReader2=$command->query();
$dataReader3=$command3->query();
$dataReader3_admin=$command3_admin->query();


?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Task</h4>
            </div>
            <div class="modal-body">
                <div id="tsuccess-alert" class="alert alert-success alert-dismissable">

                </div>

                <form id="myTaskForm" name="myTaskForm" class="form-horizontal my_modal" >
                    <!--Task name -->
                    <div class="form-group">



                        <label id="lblname" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="tname" name="tname" placeholder="Task Name"  >
                        </div>
                    </div>


                    <!--Description -->

                    <div class="form-group">
                        <label id="lbldes" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">

                            <textarea cols="50"    id="tdescription" name="tdescription" rows="5"  class="form-control" ></textarea>


                        </div>
                    </div>


                    <!--my description-->

                    <label  id="tmydescription"  >You have 500 characters remaining</label>





                    <!--Owner -->
                    <div class="form-group">


                        <label  id="lblowner" class="col-sm-2 control-label">Owner</label>
                        <div class="col-sm-10">
                            <select id="towner" class="form-control drop_button " name="towner"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <?php foreach($dataReader2_admin as $row ):?>
                                    <option value="<?php echo $row['disn_name'];?>"><?php echo $row['disn_name'];?></option>
                                <?php endforeach;?>
                                <?php foreach($dataReader2 as $row ):?>
                                    <option value="<?php echo $row['disn_name'];?>"><?php echo $row['disn_name'];?></option>
                                <?php endforeach;?>
                            </select>                        </div>
                    </div>


                    <!--State-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">State</label>
                        <div class="col-sm-10">
                            <select id="tstate" class="form-control drop_button " name="tstate"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <option value="defined">Defined</option>
                                <option value="In progress">In progress</option>
                                <option value="Completed">Completed</option>
                                <option value="Accepted">Accepted</option>


                            </select>

                        </div>

                    </div>

                    <!--Iteration -->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Iteration</label>
                        <div class="col-sm-10">
                            <select id="titeration" class="form-control drop_button " name="titeration"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <option value="Unsheduled">Unsheduled</option>
                                <option value="First Iteration">First Iteration</option>
                                <option value="Second Iteration">Second Iteration</option>
                                <option value="Third Iteration">Third Iteration</option>


                            </select>

                        </div>

                    </div>


                    <!-- blocked -->

                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Blocked</label>
                        <div class="col-sm-10">
                            <input type="checkbox">
                        </div>
                    </div>


                    <!-- blocked reason -->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Blocked Reason</label>
                        <div class="col-sm-10">
                            <input type="text" name="treason" class="form-control" id="treason" placeholder="Blocked reason">
                        </div>
                    </div>


                    <!--Estimated hours-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Plan Estimate(Hrs)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="testimate" id="testimate" placeholder="Estimated hours">
                        </div>
                    </div>


                    <!--to do-->

                    <div class="form-group">


                        <label  class="col-sm-2 control-label">To Do(Hrs)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="todo" id="todo" placeholder="To Do Hours">
                        </div>
                    </div>

                    <!--  Work product-->

                    <!--                                <div class="form-group">-->
                    <!---->
                    <!---->
                    <!--                                    <label  class="col-sm-2 control-label">Work Product</label>-->
                    <!--                                    <div >-->
                    <!--                                        <label   id="work" name="work"></label>-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                    <!--   userstory id-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">UserStory ID</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="usid" id="usid" readonly>
                        </div>
                    </div>



                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="save_task" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!--end task add form  -->

<!--task edit-->
<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Task</h4>
            </div>
            <div class="modal-body">
                <div id="edit-success-alert" class="alert alert-success alert-dismissable">

                </div>

                <form id="myTaskFormEdit" class="form-horizontal my_modal" >
                    <!--Task name -->
                    <div class="form-group">



                        <label id="lblname" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="tename" name="tename" placeholder="Task Name"   data-validation="required" data-validation-error-msg=" Userstory name is required ">
                        </div>
                    </div>


                    <!--Description -->

                    <div class="form-group">
                        <label id="lbldes" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">

                            <textarea cols="50"    id="tedescription" name="tedescription" rows="5" data-validation="required" maxlength="500"data-validation-error-msg=" User Story description is required " class="form-control" ></textarea>


                        </div>
                    </div>


                    <!--my description-->

                    <label  id="tmydescription"  >You have 500 characters remaining</label>





                    <!--Owner -->
                    <div class="form-group">


                        <label  id="lblowner" class="col-sm-2 control-label">Owner</label>
                        <div class="col-sm-10">
                            <select id="etowner" class="form-control drop_button " name="etowner"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <?php foreach($dataReader3_admin as $row ):?>
                                    <option value="<?php echo $row['disn_name'];?>"><?php echo $row['disn_name'];?></option>
                                <?php endforeach;?>
                                <?php foreach($dataReader3 as $row ):?>
                                    <option value="<?php echo $row['disn_name'];?>"><?php echo $row['disn_name'];?></option>
                                <?php endforeach;?>
                            </select>

                        </div>
                    </div>


                    <!--State-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">State</label>
                        <div class="col-sm-10">
                            <select id="testate" class="form-control drop_button " name="testate"  id="testate" style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <option value="defined">Defined</option>
                                <option value="In progress">In progress</option>
                                <option value="Completed">Completed</option>
                                <option value="Accepted">Accepted</option>


                            </select>

                        </div>

                    </div>

                    <!--Iteration -->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Iteration</label>
                        <div class="col-sm-10">
                            <select id="teiteration" class="form-control drop_button " name="teiteration"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <option value="Unsheduled">Unsheduled</option>
                                <option value="First Iteration">First Iteration</option>
                                <option value="Second Iteration">Second Iteration</option>
                                <option value="Third Iteration">Third Iteration</option>


                            </select>

                        </div>

                    </div>


                    <!-- blocked -->

                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Blocked</label>
                        <div class="col-sm-10">
                            <input type="checkbox">
                        </div>
                    </div>


                    <!-- blocked reason -->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Blocked Reason</label>
                        <div class="col-sm-10">
                            <input type="text" name="tereason" class="form-control" id="tereason" placeholder="Blocked reason">
                        </div>
                    </div>


                    <!--Estimated hours-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Plan Estimate(Hrs)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="teestimate" id="teestimate" placeholder="Estimated hours">
                        </div>
                    </div>


                    <!--to do-->

                    <div class="form-group">


                        <label  class="col-sm-2 control-label">To Do(Hrs)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="etodo" id="etodo" placeholder="To Do Hours">
                        </div>
                    </div>

                    <!--  Work product-->

                    <!--                                <div class="form-group">-->
                    <!---->
                    <!---->
                    <!--                                    <label  class="col-sm-2 control-label">Work Product</label>-->
                    <!--                                    <div >-->
                    <!--                                        <label   id="ework" name="ework"></label>-->
                    <!--                                    </div>-->
                    <!--                                </div>-->
                    <!--   userstory id-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">UserStory ID</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="eusid" readonly>
                        </div>
                    </div>

                    <!--task id-->

                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Task ID</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tid" id="tid" readonly>
                        </div>
                    </div>



                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="updatetask" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- end task edit-->
<!--iteration-->
<div class="modal fade" id="myiteration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Iteration</h4>
            </div>
            <div class="modal-body">
                <div id="itera-success-alert" class="alert alert-success alert-dismissable">

                </div>

                <form id="myTaskFormEdit" class="form-horizontal my_modal" >
                    <!--iteration name -->
                    <div class="form-group">
                        <label id="iter_name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="itename" name="tename" placeholder="Task Name"   data-validation="required" data-validation-error-msg=" Userstory name is required ">
                        </div>
                    </div>
                    <!--iteration Description -->
                    <div class="form-group">
                        <label id="lbldes" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea cols="50"    id="tedescription" name="tedescription" rows="5" data-validation="required" maxlength="500"data-validation-error-msg=" User Story description is required " class="form-control" ></textarea>
                        </div>
                    </div>
                    <!--starting and ending-->
                    <div class="form-group">
                        <label id="iter_name" class="col-sm-2 control-label">Date</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="date" name="tename" placeholder="Task Name"   data-validation="required" data-validation-error-msg=" Userstory name is required ">
                        </div>
                    </div>
                    <!--state-->
                    <div class="form-group">


                        <label  id="lblowner" class="col-sm-2 control-label">State</label>
                        <div class="col-sm-10">
                            <select id="etowner" class="form-control drop_button " name="etowner"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                    <option value="">Select State</option>
                            </select>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="updatetask" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<section class="content">

    <div class="row">
        <!--left column-->
        <div class="col-xs-4" >
            <div class=" main box box-success col-lg-12 " >

                <div class="row">
                    <div class="col-xs-3" style="margin: 20px 0 20px 0;">
                        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myiteration"><i class="fa fa-lg fa-plus-square-o"></i>  Add Iteration</button>
                    </div>
                    <div class="col-xs-3" style="margin: 20px 0 20px 0;">
<!--                        <button class="btn btn-warning" data-toggle="modal" data-target="#myModal"><i class="fa fa-lg fa-plus-square-o"></i>  Add User Stories</button>-->
                    </div>
                    <div class="col-xs-3 " style="margin: 20px 0 20px 0;">
                        <a id="userstorypdf" class="btn btn-default btn-sm " href="index.php?r=task/print">
                            <i class="fa fa-cloud-download"></i>
                            Generate PDF
                        </a>
                    </div>

                </div>
                <!--            user stories-->
                <ul id="user_story" style="min-height: 200px; " class="list-unstyled connectedSortable">
                    <?php foreach($dataProvider as $row):  ?>

                        <li id='<?php echo "li".$row->id; ?>' class="us1">
                            <div class=" ">
                                <div  class="us box box-info ">
                                    <div class="box-header" style="height: 30px;" >

                                        <div class="box-tools pull-right">

                                            <button class="btn btn-info btn-sm" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <button class="btn btn-info btn-sm" data-widget="remove">
                                                <i class="fa fa-times"></i>
                                            </button>


                                        </div>

                                        <h3  class="box-title" data-toggle="tooltip" data-placement="left" html=true title="Description: <?php echo "US".$row->dis; ?> "><span class="fa fa-book"></span></h3><h4> <?php echo "US".$row->id; ?></h4>


                                    </div>
                                    <div class="bodys box-body " data-placement="bottom"  id="<?php echo $row->id; ?>" >
                                        <div class="row">
                                            <div class="col-xs-5" style="">
                                                <h5><?php echo Yii::app()->user->getState("username"); ?></h5>

                                            </div>
                                            <div class="col-xs-2 col-xs-push-5" style="padding: 0;">
                                                <!--                                    <button data-id="--><?php //echo $row->id; ?><!--"  type="button" class="settings btn " data-container="body" data-toggle="popover" data-placement="right" >-->
                                                <!--                                        <span class="fa fa-gear"></span>-->
                                                <!--                                    </button>-->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-5">
                                                <span style="font-size: 11px;"><?php echo "US".$row->uname; ?></span>
                                            </div>
                                            <?php $task = Task::model()->findAllByAttributes(array('user_storiesid'=>$row->id));?>
                                            <div class="test col-xs-2 col-xs-push-5" style="padding: 0;">
                                                <button id="<?php echo $row->id; ?>" data-toggle="popover"  type="button" class="popover-dismiss task-btn btn "   data-html=true data-content="

                                    <button type='button' class='btn btn-xs btn-info' data-target='#myModal' data-toggle='modal'><span   class=' fa  fa-plus'></span>Add Tasks</button>
                                    <table id='tab<?php echo $row->id; ?>' class='table'>
                                    <tbody>
                                    <tr>
                                        <th style='width: 10px'>#</th>
                                        <th>Task Name</th>
                                        <th>Owner</th>
                                        <th>Stage</th>
                                        <th>State</th>
                                        <th>Block</th>
                                        <th>Todo</th>
                                        <th>Estimate</th>
                                        <th>Delete</th>
                                        <th>Edit</th>

                                    </tr>
                                    <?php

                                                foreach($task as $row2 ):
                                                    $taskID=$row2->id;
                                                    $taskname=$row2->tname;
                                                    $taskDes=$row2->dis;
                                                    $towner=$row2->owner;
                                                    $tstage=$row2->stage;
                                                    $test=$row2->est;
                                                    $tstate=$row2->state;
                                                    $tblock=$row2->breason;
                                                    $todo=$row2->todo;

                                                    ?>
                                        <input id='des<?php echo $taskID; ?>' type='hidden' value='<?php echo $taskDes;  ?>'>
                                        <tr id ='<?php echo $taskID; ?>' data-toggle='tooltip' data-placement='left' data-html='true'  data-content=' <div> Description </br><?php echo $taskDes;  ?></div> '>
                                            <td><?php echo $taskID; ?></td>
                                            <td  ><?php echo $taskname;  ?> </td>
                                            <td><?php echo $towner; ?></td>
                                            <td><?php echo $tstage ?></td>
                                            <td><?php echo $tstate ?></td>
                                            <td><?php echo $tblock ?></td>
                                            <td><?php echo $todo ?></td>
                                            <td> <i class='fa fa-clock-o' id='hh2'></i> <?php echo $test; ?></td>
                                            <td> <button class='btn btn-xs delete-task fa fa-trash-o' ></button></td>
                                            <td> <button class='btn btn-xs fa fa-edit edit-task' data-toggle='modal' data-target='#myModaledit' ></button> </td>

                                        </tr>
                                        <?php  endforeach; ?>

                                        </tbody>
                                    </table>

                                    ">

                                                    <span class="fa fa-tasks"></span>
                                                    <input type="hidden" class="id-us" value="<?php echo $row->id; ?>">

                                                </button>
                                            </div>
                                        </div>





                                    </div>
                                </div>
                            </div>
                        </li>


                    <?php endforeach;  ?>
                </ul>

            </div>
        </div>



        <!--right column-->
        <div class="col-xs-8" style="background-color: #006600;">


            <ul id="items">
                <li>
                 <div class="col-xs-12">
                                <div class="box box-danger col-xs-12" >
                                    <div class="box-header" title="" data-toggle="tooltip" data-original-title="Header tooltip">
                                        <h3 class="box-title">Primary Box (header tooltip)</h3>
                                        <div class="box-tools pull-right">
                                            <button class="btn btn-primary btn-xs" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <button class="btn btn-primary btn-xs" data-widget="remove">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <ol id="drop" class="box-body drop" style="height: 500px; width 100%; overflow-y: scroll; " >



                                    </ol>
                                </div>

                            </div>
                </li>
                <li>
                    <div class="col-xs-12">
                        <div class="box box-danger col-xs-12" >
                            <div class="box-header" title="" data-toggle="tooltip" data-original-title="Header tooltip">
                                <h3 class="box-title">Primary Box (header tooltip)</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button class="btn btn-primary btn-xs" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <ol id="drop" class="box-body drop" style="height: 500px; width 100%; overflow-y: scroll; " >



                            </ol>
                        </div>

                    </div>
                </li>
                <li>
                    <div class="col-xs-12">
                        <div class="box box-danger col-xs-12" >
                            <div class="box-header" title="" data-toggle="tooltip" data-original-title="Header tooltip">
                                <h3 class="box-title">Primary Box (header tooltip)</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button class="btn btn-primary btn-xs" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <ol id="drop" class="box-body drop" style="height: 500px; width 100%; overflow-y: scroll; " >



                            </ol>
                        </div>

                    </div>
                </li>
                <li>
                    <div class="col-xs-12">
                        <div class="box box-danger col-xs-12" >
                            <div class="box-header" title="" data-toggle="tooltip" data-original-title="Header tooltip">
                                <h3 class="box-title">Primary Box (header tooltip)</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-primary btn-xs" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button class="btn btn-primary btn-xs" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <ol id="drop" class="box-body drop" style="height: 500px; width 100%; overflow-y: scroll; " >



                            </ol>
                        </div>

                    </div>
                </li>
                <li>Page 5</li>
                <li>Page 6</li>
                <li>Page 7</li>
                <li>Page 8</li>
                <li>Page 9</li>
                <li>Page 10</li>
                <li>Page 11</li>
                <li>Page 12</li>
                <li>Page 13</li>
                <li>Page 14</li>
            </ul>


        </div>


        </div>




</section>
<script>
    $(function(){
        previoiusIndex = 0

//       $( "#user_story" ).accordion();
        $("#user_story li").draggable({
            appendTo: "body",
//            connectToSortable: "#user_story",
            revert: "invalid"
//            helper: "clone"
        });

        $( "#drop " ).droppable({
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            accept: "#user_story li",
            connectToSortable: "#drop,#user_story",
            drop: function( event, ui ) {
//                alert(ui.draggable.html())
//                $( this ).find( ".placeholder" ).remove();
//                $( this ).appendTo('<li>'+ui.draggable.html()+'<li/>' );
                id= $(ui.draggable).prop('id');
                $( "<li id='"+id+"' class='us1' ></li>" ).html( ui.draggable.html() ).appendTo( this );

//                $(ui.droppable).prop('.connectedSortable');
                $(ui.draggable).remove();
//
                initialorder = new Array()
                $(this).children().each(function(){
                    id=$(this).prop('id')
                    column=$(this).index()+1;
                    item = {}
                    item ["id"] = id;
                    item ["column"] = column;

                    initialorder.push(item);


                })

                $.each(initialorder,function(key,val){
//                    if(key=='id')
                    alert(val.id +' '+val.column )
                })
            }
        })

//        $(".drop").droppable({
//            accept: "#user_story li",
//            activeClass: "ui-state-default",
//            hoverClass: "ui-state-hover",
////            accept: ":not(.ui-sortable-helper)",
//            drop: function (event, ui) {
////                $( "<li></li>").appendTo( ui );
//
//                $( "<li></li>" ).text( ui.draggable.text() ).appendTo( this );
//
//            }
//
//        })

        $( "#drop, #user_story" ).sortable({
            connectWith: ".connectedSortable",
            sort:function(event, ui ){
//                alert($(this).children('.ui-sortable-helper').prop('id'))
            },
            stop: function(event, ui) {
//                alert("New position: " + ui.item.index());
//                alert(ui.item.prop('id'))
                if(this.id='drop'){
                    neworder = new Array()
                    $(this).children().each(function(){
                        id=$(this).prop('id')
                        column=$(this).index()+1;
                        item = {}
                        item ["id"] = id;
                        item ["column"] = column;

                        neworder.push(item);


                    })

                    $.each(neworder,function(key,val){
//                    if(key=='id')
                        alert(val.id +' '+val.column )
                    })
                }

            }

        }).disableSelection();

//        $( "#drop" ).sortable({
//            revert: true
//        });
//        $( "#drop" ).disableSelection();

    })
</script>

