<!doctype html>
<head>
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>



</head>
<body style="background-color: #ffffff">
<div class="row" style="background-color: #ffffff;" >
    <div class="col-md-4" >

        <div class="box box-danger" style="height: 580px;  margin-left: 145px;" >
            <div class="box-header">
                <h3 class="box-title">Donut Chart of User Stories</h3>
            </div>

            <div class="box-body chart-responsive">
                <div id="graph">

                </div>

            </div>


        </div>



    </div>
<!--  second box-->
    <?php $id=Yii::app()->user->getState('logId') ?>

    <?php
    $null=null;
    $connection=Yii::app()->db;   // assuming you have configured a "db" connection
    // If not, you may explicitly create a connection:
    // $connection=new CDbConnection($dsn,$username,$password);
    $command=$connection->createCommand("select * from user_stories where login_workspaceid='".$id."' and breason='".$null."'");
    $dataReader=$command->query();



    ?>

<script>

</script>

<form action="" method="post">


    <div class="col-md-5" >

        <div class="box box-danger" style="height:580px; margin-left: 150px;" >
            <div class="box-header">
                <h3 class="box-title">Generate Charts </h3>
            </div>

            <div class="box-body chart-responsive" style="overflow: hidden;">
                <div id="graphTask"> </div>

                <div style="margin-left:5px;" class="row">
                    <h5 class="col-sm-3"> Select User Story</h5>
                    <select style="margin-top: 10px;" id="unameSelect" name="unameSelect">
                        <option value="null" >Select US</option>
                        <?php foreach($dataReader as $row):?>


                            <option value="<?php  echo $row['id']?>" ><?php echo $row['uname']?></option>
                            <h3  class="box-title toolm" data-toggle="tooltip" data-placement="right" html=true title="Description: <?php echo $row['dis']; ?> "><span class="fa fa-book"></span></h3><h4> <?php echo $row['dis']; ?></h4>

                        <?php endforeach;?>
                    </select>


                </div>

                <div style="margin: 5px auto" class="col-sm-1">

                    <label class="colorlabel">Defined

                        <div class="input-group defineColor" style="width: 110px;" >
                            <input type="text"  class="form-control"  id="dcolor" name="dcolor" />
                            <span class="input-group-addon"><i></i></span>
                        </div>

                    </label>

                    <label class="colorlabel">InProgress

                        <div class="input-group inprogressColor" style="width: 110px;" >
                            <input type="text"  class="form-control"  id="icolor"  name="icolor"/>
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </label>


                    <label class="colorlabel">Accepted
                        <div class="input-group acceptedColor" style="width: 110px;" >
                            <input type="text"  class="form-control"  id="acolor" name="acolor" />
                            <span class="input-group-addon"><i></i></span>
                        </div>

                    </label>
                    <label class="colorlabel">Completed
                        <div class="input-group completedColor" style="width: 110px;" >
                            <input type="text"  class="form-control" id="ccolor"  name="ccolor" />
                            <span class="input-group-addon"><i></i></span>
                        </div>

                    </label>



                    <label class="colorlabel">Blocked
                        <div class="input-group blockedColor" style="width: 110px;" >
                            <input type="text"  class="form-control"  id="bcolor" name="bcolor" />
                            <span class="input-group-addon"><i></i></span>
                        </div>
                    </label>



                    <input type="button" class=".btn-lg btn-success " style="width:110px; height:35px;margin-left:5px;margin-top:20px;" value="Generate Chart" id="change">

                </div>

                <!--   generated graph-->
                <div class="col-sm-1" id="taskgraph">


                </div>



            </div>








        </div>



    </div>

</form>




</div>


<?php

$all="SELECT COUNT(*) FROM user_stories where login_workspaceid='".$id."' ";
$allUS = Yii::app()->db->createCommand($all)->queryScalar();


$defined = "SELECT COUNT(*) FROM user_stories where state='defined' AND login_workspaceid='".$id."' AND breason='".$null."' ";
$definedUS = Yii::app()->db->createCommand($defined)->queryScalar();
$inprogress = "SELECT COUNT(*) FROM user_stories where state='In progress' AND login_workspaceid='".$id."' AND breason='".$null."' ";
$inprogressUS = Yii::app()->db->createCommand($inprogress)->queryScalar();
$accepted = "SELECT COUNT(*) FROM user_stories where state='Accepted' AND login_workspaceid='".$id."' AND breason='".$null."' ";
$acceptedUS = Yii::app()->db->createCommand($accepted)->queryScalar();
$completed= "SELECT COUNT(*) FROM user_stories where state='Completed' AND login_workspaceid='".$id."'  AND breason='".$null."' ";
$completedUS = Yii::app()->db->createCommand($completed)->queryScalar();
$block="SELECT COUNT(*) FROM user_stories where  login_workspaceid='".$id."' AND breason!='".$null."' ";
$blockUS = Yii::app()->db->createCommand($block)->queryScalar();



?>





<script>
    $(document).ready(function(){
        $('.inprogressColor').colorpicker();
        $('.defineColor').colorpicker();
        $('.acceptedColor').colorpicker();
        $('.completedColor').colorpicker();
        $('.blockedColor').colorpicker();



        col=[  '#17c633'  ,
            '#c41bb3',
            '#ed8321',
            '#137cdd',
            '#f22626'
        ];

        <?php if($allUS!=0){ ?>

        var mchart= Morris.Donut({
            element: 'graph',
            data: [
                {value:<?php echo $definedUS/$allUS; ?>, label: 'Defined'},
                {value:<?php echo $acceptedUS/$allUS; ?>, label: 'Accepted'},
                {value:<?php echo $inprogressUS/$allUS; ?>, label: 'In progress'},
                {value:<?php echo $completedUS/$allUS; ?>, label: 'Completed'},
                {value:<?php echo $blockUS/$allUS; ?>, label: 'Blocked'}
            ],
            backgroundColor: '#ffffff',
            labelColor: '#060',
            colors: col,

            formatter: function (x) { return x + "%"}

        });

        <?php }else{?>
        //graph for task

        alert('You don\'t have any user stories');

        <?php }?>

});
</script>


</body>



