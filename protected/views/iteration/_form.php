<?php
/* @var $this IterationController */
/* @var $model Iteration */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'iteration-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'login_workspaceid'); ?>
		<?php echo $form->textField($model,'login_workspaceid'); ?>
		<?php echo $form->error($model,'login_workspaceid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dis'); ?>
		<?php echo $form->textField($model,'dis',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'dis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'startd'); ?>
		<?php echo $form->textField($model,'startd'); ?>
		<?php echo $form->error($model,'startd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'endd'); ?>
		<?php echo $form->textField($model,'endd'); ?>
		<?php echo $form->error($model,'endd'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->