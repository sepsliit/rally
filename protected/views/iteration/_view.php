<?php
/* @var $this IterationController */
/* @var $data Iteration */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_workspaceid')); ?>:</b>
	<?php echo CHtml::encode($data->login_workspaceid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dis')); ?>:</b>
	<?php echo CHtml::encode($data->dis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startd')); ?>:</b>
	<?php echo CHtml::encode($data->startd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endd')); ?>:</b>
	<?php echo CHtml::encode($data->endd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />


</div>