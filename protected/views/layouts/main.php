<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrapValidator.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/userStorie.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/morris/morris.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/colorpicker/bootstrap-colorpicker.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/pagination.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jstree/style.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/application.css" rel="stylesheet" type="text/css"/>

    <!-- font Awesome -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ionicons.min.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/datatables/dataTables.bootstrap.css" rel="stylesheet"
          type="text/css"/>

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
          rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/AdminLTE.css" rel="stylesheet" type="text/css"/>

    <!-- fullCalendar -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fullcalendar/fullcalendar.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/fullcalendar/fullcalendar.print.css" rel="stylesheet"
          type="text/css" media='print'/>
    <!-- date picker -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/datepicker/css/datepicker.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/daterangepicker/daterangepicker-bs3.css"
          rel="stylesheet" type="text/css"/>

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jQueryUI/jquery-ui-1.10.3.custom.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.appendGrid-1.3.5.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <!-- autocomplete   -->
    <!--    <link href="//raw.github.com/jharding/typeahead.js-bootstrap.css/master/typeahead.js-bootstrap.css" rel="stylesheet" media="screen">-->

    <!--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" ></script>-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jq.js" type="text/javascript"></script>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="skin-black">
<!-- header logo: style can be found in header.less -->
<header class="header">
<a href="index.html" class="logo">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->

    <?php echo CHtml::encode(Yii::app()->name); ?>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
<!-- Sidebar toggle button-->
<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>

<div class="navbar-right">
<ul class="nav navbar-nav">
<!-- Messages: style can be found in dropdown.less-->


<li class="dropdown messages-menu">


    <?php
    $wid = Yii::app()->user->getState("logId");
    $id = Yii::app()->user->getState("userId");
    //var_dump($id);
    //inbox count
    //$query = "select count(n.notifi_id) as inboxc from notification_table n JOIN notification_update u ON (n.notifi_id = u.notificationid ) where u.states=0 and n.notifi_type=0 AND u.userid = '' ";
    $inboxCres = NotificationTable::model()->findAll(array(
        'select' => 'notifi_id',
        'condition' => 'notifi_id NOT IN (SELECT notificationid FROM notification_update u WHERE u.userid = :userid)  AND notifi_type =0',
        'params' => array(':userid' => $id)
    ));

    //echo $query;
    // $inboxCres = Yii::app()->db->createCommand($query)->queryRow();
    $inbc = count($inboxCres);


    // member reg count
    // $query = "select ifnull(count(n.notifi_id),0) as memc from notification_table n where n.state=0 and n.notifi_type=2 ";
    //echo $query;
    $inboxCres = NotificationTable::model()->findAll(array(
        'select' => 'notifi_id',
        'condition' => 'notifi_id NOT IN (SELECT notificationid FROM notification_update u WHERE u.userid = :userid)  AND notifi_type =2 and wid = :wid',
        'params' => array(':userid' => $id, ':wid' => $wid)
    ));
    $memc = count($inboxCres);

    // user stories
    //$query = "select ifnull(count(n.notifi_id),0) as story from notification_table n where n.state=0 and n.notifi_type=1";
    //echo $query;
    $inboxCres = NotificationTable::model()->findAll(array(
        'select' => 'notifi_id',
        'condition' => 'notifi_id NOT IN (SELECT notificationid FROM notification_update u WHERE u.userid = :userid)  AND notifi_type =1 and wid = :wid',
        'params' => array(':userid' => $id, ':wid' => $wid)
    ));

    $story = count($inboxCres);


    ?>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-envelope"></i>
        <span class="label label-success"><?php echo $inbc; ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <?php echo $inbc; ?> messages</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">


                <?php

                //$command = Yii::app()->db->createCommand("select * from notification_table where notifi_type =0 and state=0 order by added_date desc limit 0,10");
                $wid = Yii::app()->user->getState("logId");
                $res = NotificationTable::model()->findAll(array(
                    'condition' => 'notifi_id NOT IN (SELECT notificationid FROM notification_update u WHERE u.userid = :userid)  AND notifi_type =0',
                    'order' => 'added_date desc',
                    'limit' => 10,
                    'params' => array(':userid' => $id)
                ));

                foreach ($res as $row) {

                    ?>


                    <li><!-- start message -->
                        <a href="index.php?r=mail/index">
                            <div class="pull-left">
                                <img src="img/avatar3.png" class="img-circle" alt="User Image"/>
                            </div>
                            <h4>
                                <span><?php echo $row['headertext'] ?></span>
                                <small><i
                                        class="fa fa-clock-o"></i><span><?php echo date("h:m a", strtotime($row['added_date'])); ?></span>
                                </small>
                            </h4>
                            <p style="width: 200px;overflow: hidden"><?php echo $row['subtext'] ?></p>
                        </a>
                    </li>





                <?php
                }

                ?>

            </ul>
        </li>
        <li class="footer"><a href="#">See All Messages</a></li>
    </ul>
</li>

<!-- Notifications: style can be found in dropdown.less -->

<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-warning"></i>
        <span class="label label-warning"><?php echo $memc; ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header"><?php echo $memc; ?> new defects</li>
        <li>
            <ul class="menu">
                <?php
                $wid = Yii::app()->user->getState("logId");
                $res = NotificationTable::model()->findAll(array(
                    'condition' => 'notifi_id NOT IN (SELECT notificationid FROM notification_update u WHERE u.userid = :userid)  AND notifi_type =2 AND wid = :wid ',
                    'params' => array(':userid' => $id, ':wid' => $wid),
                    'order' => 'added_date desc',
                    'limit' => 10,
                    'params' => array(':userid' => $id, ':wid' => $wid)
                ));
                foreach ($res as $row) {

                    ?>



                    <li>
                        <!--                        <a href="index.php?r=user/index&notifi=-->
                        <?php //echo $row['notifi_id']; ?><!--">-->
                        <a href="index.php?r=mydefect&id=<?php echo $row['notifi_id'] . "&subid=" . $row['subj_id'] . "#" . $row['subj_id']; ?>">
                            <i class="ion ion-bug"></i>

                            <p style="width: 200px;overflow: hidden;display: inline-block;margin-bottom: 0px;padding-top: 0px;">
                                <b> <?php echo $row['headertext'] . " </b> |   " . date("M d h:m a", strtotime($row['added_date'])); ?>
                                    <br> <?php echo $row['subtext']; ?></p>
                        </a>
                    </li>



                <?php
                }

                ?>
            </ul>
        </li>
        <li class="footer"><a href="#">View all</a></li>
    </ul>
</li>


<!-- Tasks: style can be found in dropdown.less -->
<li class="dropdown tasks-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-tasks"></i>
        <span class="label label-danger"><?php echo $story; ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header"><?php echo $story; ?> new user stories been added</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">

                <?php
                $wid = Yii::app()->user->getState("logId");

                $res = NotificationTable::model()->findAll(array(
                    'condition' => 'notifi_id NOT IN (SELECT notificationid FROM notification_update u WHERE u.userid = :userid)  AND notifi_type =1 AND wid = :wid',
                    'params' => array(':userid' => $id, ':wid' => $wid),
                    'order' => 'added_date desc',
                    'limit' => 10,
                    'params' => array(':userid' => $id, ':wid' => $wid)
                ));
                foreach ($res as $row) {

                    ?>




                    <li>
                        <a href="index.php?r=workspace/addUserStory&id=<?php echo $row['notifi_id'] . "&subid=" . $row['subj_id'] . "#" . $row['subj_id']; ?>">
                            <h3>
                                <?php echo $row['headertext'] . "  |   " . date("M d h:m a", strtotime($row['added_date'])); ?>
                                <small class="pull-right">0% Complete</small>
                            </h3>
                            <div class="progress xs" style="margin-top: 5px ">
                                <div class="progress-bar progress-bar-aqua" style="width: 0%" role="progressbar"
                                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                    <span class="sr-only">0% Complete</span>
                                </div>
                            </div>
                        </a>
                    </li><!-- end task item -->


                <?php
                }

                ?>


            </ul>
        </li>
        <li class="footer">
            <a href="#">View all tasks</a>
        </li>
    </ul>
</li>

<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="glyphicon glyphicon-user"></i>
        <span><?php echo Yii::app()->user->getState("username"); ?> <i class="caret"></i></span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->

        <li class="user-header bg-light-blue">
            <img src="img/avatar3.png" class="img-circle" alt="User Image"/>

            <p>
                <?php echo Yii::app()->user->getState("username"); ?> - Admin
                <small><?php echo date("Y-m-d"); ?></small>
            </p>
        </li>
        <!-- Menu Body -->
        <!--        <li class="user-body">-->
        <!--            <div class="col-xs-4 text-center">-->
        <!--                <a href="#">Followers</a>-->
        <!--            </div>-->
        <!--            <div class="col-xs-4 text-center">-->
        <!--                <a href="#">Sales</a>-->
        <!--            </div>-->
        <!--            <div class="col-xs-4 text-center">-->
        <!--                <a href="#">Friends</a>-->
        <!--            </div>-->
        <!--        </li>-->
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
                <a href="index.php?r=site/logout" class="btn btn-default btn-flat">Sign out</a>
            </div>
        </li>
    </ul>
</li>
</ul>
</div>
</nav>
</header>

<div class="wrapper row-offcanvas row-offcanvas-left view">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="img/avatar3.png" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p><?php echo Yii::app()->user->getState("username"); ?></p>
                    <input id="userid" type="hidden" value="<?php echo Yii::app()->user->getState("userId"); ?>">
                    <input id="wid" type="hidden" value="<?php echo Yii::app()->user->getState("logId"); ?>">

                    <!--                    <a href="#"><i class="fa fa-circle text-success"></i> -->
                    <?php //echo Yii::app()->user->getState("userId");?><!--</a>-->
                    <a href="#" id="userid"> </a>
                </div>
            </div>
            <!-- search form -->
            <!--            <form action="#" method="get" class="sidebar-form">-->
            <!--                <div class="input-group">-->
            <!--                    <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
            <!--                            <span class="input-group-btn">-->
            <!--                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>-->
            <!--                            </span>-->
            <!--                </div>-->
            <!--            </form>-->
            <!-- /.search form -->

            <div id="project">
                <!--                <ul>-->
                <!--                    <li>Own project-->
                <!--                        <ul>-->
                <!--                            <li>Child node 1</li>-->
                <!--                            <li><a href="#">Child node 2</a></li>-->
                <!--                            <li><a href="#">Child node 2</a></li>-->
                <!--                            <li><a href="#">Child node 2</a></li>-->
                <!--                        </ul>-->
                <!--                    </li>-->
                <!--                    <li>Assigned Project-->
                <!--                        <ul>-->
                <!--                            <li>Child node 1</li>-->
                <!--                            <li><a href="#">Child node 2</a></li>-->
                <!--                        </ul>-->
                <!--                    </li>-->
                <!--                </ul>-->
            </div>

            <?php $this->widget('zii.widgets.CMenu', array(
                'id' => 'side-menu',
                'encodeLabel' => false,
                'items' => array(
                    array('label' => '<i class="fa fa-dashboard fa-fw "></i> Home', 'url' => array('site/index')),
                    array('label' => '<i class="fa fa-th "></i> <span>User</span> <small class="badge pull-right bg-green">new</small>', 'url' => array('/user/index'), 'visible' => (Yii::app()->user->getState("usertype") != "team")),
                    array('label' => '<i class="fa fa-folder-open "></i> <span>User Stories</span>', 'url' => array('/workspace/addUserStory')),
                    array('label' => '<i class="fa fa-ban "></i> <span>Blocked User Stories</span>', 'url' => array('/blockedwork/index'), 'visible' => (Yii::app()->user->name != "enduser")),
                    array('label' => '<i class="fa fa-puzzle-piece "></i> <span>Manage Stories</span>', 'url' => array('/task/index'), 'visible' => (Yii::app()->user->name != "enduser")),
                    array('label' => '<i class="fa fa-tasks "></i> <span>Track Iteration</span>', 'url' => array('/workspace/track'), 'visible' => (Yii::app()->user->name != "enduser")),
                    array('label' => '<i class="fa fa-puzzle-piece"></i> <span>My Tasks</span>', 'url' => array('/workspace/myTask'), 'visible' => (Yii::app()->user->name != "enduser")),
                    array('label' => '<i class="fa fa-users"></i> <span>Discussion</span>', 'url' => array('/discussion/index'), 'visible' => (Yii::app()->user->name != "enduser")),

                   array('label' => '<i class="fa fa-wheelchair fa-lg"></i> <span>Defects</span>', 'url' => array('/mydefect'), 'visible' => (Yii::app()->user->name != "enduser")),

                    array('label' => '<i class="fa fa-book"></i> <span>Mail</span>', 'url' => array('/mail/index'), 'visible' => (Yii::app()->user->name != "enduser")),
                    array('label' => '<i class="fa fa-bar-chart-o "></i> <span>Chart</span>', 'url' => array('/task/charts'), 'visible' => (Yii::app()->user->name != "enduser")),

//                    array('label' => '<i class="fa fa-bar-chart-o"></i> <span>Invoice</span>' , 'url' => array('/invoice/index'), 'visible' => (Yii::app()->user->name != "enduser")),

//                                <span>Test</span>
//                                <i class="fa fa-angle-left pull-right"></i>', 'url' => '#', 'itemOptions'=>array('class'=>'treeview'),
//                    'items' => array(
//                array('label' => '<i class="fa fa-laptop"></i>
//                    array('label' => '<i class="fa fa-angle-double-right"></i> General', 'url' => array('/device/mileage')),
//                    array('label' => '<i class="fa fa-angle-double-right"></i> Icons', 'url' => array('/device/speed')),
//                    array('label' => '<i class="fa fa-angle-double-right"></i> Buttons', 'url' => array('/device/playback')),
//                    array('label' => '<i class="fa fa-angle-double-right"></i> Sliders', 'url' => array('/device/playback')),
//                    array('label' => '<i class="fa fa-angle-double-right"></i> Timeline', 'url' => array('/device/playback')),
//
//
//                    ),
//                ),
                    array('label' => '<i class="fa fa-edit"></i> <span>Releases</span>
                                <i class="fa fa-angle-left pull-right"></i>', 'url' => '#', 'itemOptions' => array('class' => 'treeview'),
                        'items' => array(
                            array('label' => '<i class="fa fa-angle-double-right"></i> Calender', 'url' => array('/workspace/calender')),
                            array('label' => '<i class="fa fa-angle-double-right"></i> Resease Backlog', 'url' => array('/workspace/release')),


                        ),
                    ),
//                array('label' => '<i class="fa fa-user fa-fw"></i> Users', 'url' => array('/user/update')),
//                array('label' => '<i class="fa fa-bar-chart-o"></i> Reports<span class="fa arrow"></span>', 'url' => '#',
//                    'items' => array(
//                        array('label' => 'Mileage', 'url' => array('/device/mileage')),
//                        array('label' => 'Speed', 'url' => array('/device/speed')),
//                        array('label' => 'Playback', 'url' => array('/device/playback')),
//                        array('label' => 'Parking', 'url' => array('/device/park')),
//                    ),
//                ),
//                array('label' => '<i class="fa fa-signal fa-fw"></i> Statistics', 'url' => array('/site/login')),
//                    array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                ), 'htmlOptions' => array('class' => 'sidebar-menu '),
                'submenuHtmlOptions' => array('class' => 'treeview-menu ',

                ),

            )); ?>


        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side ">

        <?php echo $content; ?>


    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<!-- add new calendar event modal -->


<!-- jQuery 2.0.2 -->

<!-- jQuery UI 1.10.3 -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!--<script src="-->
<?php //echo Yii::app()->request->baseUrl; ?><!--/js/jquery.easyui.min.js" type="text/javascript"></script>-->


<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/datatables/jquery.dataTables.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jstree.min.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/datatables/dataTables.bootstrap.js"
        type="text/javascript"></script>
<!-- Bootstrap -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/src/js/bootstrapValidator.min.js"
        type="text/javascript"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
        type="text/javascript"></script>
<!-- iCheck -->
<!-- date picker -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/datepicker/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.form-validator.js" type="text/javascript"></script>
<!--autocomplete-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.appendGrid-1.3.5.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/userStories.js" type="text/javascript"></script>

<!--charts morrise  -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/morris/morris.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/daterangepicker/daterangepicker.js"
        type="text/javascript"></script>
<!--color picker-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/colorpicker/bootstrap-colorpicker.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/easypagination.js" type="text/javascript"></script>
<!--<!-- fullCalendar -->
<!--<script src="-->
<?php //echo Yii::app()->request->baseUrl; ?><!--/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>-->
<!--morris charts-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/application.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<!--<script src="-->
<?php //echo Yii::app()->request->baseUrl; ?><!--/js/jquery.appendGrid-1.3.5.js" type="text/javascript"></script>-->
</body>


</html>
