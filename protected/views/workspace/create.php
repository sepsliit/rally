<?php
/* @var $this WorkspaceController */
/* @var $model Workspace */

$this->breadcrumbs=array(
	'Workspaces'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Workspace', 'url'=>array('index')),
	array('label'=>'Manage Workspace', 'url'=>array('admin')),
);
?>

<h1>Create Workspace</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>