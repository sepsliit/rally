<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 8/28/14
 * Time: 10:21 PM
 */ ?>

<div class="col-xs-12">
    <section class="content-header">
        <h1>
            <span class="proj_title"><?php echo Yii::app()->user->getState("project_n");?></span>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb col-xs-12 ">
            <li><a href="index.php?r=site/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Manege US</li>
        </ol>
    </section>
</div>


<!--user stories-->
<div class="modal fade" id="viewUs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Iteration</h4>
            </div>
            <div class="modal-body">

            </div>

        </div>
    </div>
</div>
<!--end of user stories-->

<!-- iteration-->
<div class="modal fade" id="tree" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">User Story Details</h4>
            </div>
            <div class="modal-body">
                <table id="view-iteration" class="table table-bordered table-striped dataTable" aria-describedby="example1_info" style="font-size: 13px;">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 189px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 284px;" aria-label="Browser: activate to sort column ascending">Name</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Description</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">owner</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">est</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">State</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Delete</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Edit</th>


                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">


                    </tbody>
                </table>


            </div>

        </div>
    </div>
</div>
<!-- end of  iteration-->
<section class="content">

<div class="row">
<!--left column-->
<div class="col-xs-4" >
    <div class=" main box box-success col-lg-12 " >

        <div class="row">
            <div class="col-xs-3 " style="margin: 20px 0 20px 0;">
<!--                <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myiteration"><i class="fa fa-lg fa-plus-square-o"></i>  Add Iteration</button>-->
            </div>
<!--            <div class="col-xs-3 col-xs-push-2 " style="margin: 20px 0 20px 0;">-->
<!--                <a id="userstorypdf" class="btn btn-default  " href="index.php?r=task/print">-->
<!--                    <i class="fa fa-cloud-download"></i>-->
<!--                    Generate PDF-->
<!--                </a>-->
<!--            </div>-->

        </div>
        <div class="row">
            <div class="col-xs-3" style="margin: 20px 0 20px 0;">
<!--                <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-lg fa-plus-square-o"></i>  Add User Stories</button>-->
            </div>
        </div>
        <!--            user stories-->
        <ul id="user_story" style="min-height: 200px; " class="user_story list-unstyled connectedSortable">
            <?php foreach($iteration as $row):  ?>

                <?php if($row->relid==0){ ?>

                    <li id='<?php echo "li".$row->id; ?>' class="us1">
                        <div class=" ">
                            <div  class="us box box-warning ">
                                <div class="box-header" style="height: 30px;" >

                                    <div class="box-tools pull-right">

                                        <button class="btn btn-warning btn-sm" data-widget="collapse">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <button class="btn btn-warning btn-sm" data-widget="remove">
                                            <i class="fa fa-times"></i>
                                        </button>


                                    </div>

                                    <h3  class="box-title" data-toggle="tooltip" data-placement="left" html=true title="Description: <?php echo "IT".$row->dis; ?> "><span class="fa fa-book"></span></h3><h4> <?php echo " IT".$row->id; ?></h4>


                                </div>
                                <div class="bodys box-body " data-placement="bottom"  id="<?php echo $row->id; ?>" >
                                    <div class="row">
                                        <div class="col-xs-5" style="">
                                            <h5><?php echo Yii::app()->user->getState("username"); ?></h5>

                                        </div>

                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button id=""  it-id="<?php echo $row->id; ?>"  class="btn bg-purple btn-xs viewIt" data-toggle="tooltip" data-placement="top" title="User Stories Detail" >
                                        <i class="fa fa-italic"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>


                <?php } endforeach;  ?>
        </ul>

    </div>
</div>



<!--right column-->
<div class="col-xs-8" style=" ">


    <ul id="items">

        <?php foreach($release as $row1): $nowindex=1;?>

            <li it-id="<?php echo $row1->id; ?>">
                <div class="box-header ">
                    <div class="reteCont">
                        <h3 header-id="<?php echo $row1->id; ?>" class="box-title iterationH "><?php echo $row1->name; ?></h3>
                    </div>
                    <h3 header-id="" class="box-title  "><?php echo $row1->sdate; ?> - <?php echo $row1->edate; ?></h3>
                    <div class="box-tools pull-right">

<!--                        <button id="" class="btn btn-primary btn-xs itdelete" >-->
<!--                            <i class="fa fa-times"></i>-->
<!--                        </button>-->

                    </div>

                </div>

                <ol   class=" drop sort box box-success iteration" style=" " >
                    <?php foreach($iteration as $row):  ?>

                        <?php

                        if($row->relid==$row1->id){

//                               if($nowindex==$row->index) {`
                            ?>

                            <li id='<?php echo "li".$row->id; ?>' class="us1">
                                <div class=" ">
                                    <div  class="us box box-warning ">
                                        <div class="box-header" style="height: 30px;" >

                                            <div class="box-tools pull-right">

                                                <button class="btn btn-warning btn-sm" data-widget="collapse">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                                <button class="btn btn-warning btn-sm" data-widget="remove">
                                                    <i class="fa fa-times"></i>
                                                </button>


                                            </div>

                                            <h3  class="box-title" data-toggle="tooltip" data-placement="left" html=true title="Description: <?php echo "IT".$row->dis; ?> "><span class="fa fa-book"></span></h3><h4> <?php echo " IT".$row->id; ?></h4>


                                        </div>
                                        <div class="bodys box-body " data-placement="bottom"  id="<?php echo $row->id; ?>" >
                                            <div class="row">
                                                <div class="col-xs-5" style="">
                                                    <h5><?php echo Yii::app()->user->getState("username"); ?></h5>

                                                </div>

                                            </div>






                                        </div>
                                        <div class="box-footer">
<!--                                            <button id="userSt" data-toggle="modal" data-target=""  class="btn btn-danger btn-xs  " data-toggle="tooltip" data-placement="top" title="User Stories" >-->
<!--                                                <i class="fa fa-magnet"></i>-->
<!--                                            </button>-->
                                            <button id=""  it-id="<?php echo $row->id; ?>"  class="btn bg-purple btn-xs viewIt" data-toggle="tooltip" data-placement="top" title="User Stories Detail" >
                                                <i class="fa fa-italic"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </li>


<?php
//                                   $nowindex++;
//                               }

                        } endforeach;  ?>


                </ol>

            </li>

        <?php endforeach; ?>

    </ul>


</div>


</div>




</section>

<script>
$(function(){
    previoiusIndex = 0

//       $( "#user_story" ).accordion();
//        $(".user_story li").draggable({
//            appendTo: "body",
////            connectToSortable: "#user_story",
//            revert: "invalid"
////            helper: "clone"
//        });
//        $(".drop li").draggable({
//            appendTo: "body",
////            connectToSortable: "#user_story",
//            revert: "invalid"
////            helper: "clone"
//        });
//        $(".drop li").on( "drag", function( event, ui ) {
//
//
//        } );


    $(".user_story").droppable({
        activeClass: "ui-state-default",
        revert: "invalid",
        hoverClass: "ui-state-hover",
        accept: ".user_story li,.ui-sortable-helper,.drop li",
        connectToSortable: ".sort",
        drop:function( event, ui){
            id= $(ui.draggable).prop('id');
            $( "<li id='"+id+"' class='us1' ></li>" ).html( ui.draggable.html() ).appendTo( this );

//                $(ui.droppable).prop('.connectedSortable');
            $(ui.draggable).remove();
//
            initialorder = new Array()
            $(this).children().each(function(){
                id=$(this).prop('id')
                column=$(this).index()+1;
                main= $(this).parent().parent().attr('it-id')
                item = {}
                item ["id"] = id.replace('li','');

                if(item ["id"]==""){
                    item ["id"]=0
                }
                initialorder.push(item);


            })



            $.each(initialorder,function(key,val){

                if(val.id==""){
                    val.id = 0
                }
//                alert(val.id +'us drop ' )
            })

            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/empty",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    drop: initialorder

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data){


                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });


        }
    })

    $( ".sort" ).sortable({
        connectWith: ".connectedSortable",

        update:function(event, ui ){
            us_id =$(this).children().prop('id')
            it_id = $(this).parent().attr('it-id')

            if(us_id==undefined && us_id!="user_story"){

                $.ajax({
                    type: "POST",
                    url: "index.php?r=workspace/empty",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: {
                        it_id: it_id

                    },
//            contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data){



                    },
                    failure: function(errMsg) {
                        alert(errMsg);
                    }
                });


//                alert($(this).children().prop('id')+' helper '+$(this).parent().attr('it-id'))
            }


        },
        stop: function(event, ui) {
            $('.ui-sortable-placeholder').remove()
//                alert("New position: " + ui.item.index());
//                alert(ui.item.prop('id'))
            divclass = $(this).attr('class').split(' ')

//                alert(divclass[0])
            if(divclass[0]=='drop'){
                neworder = new Array()
                $(this).children().each(function(){
                    id=$(this).prop('id').replace('li','')
                    column=$(this).index()+1;
                    main= $(this).parent().parent().attr('it-id')
                    item = {}
                    item ["id"] = id;
                    item ["column"] = column;
                    item ["main"] =main

                    neworder.push(item);


                })

//                $.each(neworder,function(key,val){
////                    if(key=='id')
//                    alert(val.id +' '+val.column+'sortable'+val.main )
//                })

                $.ajax({
                    type: "POST",
                    url: "index.php?r=workspace/updateui",
                    // The key needs to match your method's input parameter (case-sensitive).
                    data: {
                        drop: neworder

                    },
//            contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data){


                    },
                    failure: function(errMsg) {
                        alert(errMsg);
                    }
                });
            }

        }

    }).disableSelection();

    $( ".drop  " ).droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ".user_story li,:not(.ui-sortable-helper),.drop li",
        connectToSortable: ".sort",
        drop: function( event, ui ) {
//                alert(ui.draggable.html())
//                $( this ).find( ".placeholder" ).remove();
//                $( this ).appendTo('<li>'+ui.draggable.html()+'<li/>' );
            id= $(ui.draggable).prop('id');
            $( "<li id='"+id+"' class='us1' ></li>" ).html( ui.draggable.html() ).appendTo( this );

//                $(ui.droppable).prop('.connectedSortable');
            $(ui.draggable).remove();
//
            initialorder = new Array()
            $(this).children().each(function(){
                id=$(this).prop('id')
                column=$(this).index()+1;
                main= $(this).parent().parent().attr('it-id')
                item = {}
                item ["id"] = id.replace('li','');
                item ["column"] = column;
                item ["main"] =main

                initialorder.push(item);


            })

//            $.each(initialorder,function(key,val){
////                    if(key=='id')
//                alert(val.id +' '+val.column +' '+val.main )
//            })

            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/updateui",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    drop: initialorder

                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data){



                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });

        },
        over: function( event, ui ) {
//            id= $(ui.draggable).parent().prop('id');
//             $(ui.draggable).parent().removeClass('ui-droppable');
//            alert(id+'over')
//            $(ui.item).removeClass('drop')


        },
        out: function( event, ui ) {
//            id= $(ui.draggable).parent().prop('id');

//             $(ui.draggable).parent().addClass('ui-droppable');
//            alert(id+'out')
            $(ui.item).addClass('drop')

        }
    })




})
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dataTables.js" type="text/javascript"></script>
<script>
    $(document).ready(
        function(){
            $('.viewIt').on('click',function(){

                itid=$(this).attr('it-id');



            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/getiteration",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    id:itid
                },
//            contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data){


                    $('#view-iteration').DataTable().destroy()
                    var vdata = [];

                    $.each(data,function(key,val){

                        var row=[
                            val.id,
                            val.uname,
                            val.dis,
                            val.owner,
                            val.est,
                            val.stage,
                            '<button class="btn btn-outline btn-default delete-user"><i class="fa fa-trash-o"></i></button>',
                            '<button class="btn btn-outline btn-default edit-user" data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button>'

                        ]

                        vdata.push(row)


                    })


                    $('#view-iteration').DataTable( {
                        "data": vdata
                    } );

                    $('#tree').modal('show')



                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });



            })
        }
    )
</script>

