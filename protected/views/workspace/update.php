<?php
/* @var $this WorkspaceController */
/* @var $model Workspace */

$this->breadcrumbs=array(
	'Workspaces'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Workspace', 'url'=>array('index')),
	array('label'=>'Create Workspace', 'url'=>array('create')),
	array('label'=>'View Workspace', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Workspace', 'url'=>array('admin')),
);
?>

<h1>Update Workspace <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>