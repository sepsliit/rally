<!doctype html>
<head>
</head>
<body id="bodyMain" >
<div style="margin-top: 5px;">

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Userstory Name</th>
            <th>Description</th>
            <th>Owner</th>
            <th>State</th>
            <th>Hrs</th>

        </tr>
        </thead>
        <tbody>
        <?php
        foreach($userstories as $row ):
        ?>
        <tr>
            <td><?php echo $row['id']?></td>
            <td><?php echo $row['uname']?></td>
            <td><?php echo $row['dis']?></td>
            <td><?php echo $row['owner']?></td>
            <td><?php echo $row['state']?></td>
            <td><?php echo $row['est']?></td>
        </tr>
        <?php  endforeach; ?>
        </tbody>
    </table>
</div>

</body>