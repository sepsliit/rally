

<div class="row" style="margin-top: 5px;">

<!--            defined-->
<div class="col-sm-3 definedus  stateCol" style="min-height: 550px; background-color: #ddd4dc;"  id="Defined" >

    <div class="box-header" style="font-weight: bolder;border-bottom:dashed 1px;">Defined</div>
    <div class="box-body event-body" style="margin-top: 10px;" id="dropHere">

        <?php
        foreach($defined as $row ):
            ?>
            <div class="box usMain" id="<?php echo $row->id."box"?>" >
                <div class="box-header" id="bheader<?php echo $row['id']?>" style="background-color: #5dacff; height: 8px; "></div>
                <div class="box-body usBox" id="<?php echo $row['id']; ;?>" style="border:solid; border-color: #ffac3a; border-top: hidden; min-height: 50px; cursor:move; ">

                    <i class="fa fa-book"  style="color: #ababab;"  > <i style="color: #5e96ff;font-weight: bolder;">US<?php echo $row['id'];?></i></i>
                    <br>
                    <i><?php echo $row['owner']." ";?><i class="fa fa-smile-o fa-2x" style="color:#ababab;"></i></i>

                    <p ><?php echo $row['dis'];?></p>
                    <div  style="float: right; margin-top: -15px;margin-right: -10px; background-color: #c5c5c5; min-width:25px;min-height: 25px;"><?php echo $row['est'];?></div>
                </div>

            </div>
            <div class="menudef" id="menudef<?php echo $row['id']?>" style="width: 100px; margin: 2px auto;z-index:55;" hidden="hidden">
                <ul style="background-color: #dbdbdb; margin-top: -20px;">
                    <li style="margin-right: 5px;" id="menuli<?php echo $row['id']?>" class="menuli" ><a href="#"><i class="fa fa-tint" title="Card color"  ></i></a></li>



                </ul>
            </div>
            <div id="menu2def<?php echo $row['id']?>" class="menu2def"  hidden="hidden">

                <li style="list-style-type: none; float: left;"  ><a href="#" id="orange<?php echo $row['id']?>" ><i class="fa fa-square fa-lg"  style="color: #ff6633;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#"  id="blue<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #1147ff;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="green<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #54b00c;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="yellow<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ffca2c;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="pink<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ff669d;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="purple<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #7b5897;margin-right: 2px;" ></i></a></li>




            </div>
        <?php  endforeach; ?>
    </div>
</div>





<!-- in progress-->
<div class="col-sm-3 inproUS stateCol" style="min-height: 550px;background-color: #ddd4dc;"  id="In progress">

    <div class="box-header"  style="font-weight: bolder;border-bottom:dashed 1px;">In progress</div>
    <div class="box-body event-body" style="margin-top: 10px;" id="dropHere">

        <?php

        foreach( $inprogress as $row ):


            ?>
            <div class="box usMain" id="<?php echo $row->id."box"?>" >
                <div class="box-header"  id="bheader<?php echo $row['id']?>" style="background-color:#5dacff; height: 8px; "></div>
                <div class="box-body usBox" id="<?php echo $row['id']; ;?>" style="border:solid; border-color: #ffac3a; border-top: hidden; min-height: 50px; cursor:move; ">

                    <i class="fa fa-book" style="color: #ababab;" > <i style="color: #5e96ff;font-weight: bolder;">US<?php echo $row['id'];?></i></i>
                    <br>
                    <i><?php echo $row['owner']." ";?><i class="fa fa-smile-o fa-2x" style="color:#ababab;"></i></i>

                    <p><?php echo $row['dis'];?></p>
                    <div  style="float: right; margin-top: -15px;margin-right: -10px; background-color: #c5c5c5; min-width:25px;min-height: 25px;"><?php echo $row['est'];?></div>



                </div>

            </div>
            <div class="menuinp" id="menuinp<?php echo $row['id']?>" style="width: 100px; margin: 2px auto;z-index:55;" hidden="hidden">
                <ul style="background-color: #dbdbdb; margin-top: -20px;">
                    <li style="margin-right: 5px;" id="menuliinp<?php echo $row['id']?>" class="menuliinp"><a href="#"><i class="fa fa-tint" title="Card color"  ></i></a></li>




                </ul>
            </div>
            <div id="menu2inp<?php echo $row['id']?>" class="menu2inp"  hidden="hidden">

                <li style="list-style-type: none; float: left;"  ><a href="#" id="orange<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ff6633;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="blue<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #1147ff;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#"  id="green<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #54b00c;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="yellow<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ffca2c;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="pink<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ff669d;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="purple<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #7b5897;margin-right: 2px;" ></i></a></li>





            </div>
        <?php  endforeach; ?>
    </div>
</div>




<!-- completed-->

<div class="col-sm-3 comUS stateCol"  style="min-height: 550px;background-color: #ddd4dc;"  id="Completed">

    <div class="box-header"  style="font-weight: bolder;border-bottom:dashed 1px;">Completed</div>
    <div class="box-body event-body" style="margin-top: 10px;" id="dropHere">

        <?php

        foreach( $completed as $row ):


            ?>
            <div class="box usMain" id="<?php echo $row->id."box"?>" >
                <div class="box-header"  id="bheader<?php echo $row['id']?>" style="background-color:#5dacff; height: 8px; "></div>
                <div class="box-body usBox" id="<?php echo $row['id'];?>" style="border:solid; border-color: #ffac3a; border-top: hidden; min-height: 50px; cursor:move; ">

                    <i class="fa fa-book" style="color: #ababab;" > <i style="color: #5e96ff;font-weight: bolder;">US<?php echo $row['id'];?></i></i>
                    <br>
                    <i><?php echo $row['owner']." ";?><i class="fa fa-smile-o fa-2x" style="color:#ababab;"></i></i>

                    <p><?php echo $row['dis'];?></p>
                    <div  style="float: right; margin-top: -15px;margin-right: -10px; background-color: #c5c5c5; min-width:25px;min-height: 25px;"><?php echo $row['est'];?></div>



                </div>

            </div>
            <div class="menucom" id="menucom<?php echo $row['id'];?>" style="width: 100px; margin: 2px auto;z-index:55;" hidden="hidden">
                <ul style="background-color: #dbdbdb; margin-top: -20px;">
                    <li style="margin-right: 5px;" id="menulicom<?php echo $row['id'];?>"  class="menulicom"><a href="#"><i class="fa fa-tint" title="Card color"  ></i></a></li>



                </ul>
            </div>
            <div id="menu2com<?php echo $row['id'];?>" class="menu2com"  hidden="hidden">

                <li style="list-style-type: none; float: left;"  ><a href="#" id="orange<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ff6633;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="blue<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color:  #1147ff;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#"  id="green<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #54b00c;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="yellow<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ffca2c;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="pink<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ff669d;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="purple<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #7b5897;margin-right: 2px;" ></i></a></li>





            </div>
        <?php  endforeach; ?>
    </div>
</div>



<!--     accepted-->


<div class="col-sm-3 accUS stateCol" style="min-height: 550px;background-color: #ddd4dc;" id="Accepted">

    <div class="box-header"  style="font-weight: bolder;border-bottom:dashed 1px;">Accepted</div>
    <div class="box-body event-body" style="margin-top: 10px;" id="dropHere">

        <?php

        foreach( $accepted as $row ):


            ?>
            <div class="box usMain" id="<?php echo $row->id."box"?>" >
                <div class="box-header" id="bheader<?php echo $row['id']?>" style="background-color: #5dacff; height: 8px; "></div>
                <div class="box-body usBox" id="<?php echo $row['id']; ;?>" style="border:solid; border-color: #ffac3a; border-top: hidden; min-height: 50px; cursor:move; ">

                    <i class="fa fa-book" style="color: #ababab;" > <i style="color: #5e96ff;font-weight: bolder;">US<?php echo $row['id'];?></i></i>
                    <br>
                    <i ><?php echo $row['owner']." ";?><i class="fa fa-smile-o fa-2x" style="color:#ababab;"></i></i>

                    <p><?php echo $row['dis'];?></p>
                    <div  style="float: right; margin-top: -15px;margin-right: -10px; background-color: #c5c5c5; min-width:25px;min-height: 25px;"><?php echo $row['est'];?></div>



                </div>

            </div>
            <div class="menuacc" id="menuacc<?php echo $row['id'];?>" style="width: 100px; margin: 2px auto;z-index:55;" hidden="hidden">
                <ul style="background-color: #dbdbdb; margin-top: -20px;">
                    <li style="margin-right: 5px;" class="menuliacc" id="menuliacc<?php echo $row['id'];?>"  ><a href="#"><i class="fa fa-tint" title="Card color"  ></i></a></li>



                </ul>
            </div>
            <div id="menu2acc<?php echo $row['id'];?>" class="menu2acc"  hidden="hidden">

                <li style="list-style-type: none; float: left;"  ><a href="#" id="orange<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ff6633;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="blue<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color:  #1147ff;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#"  id="green<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #54b00c;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="yellow<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ffca2c;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="pink<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #ff669d;margin-right: 2px;" ></i></a></li>
                <li style="list-style-type: none; float: left;"  ><a href="#" id="purple<?php echo $row['id']?>"><i class="fa fa-square fa-lg"  style="color: #7b5897;margin-right: 2px;" ></i></a></li>





            </div>
        <?php  endforeach; ?>
    </div>
</div>

</div>









