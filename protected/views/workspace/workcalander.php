<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 8/18/14
 * Time: 9:48 PM
 */ ?>
<div class="col-xs-12">
    <section class="content-header">
        <h1>
            <span class="proj_title"><?php echo Yii::app()->user->getState("project_n");?></span>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb col-xs-12 ">
            <li><a href="index.php?r=site/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Manege US</li>
        </ol>
    </section>
</div>
<section class="content">


    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header">
                    <h4 class="box-title">Draggable Release</h4>
                </div>
                <div class="box-body">
                    <!-- the events -->
                    <div id='external-events'>
                        <div class='external-event bg-green'>Release version</div>
                        <div class='external-event bg-red'>Release1.1</div>
                        <div class='external-event bg-aqua'>Release Test</div>
                        <div class='external-event bg-yellow'>Work on UI design</div>
                        <div class='external-event bg-navy'>Release2.1</div>
                        <p>
                            <input type='checkbox' id='drop-remove' /> <label for='drop-remove'>remove after drop</label>
                        </p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Create Release</h3>
                </div>
                <div class="box-body">
                    <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                        <button type="button" id="color-chooser-btn" class="btn btn-danger btn-block btn-sm dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>
                        <ul class="dropdown-menu" id="color-chooser">
                            <li><a class="text-green" href="#"><i class="fa fa-square"></i> Green</a></li>
                            <li><a class="text-blue" href="#"><i class="fa fa-square"></i> Blue</a></li>
                            <li><a class="text-navy" href="#"><i class="fa fa-square"></i> Navy</a></li>
                            <li><a class="text-yellow" href="#"><i class="fa fa-square"></i> Yellow</a></li>
                            <li><a class="text-orange" href="#"><i class="fa fa-square"></i> Orange</a></li>
                            <li><a class="text-aqua" href="#"><i class="fa fa-square"></i> Aqua</a></li>
                            <li><a class="text-red" href="#"><i class="fa fa-square"></i> Red</a></li>
                            <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i> Fuchsia</a></li>
                            <li><a class="text-purple" href="#"><i class="fa fa-square"></i> Purple</a></li>
                        </ul>
                    </div><!-- /btn-group -->
                    <div class="input-group">
                        <input id="new-event" type="text" class="form-control" placeholder="Event Title">
                        <div class="input-group-btn">
                            <button id="add-new-event" type="button" class="btn btn-default btn-flat">Add</button>
<!--                            <a id="sss" href="index.php?r=workspace/sms" class="btn btn-default btn-flat">SMS</a>-->
                        </div><!-- /btn-group -->
                    </div><!-- /input-group -->
                </div>
            </div>
        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div><!-- /.row -->


</section>
<!-- fullCalendar -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
        ele.each(function() {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 1070,
                revert: true, // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });
    }
    ini_events($('#external-events div.external-event'));
//                ini_events($('#calendar div.fc-day-content'));





    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        buttonText: {//This is to add icons to the visible buttons
            prev: "<span class='fa fa-caret-left'></span>",
            next: "<span class='fa fa-caret-right'></span>",
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
        },
        //Random default events
        events:function(start, end, callback) {
            $.ajax({
                url: 'index.php?r=workspace/getevent',
                dataType: 'json',
                data: {
                    // our hypothetical feed requires UNIX timestamps
                    start: Math.round(start.getTime() / 1000),
                    end: Math.round(end.getTime() / 1000)
                },
                success: function(doc) {
                    var events = [];
                    $.each(doc,function(key,val) {


                        if(val.sdate<new Date()){
                            events.push({
                                title: val.name,
                                id: val.id,
                                start: val.sdate, // will be parsed
                                end: val.edate,
                                backgroundColor: val.color,
                                borderColor: val.color,
                                editable:false,
                                disableResizing: true

                            });
                        }else{
                            events.push({
                                title: val.name,
                                id: val.id,
                                start: val.sdate, // will be parsed
                                end: val.edate,
                                backgroundColor: val.color,
                                borderColor: val.color
                            });
                        }

                    });

                    callback(events);
                }
            });
        },



        editable: true,
        eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
            var check = $.fullCalendar.formatDate(event.start,'yyyy-MM-dd');
            var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd')
            if(check<today){
                revertFunc();
            }else{

                sdate= Formatyear(event.start)+"-"+Formatmonth(event.start)+"-"+Formatdate(event.start)
                edate=event.end
                if(edate!=null){
                    edate= Formatyear(event.end)+"-"+Formatmonth(event.end)+"-"+Formatdate(event.end);
                }else{
                    edate=sdate;
                }


                $.ajax({
                    type: "POST",
                    url: "index.php?r=workspace/eventdrop",
                    // The key needs to match your method's input parameter (case-sensitive).

                    data: {
                        id:event.id,
                        title:event.title,
                        color:event.backgroundColor,
                        sdate:sdate,
                        edate:edate

                    },
                    dataType: "json",
                    // contentType: "application/json; charset=utf-8",

                    success: function(data){


//

                    },
                    failure: function(errMsg) {
                        alert(errMsg);
                    }
                });


            }




//            alert( event.title+" was drop on "+' '+event.start+'  '+ event.end );






        },
        selectable: false,
        selectHelper: false,
        select: function(start, end) {
            var title = prompt('Event Title:');
            var eventData;
            if (title) {
                eventData = {
                    title: title,
                    start: start,
                    end: end
                };
                $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
            }
            $('#calendar').fullCalendar('unselect');

//            alert(start+"  "+end );
        },
        eventClick: function (calEvent, jsEvent, view) {

            if (confirm("Are you sure delete this event?")) {
//                $('#calendar').fullCalendar('removeEvents', calEvent._id);



                $.ajax({
                    type: "POST",
                    url: "index.php?r=workspace/eventdelete",
                    // The key needs to match your method's input parameter (case-sensitive).

                    data: {
                        id:calEvent._id
                    },
                    dataType: "json",
                    // contentType: "application/json; charset=utf-8",

                    success: function(data){

                        $('#calendar').fullCalendar('removeEvents', calEvent._id);
//

                    },
                    failure: function(errMsg) {
                        alert(errMsg);
                    }
                });



            }



        },
        eventResizeStart:function( event, jsEvent, ui, view ) {
            var check = $.fullCalendar.formatDate(event.start,'yyyy-MM-dd');
            var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd')
            if(check<today){

//                event.editable=false;
                event.disableResizing= true;
//                alert('dsds')
            }

        },
        eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {

            var check = $.fullCalendar.formatDate(event.start,'yyyy-MM-dd');
            var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd')
            if(check<today){

                event.editable=false;
                revertFunc();
            }else{

                $('#calendar').fullCalendar('updateEvent', event)

                sdate= Formatyear(event.start)+"-"+Formatmonth(event.start)+"-"+Formatdate(event.start)
                edate=event.end
                if(edate!=null){
                    edate= Formatyear(event.end)+"-"+Formatmonth(event.end)+"-"+Formatdate(event.end);
                }else{
                    edate=sdate;
                }


                $.ajax({
                    type: "POST",
                    url: "index.php?r=workspace/eventdrop",
                    // The key needs to match your method's input parameter (case-sensitive).

                    data: {
                        id:event.id,
                        title:event.title,
                        color:event.backgroundColor,
                        sdate:sdate,
                        edate:edate

                    },
                    dataType: "json",
                    // contentType: "application/json; charset=utf-8",

                    success: function(data){


//

                    },
                    failure: function(errMsg) {
                        alert(errMsg);
                    }
                });



            }






        },
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function(date, allDay) { // this function is called when something is dropped





            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

//            alert(year+"-"+month+"-"+day+"test "+$(this).css("background-color")+" "+originalEventObject.title )
            var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd')
            var check = $.fullCalendar.formatDate(date,'yyyy-MM-dd')



            if(check > today){
                retDate = Formatyear(date)+"-"+Formatmonth(date)+"-"+Formatdate(date);
            color =$(this).css("background-color")
            title=originalEventObject.title;

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)


            $.ajax({
                type: "POST",
                url: "index.php?r=workspace/calenderdrop",
                // The key needs to match your method's input parameter (case-sensitive).

                data: {
                title:title,
                color:color,
                date:retDate
                },
                dataType: "json",
                // contentType: "application/json; charset=utf-8",

                success: function(data){


//                    $("#"+data.id).css('id',)
                    copiedEventObject.id=data.id;
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
//

                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });



            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        }else{
                alert('Event date is not valid ')
            }
        }
    });

    function Formatdate(fdate){

        var fulldate = fdate+"";
        fulldate = fulldate.split(' ')
        return fulldate[2];

    }
    function Formatmonth(fdate){
        var fulldate = fdate+"";
        fulldate = fulldate.split(' ')

        return monthConv(fulldate[1])


    }
    function Formatyear(fdate){

        var fulldate = fdate+"";
        fulldate = fulldate.split(' ')

        return fulldate[3];
    }
    function monthConv(m){

        if(m=="Jan"){
            return 01;
        }else if(m=="Feb"){
            return 02;
        }else if(m=="Mar"){
            return 03;
        }else if(m=="Apr"){
            return 04;
        }else if(m=="May"){
            return 05;
        }else if(m=="Jun"){
            return 06;
        }else if(m=="Jul"){
            return 07;
        }else if(m=="Aug"){
            return 08;
        }else if(m=="Sep"){
            return 09;
        }else if(m=="Oct"){
            return 10;
        }else if(m=="Nov"){
            return 11;
        }else if(m=="Dec"){
            return 12;
        }

    }

    /* ADDING EVENTS */
    var currColor = "#f56954"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function(e) {
        e.preventDefault();
        //Save color
        currColor = $(this).css("color");
        //Add color effect to button
        colorChooser
            .css({"background-color": currColor, "border-color": currColor})
            .html($(this).text()+' <span class="caret"></span>');
    });
    $("#add-new-event").click(function(e) {
        e.preventDefault();
        //Get value and make sure it is not null
        var val = $("#new-event").val();
        if (val.length == 0) {
            return;
        }

        //Create event
        var event = $("<div />");
        event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
        event.html(val);
        $('#external-events').prepend(event);

        //Add draggable funtionality
        ini_events(event);

        //Remove event from text input
        $("#new-event").val("");
    });

    //create month
});
</script>