<div class="col-xs-12">
    <section class="content-header">
        <h1>
            <span id="proj_title"><?php echo Yii::app()->user->getState("project_n");?></span>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb col-xs-12 ">
            <li><a href="index.php?r=site/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Stories</li>
        </ol>
    </section>
</div>

<section class="content">


<div class="col-xs-12" >


<div class="box col-xs-12" >
<div class="row">
    <div class="col-xs-3" style="margin: 20px 0 20px 0;">
        <button class="btn btn-primary" data-toggle="modal" id="addUS" data-target="#myModal"><i class="fa fa-lg fa-plus"></i> Add Userstory</button>
    </div>
</div>




<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 750px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Userstory</h4>
            </div>
            <div class="modal-body" style="max-height:400px;overflow-y: auto;">
                <div id="success-alert" class="alert alert-success alert-dismissable">
                    <!--                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>-->
                    <!--                        <a class="alert-link" href="#">Alert Link</a>-->
                </div>

                <form id="myForm" class="form-horizontal my_modal" >
                    <!--Userstory name -->
                    <div class="form-group">



                        <label id="lblname" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Userstory Name"
                                   data-bv-notempty="true"
                                   data-bv-notempty-message="The Department is required and cannot be empty"
                                   style="border: 1px solid #ccc;border-radius: 4px;"/>
                        </div>
                    </div>


                    <!--Description -->

                    <div class="form-group">
                        <label id="lbldes" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">

                            <textarea cols="50"    id="description" name="description" rows="5" data-validation="required" maxlength="500"data-validation-error-msg=" User Story description is required " class="form-control" ></textarea>


                        </div>
                    </div>


                    <!--my description-->
                    <div class="form-group">
                        <label  class="col-sm-2 control-label" id="mydescription"  >You have 500 characters remaining</label>
                    </div>




                    <!--Owner -->
                    <div class="form-group">


                        <label  id="lblowner" class="col-sm-2 control-label">Owner</label>
                        <div class="col-sm-10">
                            <!--                            <input type="text" class="form-control" id="owner"  name="owner"   placeholder="Owner"  data-validation="required" data-validation-error-msg=" Owner is required ">-->

                            <select id="owner" class="form-control drop_button " name="owner"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <?php foreach($users as $row ):?>
                                    <option value="<?php echo $row->login->username;?>"><?php echo $row->login->username;?></option>
                                <?php endforeach;?>

                            </select>


                        </div>
                    </div>




                    <!--Iteration -->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Iteration</label>
                        <div class="col-sm-10">
                            <select id="iteration" class="form-control drop_button " name="iteration"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <option value="0">Unsheduled</option>
                                <?php foreach($iteration as $row):?>
                                <option value="<?php echo $row->id?>" ><?php echo $row->name?></option>
                                <?php endforeach;?>
                            </select>

                        </div>

                    </div>


<!--                    <!-- blocked -->
<!---->
<!--                    <div class="form-group">-->
<!---->
<!---->
<!--                        <label  class="col-sm-2 control-label">Blocked</label>-->
<!--                        <div class="col-sm-10">-->
<!--                            <input type="checkbox" name="bcheck" id="bcheck">-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!---->
<!--                    <!-- blocked reason -->
<!--                    <div class="form-group">-->
<!---->
<!---->
<!--                        <label  class="col-sm-2 control-label">Blocked Reason</label>-->
<!--                        <div class="col-sm-10">-->
<!--                            <input type="text" class="form-control" id="reason" disabled="disabled" placeholder="Blocked reason">-->
<!--                        </div>-->
<!--                    </div>-->


                    <!--Estimated hours-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Plan Estimate(Hrs)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="estimate" name="estimate" placeholder="Estimated hours">
                        </div>
                    </div>




                </form>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="save_userstory" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<!--            toggle div-->

<!--            toggle edit-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 750px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Userstory</h4>
            </div>
            <div class="modal-body" style="max-height: 400px;overflow-y: auto;">
                <div id="esuccess-alert" class="alert alert-success alert-dismissable">
                    <!--                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>-->

                    <!--                                                    <a class="alert-link" href="#">Alert Link</a>-->
                </div>

                <!--  form edit  goes here-->



                <form id="myFormEdit" class="form-horizontal my_modal" >
                    <!--Userstory name -->
                    <div class="form-group">



                        <label  class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="ename" name="ename"  data-validation="required" data-validation-error-msg=" Userstory name is required ">
                        </div>
                    </div>


                    <!--Description -->

                    <div class="form-group">
                        <label  class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">

                            <textarea cols="50"      id="edescription" name="edescription" rows="5" data-validation="required" maxlength="500"data-validation-error-msg=" User Story description is required " class="form-control" ></textarea>


                        </div>
                    </div>


                    <!--my description-->
                    <div class="form-group">
                        <label  class="col-sm-2 control-label" id="emydescription"  >You have 200 characters remaining</label>
                    </div>




                    <!--Owner -->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Owner</label>
                        <div class="col-sm-10">
                            <!--                            <input type="text" class="form-control" id="eowner"   name="eowner"   placeholder="Owner"  data-validation="required" data-validation-error-msg=" Owner is required ">-->

                            <select id="eowner" class="form-control drop_button " name="eowner"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <?php foreach($users as $row ):?>
                                    <option value="<?php echo $row->login->username;?>"><?php echo $row->login->username;?></option>

                                <?php endforeach;?>

                            </select>

                        </div>
                    </div>


                    <!--Iteration -->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Iteration</label>
                        <div class="col-sm-10">
                            <select id="editeration" class="form-control drop_button " name="editeration"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <option value="0" >Unsheduled</option>
                                <?php foreach($iteration as $row):?>
                                    <option value="<?php echo $row->id?>"><?php echo $row->name?></option>
                                <?php endforeach;?>
                            </select>

                        </div>

                    </div>


                    <!--State-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">State</label>
                        <div class="col-sm-10">
                            <select id="estate" class="form-control drop_button " name="estate"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                <option value="Defined">Defined</option>
                                <option value="In progress">In progress</option>
                                <option value="Completed">Completed</option>
                                <option value="Accepted">Accepted</option>

                            </select>

                        </div>

                    </div>




                    <!-- blocked -->

                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Blocked</label>
                        <div class="col-sm-10">
                            <input type="checkbox" name="ebcheck" id="ebcheck">
                        </div>
                    </div>


                    <!-- blocked reason -->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Blocked Reason</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" disabled="disabled"   id="ereason" placeholder="Blocked reason">
                        </div>
                    </div>


                    <!--Estimated hours-->
                    <div class="form-group">


                        <label  class="col-sm-2 control-label">Plan Estimate(Hrs)</label>
                        <div class="col-sm-10">
                            <input type="text"  class="form-control" id="eestimate" name="eestimate" placeholder="Estimated hours">
                        </div>
                    </div>




                </form>
                <!--  Edit   Form ends-->

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="edit_userstory" class="btn btn-primary">Save</button>
            </div>

        </div>
    </div>
</div>
<!--            toggle edit-->


<!--Table starts -->



<div class="box-body table-responsive">
    <div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <div class="row">


        </div>
        <table id="story-table" class="table table-bordered table-striped dataTable" aria-describedby="example1_info" style="font-size: 13px;">
            <thead>
            <tr role="row">
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 189px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 284px;" aria-label="Browser: activate to sort column ascending">Userstory Name</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Description</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Owner</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">State</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Iteration</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Hrs</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Delete</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Edit</th>


            </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i=0;
            $myclass='colorR';
            foreach($userstories as $row ):
                if(($i%2)==0){

                    ?>

                    <tr id="<?php echo $row->id?>" class="even <?php if($row->breason!=null) echo $myclass ?>"  >
                        <td class=" sorting_1"><a id="<?php echo $row->id?>kk" name='<?php echo $row->id; ?>'><?php echo $row->id; ?></a></td>
                        <td class=" " ><?php echo $row->uname; ?></td>
                        <td class=" "><?php echo $row->dis; ?></td>
                        <td class=" "><?php echo $row->owner; ?></td>
                        <td class=" "><?php echo $row->state; ?></td>
                        <?php if($row->iterationid!=NULL){?>
                            <td class=" "><?php echo $row->iteration->name; ?></td>
                        <?php } else{?>
                            <td class=" ">Unsheduled</td>
                        <?php }?>
                        <td class=" "><?php echo $row->est ?></td>
                        <td class="center"><button class="btn btn-outline btn-default delete-product"><i class="fa fa-trash-o"></i></button></td>
                        <td class="center"><button  class="btn btn-outline btn-default edit-product" data-toggle="modal" data-target="#edit" ><i class="fa fa-edit"></i></button></td>

                    </tr>
                <?php }else{ ?>
                    <tr id="<?php echo $row->id?>" class="odd <?php if($row->breason!=null) echo $myclass ?>">
                        <td class=" sorting_1"><a id="<?php echo $row->id?>kk" name='<?php echo $row->id; ?>'><?php echo $row->id; ?></a></td>
                        <td class=" "><?php echo $row->uname; ?></td>
                        <td class=" "><?php echo $row->dis; ?></td>
                        <td class=" "><?php echo $row->owner; ?></td>
                        <td class=" "><?php echo $row->state; ?></td>
                        <?php if($row->iterationid!=NULL){?>
                        <td class=" "><?php echo $row->iteration->name; ?></td>
                        <?php } else{?>
                        <td class=" ">Unsheduled</td>
                          <?php }?>
                        <td class=" "><?php echo $row->est; ?></td>
                        <td class="center"><button class="btn btn-outline btn-default delete-product"><i class="fa fa-trash-o"></i></button></td>
                        <td class="center"><button class="btn btn-outline btn-default edit-product" data-toggle="modal" data-target="#edit"><i class="fa fa-edit"></i></button></td>

                    </tr>



                <?php } endforeach; ?>

            </tbody>
        </table>
        <div class="row">
        </div>
    </div>
</div>

<script type="text/javascript">



</script>



<!--    Table ends           -->

</section>




