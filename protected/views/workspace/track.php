    <script language="Javascript">

        $(document).ready(function()
        {
            //alert('555');
            $('#detailUS').data('clicked', true);
            $('#detailUS').css("color","white");
            $('#detailUS').parent().css("background-color","#36bce1");




        });



        function ajaxFileUpload()
        {

            $.ajaxFileUpload
            (
                {
                    url:'index.php?r=workspace/usImport',
                    secureuri:false,
                    fileElementId:'us',
                    dataType: 'json',
                    data:{stage:$('#trackIteration').val()},
                    success: function (data, status)
                    {
                        if(typeof(data.error) != 'undefined')
                        {
                            if(data.error != '')
                            {
                                alert(data.error);

                            }else
                            {

                                // alert(data);
                                if($('#listUS').data('clicked')) {

                                    var url=encodeURI("index.php?r=workspace/lView&stage="+data.msg)
                                    $( "#main" ).load( url+" #content" );

                                }else if($('#detailUS').data('clicked')) {

                                    var url= encodeURI("index.php?r=workspace/dView&stage="+data.msg) ;
                                    $( "#main" ).load(url+" #content" ,function()
                                    {

                                    });

                                }



                            }
                        }
                    },
                    error: function (data, status, e)
                    {
                        alert(e);
                    }
                }
            )

            return false;

        }


    </script>


    <div class="col-xs-12">
        <section class="content-header">
            <h1>
                <span id="proj_title"><?php echo Yii::app()->user->getState("project_n");?></span>
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb col-xs-12 ">
                <li><a href="index.php?r=site/index"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">  Iteration Tracking</li>
            </ol>
        </section>
    </div>




    <div class="col-lg-12"  style="margin-top: 5px;" >


    <div class="box">
        <div class="box-header" style="background-color: rgb(228, 228, 228);font-weight: bolder">

        </div>
    </div>

    <div class="box-body">


        <select id="trackIteration"   class="form-control drop_button " name="trackIteration"  style="font-size: 10px; width: 125px;margin-top: 5px; ">
                  <option value="">Select an Iteration</option>
                   <?php foreach($iteration as $row ):?>
                <option value="<?php echo $row['id']?>"> <?php echo $row['name']." ".$row['startd']." - ".$row['endd']?></option>
            <?php endforeach;?>

        </select>

        <div style="background-color:#ffffff; width: 25px;height: 25px;float:right;margin-top: -23px; border:1px solid #a6a6a6;margin-left: 2px;">
            <i class="fa fa-bars " id="listUS"  title="List View " style="margin:6px;cursor: pointer;color: #a4a4a4"></i>

        </div>
        <div style="background-color:#ffffff; width: 25px;height: 25px;float:right;margin-top: -23px; border:1px solid #a6a6a6;">
            <i class="fa fa-th " id="detailUS" title="Detail View" style="margin:6px;  cursor: pointer;color: #a4a4a4"></i>

        </div>

        <div style="background-color:#ffffff; width: 25px;height: 25px;float:right;margin-top: -23px; margin-right: 20px; border:1px solid #a6a6a6; ">


            <i class="fa fa-share-square-o fa-rotate-90 " id="import"   title='Import UserStories' style="margin:6px;  cursor: pointer;color: #4ba7ff"></i>

        </div>
        <div style="background-color:#ffffff; width: 25px;height: 25px;float:right;margin-top: -23px; margin-right: 20px; border:1px solid #a6a6a6; ">

            <!--change this.............-->
            <i class="fa fa-share-square-o " id="export"  title="Export User Stories" style="margin:6px;  cursor: pointer;color: #4ba7ff"></i>

        </div>

        <!--        <i class="fa fa-th fa-2x"style="float:right; margin-right: 8px;margin-top: -23px;cursor: pointer;background-color:#ffffff;color:#a4a4a4"  id="detailUS"></i>-->




        <div id="main" style="min-height: 520px;" >

        </div>





    </div>
</div>

<!--modal goes here-->


    <div class="modal fade" id="importmodal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 450px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >IMPORT USER STORIES</h4>
                </div>
                <div class="modal-body">
                    <form name="formMy"  id="formMy"  enctype="multipart/form-data" method="post">
                        1. use our <a href="<?php echo Yii::app()->request->baseUrl; ?>/src/download/abc.csv" target="_top">import template</a> to ensure the proper data format for import.<br>

                        2. Choose a .csv file to import
                        <input type="file" style="margin-top: 8px;" id="us" name="us"><br>

                        <button class="btn btn-primary" id="usImport" name="usImport" onclick="return ajaxFileUpload();">Import</button>
                        <button class="btn btn-primary">Cancel</button>


                    </form>



                </div>
            </div>
        </div>
    </div>


<script>
    $('#import').click(function()
    {
        if($('#trackIteration').val()!='')
        {
            $('#importmodal').modal();
        }else{
            alert('Plese select an iteration');
        }

    });
</script>





