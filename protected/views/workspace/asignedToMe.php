<div class="col-xs-12">
    <section class="content-header">
        <h1>
            <span id="proj_title"><?php echo Yii::app()->user->getState("project_n");?></span>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb col-xs-12 ">
            <li><a href="index.php?r=site/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Task</li>
        </ol>
    </section>
</div>



<?php foreach($iteration as $row):?>
<div class="col-sm-3" >

    <div class="box-header" style="font-weight: bolder;border-bottom:dashed 1px;"><?php echo $row->name?></div>
    <div class="box-body " style="margin-top: 10px;">
<?php foreach($task as $ts):?>
<?php if(($ts->userStories->login_workspaceid)==($row->login_workspaceid)){?>
        <?php if(($ts->stage)==($row->name)){?>
            <div class="box usMain" id="<?php echo $ts->id."box"?>" >
                <div class="box-header" id="bheader" style="background-color: #ff6eb1; height: 8px; "></div>
                <div class="box-body usBox" style="border:solid; border-color: #ffac3a; border-top: hidden; min-height: 50px; cursor:move; ">

                    <i class="fa fa-book"  style="color: #ababab;"  > <i style="color: #5e96ff;font-weight: bolder;">Task - <?php echo$ts->dis;?></i></i>
                    <br>


                    <p >US - <?php echo $ts->userStories->dis?></p>
                    <p style="color: #4e58af;"><?php echo $ts->state;?></p>
                    <div  style="float: right; margin-top: -15px;margin-right: -10px; background-color: #c5c5c5; min-width:25px;min-height: 25px;"><?php echo $ts->est;?></div>
                </div>

                </div>
    <?php }?>
   <?php }?>
<?php endforeach;?>


    </div>
</div>
<?php endforeach;?>
