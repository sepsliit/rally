<?php
$userinfo = Yii::app()->user->getState('userinfo');
?>

<div class="form-box" id="login-box">
    <div class="header">Register New Membership</div>
    <form id="regForm" >
        <div class="body bg-gray">
            <div class="form-group has-feedback">
                <!--                <input type="text" id="fname" name="name" class="form-control" placeholder="Full name" data-validation="required" data-validation-error-msg=" Full name is required " />-->
                <input id="fname" type="text" class="form-control" name="firstName" value="<?php echo  ($userinfo != null && $userinfo->displayname != null) ? $userinfo->displayname : '' ?>" placeholder="First name" style="border: 1px solid #ccc;
    border-radius: 4px;"
                       data-bv-notempty="true"
                       data-bv-notempty-message="The first name is required and cannot be empty" />
            </div>
            <div class="form-group">
                <input id="email" class="form-control" name="email" type="email" value="<?php echo  ($userinfo != null && $userinfo->email != null) ? $userinfo->email : '' ?>" placeholder="Email" style="border: 1px solid #ccc;
    border-radius: 4px;"
                       data-bv-notempty="true"
                       data-bv-notempty-message="The Email is required and cannot be empty"
                       data-bv-emailaddress="true"
                       data-bv-emailaddress-message="The input is not a valid email address"/>
            </div>

            <div class="form-group">
                <select id="title" name="title" class="form-control"
                        data-bv-notempty="true"
                        data-bv-notempty-message="The Job Title is required and cannot be empty"
                    >
                    <option selected="selected" value="">Choose a Job Title...</option>
                    <option value="architect">Architect</option>
                    <option value="business_analyst">Business Analyst</option>
                    <option value="ceo">CEO</option><option value="cio">CIO</option>
                    <option value="consultant">Consultant</option>
                    <option value="cto">CTO</option>
                    <option value="development_manager">Dev. Manager</option>
                    <option value="it_manager">IT Manager</option>
                    <option value="pmo_manager">PMO Manager</option>
                    <option value="product_manager">Product Manager</option>
                    <option value="professor">Professor</option>
                    <option value="program_manager">Program Manager</option>
                    <option value="project_manager">Project Manager</option>
                    <option value="qa_engineer">QA Engineer</option>
                    <option value="qa_manager">QA Manager</option>
                    <option value="software_methodologist">SW Methodologist</option>
                    <option value="student">Student</option>
                    <option value="sw_developer">Software Developer</option>
                    <option value="sw_engineer">Software Engineer</option>
                    <option value="vp_development">VP Development</option>
                    <option value="vp_engineering">VP Engineering</option>
                    <option value="other">Other</option>
                </select>

            </div>
            <div class="form-group">
                <input type="tel" id="phone"  name="mobilePhone" class="form-control" placeholder="Phone Number" style="border: 1px solid #ccc;border-radius: 4px;"
                       data-bv-stringlength="true"
                       data-bv-digits="true"
                       data-bv-digits-message="The input is not a valid phone number"
                       data-bv-stringlength-max="10"
                       data-bv-stringlength-min="10"
                       data-bv-stringlength-message="The only 10 numbers"

                       data-bv-notempty="true"
                       data-bv-notempty-message="The Phone Number is required and cannot be empty"

                    />
            </div>
            <!--            <div class="form-group">-->
            <!--                <select class="form-control product">-->
            <!--                    <option value="0">Select Product</option>-->
            <!--                </select>-->
            <!--            </div>    -->
            <div class="form-group">
                <input type="text" id="dept" name="userid" class="form-control" placeholder="Department"
                       data-bv-notempty="true"
                       data-bv-notempty-message="The Department is required and cannot be empty"
                       style="border: 1px solid #ccc;border-radius: 4px;"

                    />
            </div>
            <div class="form-group">
                <input type="text" id="office" name="userid" class="form-control" placeholder="Office Location"
                       data-bv-notempty="true"
                       data-bv-notempty-message="The Office Location is required and cannot be empty"
                       style="border: 1px solid #ccc;border-radius: 4px;"

                    />
            </div>

            <div class="form-group">
                <input type="password" id="pass"  class="form-control" placeholder="Password" name="password"
                       style="border: 1px solid #ccc;border-radius: 4px;"
                       data-bv-notempty="true"
                       data-bv-notempty-message="The password is required and cannot be empty"

                       data-bv-identical="true"
                       data-bv-identical-field="confirmPassword"
                       data-bv-identical-message="The password and its confirm are not the same"

                       data-bv-different="true"
                       data-bv-different-field="firstName"
                       data-bv-different-message="The password cannot be the same as Firstname"/>
            </div>
            <div class="form-group">
                <input type="password" id="pass2" name="confirmPassword" class="form-control" placeholder="Retype password"

                       style="border: 1px solid #ccc;border-radius: 4px;"
                       data-bv-notempty="true"
                       data-bv-notempty-message="The confirm password is required and cannot be empty"

                       data-bv-identical="true"
                       data-bv-identical-field="password"
                       data-bv-identical-message="The password and its confirm are not the same"

                       data-bv-different="true"
                       data-bv-different-field="firstName"
                       data-bv-different-message="The password cannot be the same as username"


                    />
            </div>
        </div>
        <div class="footer">

            <button type="button" id="register" class="btn bg-olive btn-block">Sign me up</button>

            <a href="index.php?r=site/login" class="text-center">I already have a membership</a>
        </div>
    </form>

    <div class="margin text-center">
        <span>Register using social networks</span>
        <br/>
        <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
        <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
        <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

    </div>
</div>

<script>

</script>





