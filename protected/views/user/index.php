
<div class="col-xs-12">
    <section class="content-header">
        <h1>
            <span class="proj_title"><?php echo Yii::app()->user->getState("project_n");?></span>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb col-xs-12 ">
            <li><a href="index.php?r=site/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Manage</li>
        </ol>
    </section>
</div>
<section class="content">


<div class="col-xs-12" >


    <div class="box col-xs-12" >
        <div class="row">
            <div class="col-xs-3" style="margin: 20px 0 20px 0;">
                <button class="btn btn-primary" id="addteam" data-toggle="modal" data-target="#myModal"><i class="fa fa-lg fa-plus-square-o"></i>  Add User</button>
            </div>
        </div>
        <!--            toggle div-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div>



                    <div class="modal-body">
                        <div id="successUser-alert" class="alert alert-success alert-dismissable">
                            <!--                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>-->
                            Saved
                            <!--                        <a class="alert-link" href="#">Alert Link</a>-->
                        </div>

                        <div class="form-box" id="login-box" style="margin-bottom: 60px;">
                            <button type="button" class="close" style="margin: 3px 9px  0 0" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <div class="header">Add User</div>
                            <form id="addUserForm">
                                <div class="body bg-gray">

                                    <div class="form-group ui-widget">
                                            <input type="email" id="temail" name="temail"  style="height:38px;width:320px " class="form-control" placeholder="Email"/>
                                    </div>

                                </div>
                                <div class="footer">

                                    <button type="button" id="tregister" class="btn bg-olive btn-block">Add User</button>

                                </div>
                            </form>


                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--            toggle div-->

        <!--            toggle edit-->
        <div class="modal fade" id="ttedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div>



                    <div class="modal-body">
                        <div id="esuccessUser-alert" class="alert alert-success alert-dismissable">
                            <!--                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>-->
                            Updated
                            <!--                        <a class="alert-link" href="#">Alert Link</a>-->
                        </div>

                        <div class="form-box" id="login-box" style="margin-bottom: 60px; width:91%;">
                            <button type="button" class="close" style="margin: 3px 9px  0 0" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <div class="header">Edit Membership</div>
                            <form id="user-form-edit">
                                <div class="body bg-gray " style="height:458px " >
                                    <div class="form-group cus-margin">
                                        <label  class="col-sm-3 text-left control-label">User name</label>
                                        <div class="col-sm-7 cus-margin" >
                                        <input type="text" id="etfname" name="etfname" class="form-control" placeholder="Full name"/>
                                        </div>
                                    </div>
                                    <div class="form-group cus-margin ">
                                        <label  class="col-sm-3 text-left control-label">Email</label>
                                        <div class="col-sm-7 cus-margin" >
                                        <input type="text" id="etemail" name="etemail" class="form-control" placeholder="Email"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label  class="col-sm-3 text-left control-label">Title</label>
                                        <div class="col-sm-7 cus-margin" >
                                        <select id="etitle" name="etitle" class="form-control">
                                            <option selected="selected" value="">Choose a Job Title...</option>
                                            <option value="architect">Architect</option>
                                            <option value="business_analyst">Business Analyst</option>
                                            <option value="ceo">CEO</option><option value="cio">CIO</option>
                                            <option value="consultant">Consultant</option>
                                            <option value="cto">CTO</option>
                                            <option value="development_manager">Dev. Manager</option>
                                            <option value="it_manager">IT Manager</option>
                                            <option value="pmo_manager">PMO Manager</option>
                                            <option value="product_manager">Product Manager</option>
                                            <option value="professor">Professor</option>
                                            <option value="program_manager">Program Manager</option>
                                            <option value="project_manager">Project Manager</option>
                                            <option value="qa_engineer">QA Engineer</option>
                                            <option value="qa_manager">QA Manager</option>
                                            <option value="software_methodologist">SW Methodologist</option>
                                            <option value="student">Student</option>
                                            <option value="sw_developer">Software Developer</option>
                                            <option value="sw_engineer">Software Engineer</option>
                                            <option value="vp_development">VP Development</option>
                                            <option value="vp_engineering">VP Engineering</option>
                                            <option value="other">Other</option>
                                        </select>
                                            </div>

                                    </div>
                                    <div class="form-group">
                                        <label  class="col-sm-3 text-left control-label">Phone</label>
                                        <div class="col-sm-7 cus-margin">
                                        <input type="text" id="etphone" name="etphone" class="form-control" placeholder="Phone"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label  class="col-sm-3 text-left control-label">Department</label>
                                        <div class="col-sm-7 cus-margin">
                                        <input type="text" id="etdept" name="etdept" class="form-control" placeholder="Department"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-sm-3 text-left control-label">Office Loc</label>
                                        <div class="col-sm-7 cus-margin">
                                        <input type="text" id="etoffice" name="etoffice" class="form-control" placeholder="Office Location"/>
                                        </div>
                                    </div>

                                </div>
                                <div class="footer">

                                    <button type="button" id="edit_register" class="tt btn bg-olive btn-block">Update</button>

                                </div>
                            </form>


                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--            toggle edit-->


        <div class="box-body table-responsive">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <div class="row">


                </div>
                <table id="user-table" class="table table-bordered table-striped dataTable" aria-describedby="example1_info" style="font-size: 12px;">
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 189px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">ID</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 284px;" aria-label="Browser: activate to sort column ascending">User Name</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Email</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Role</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Department</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Title</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Phone</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Office Location</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Delete</th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Edit</th>


                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    <?php
                    $i=0;
                    foreach($userdetails as $row ):


                            if($row['loginId']==$ownerid){
                            ?>

                            <tr class="even">
                                <td class=" sorting_1"><?php echo $row['loginId']; ?></td>
                                <td class=" "><?php echo $row['name']; ?></td>
                                <td class=" "><?php echo $row['email'];?></td>
                                <td class=" ">Admin</td>
                                <td class=" "><?php echo $row['deptt']; ?></td>
                                <td class=" "><?php echo $row['title']; ?></td>
                                <td class=" "><?php echo $row['phone']; ?></td>
                                <td class=" "><?php echo $row['office']; ?></td>
                                <td class="center"><button class="btn btn-outline btn-default delete-product" disabled><i class="fa fa-trash-o"></i></button></td>
                                <td class="center"><button id="admin"  class="btn btn-outline btn-default edit-user" data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button></td>

                            </tr>
                    <?php }else{?>
                        <tr class="even">
                            <td class=" sorting_1"><?php echo $row['loginId']; ?></td>
                            <td class=" "><?php echo $row['name']; ?></td>
                            <td class=" "><?php echo $row['email'];?></td>
                            <td class=" ">Member</td>
                            <td class=" "><?php echo $row['deptt']; ?></td>
                            <td class=" "><?php echo $row['title']; ?></td>
                            <td class=" "><?php echo $row['phone']; ?></td>
                            <td class=" "><?php echo $row['office']; ?></td>

                                <td class="center"><button class="btn btn-outline btn-default delete-user"><i class="fa fa-trash-o"></i></button></td>
                                <td class="center"><button class="btn btn-outline btn-default edit-user" data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button></td>

                            </tr>


                            <?php }?>


                        <?php endforeach; ?>

                    </tbody>
                </table>
                <div class="row">
                </div>
            </div>
        </div>

    </div>

</section>






