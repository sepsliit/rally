<?php ?>




<div class="col-lg-6">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
</div>
<section class="content">


<div class="col-lg-12" >


<div class="box col-lg-12" >

    <div class="row">
        <div class="box box-info">
            <div class="box-header" style="cursor: move;">
                <i class="fa fa-envelope"></i>
                <h3 class="box-title">Quick Email</h3>
                <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="remove" data-original-title="Remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <form action="#" method="post">
<!--                    <div class="form-group">-->
                        <input id="name" type="hidden" value="<?php echo Yii::app()->user->getState("username");?>"/>
<!--                    </div>-->
                    <div class="form-group">
                        <input id="mail_subject" type="text" class="form-control" name="subject" placeholder="Subject"/>
                    </div>
                    <div>
                        <textarea id="mail_content" class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                </form>
            </div>
            <div class="box-footer clearfix">
                <button id="sendEmail" class="pull-right btn btn-default">
                    Send
                    <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>
        </div>

    </div>


</div>

</section>











