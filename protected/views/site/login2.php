<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>



<div class="form-box" id="login-box">
    <div class="header">Sign In</div>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<!--	<p class="note">Fields with <span class="required">*</span> are required.</p>-->




            <div class="body bg-gray">
                <div class="form-group">
                    <?php echo $form->textField($model,'username',array('class'=>'form-control',
                        'placeholder'=>'User ID','name'=>'userid'
                    )); ?>
                    <?php echo $form->error($model,'username',array('class'=>' alert-danger ')); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->passwordField($model,'password',array('class'=>'form-control',
                            'placeholder'=>'Password','name'=>"password"
                        )
                    ); ?>
                    <?php echo $form->error($model,'password' ,array('class'=>' alert-danger ')); ?>
                </div>
                <div class="form-group">
<!--                    <input type="checkbox" name="remember_me"/> Remember me-->

                        <label>
                            <?php echo $form->checkBox($model,'rememberMe'); ?>
                            <?php echo $form->label($model,'rememberMe'); ?>
                            <?php echo $form->error($model,'rememberMe'); ?>
                            <!--                                    <input name="remember" type="checkbox" value="Remember Me">Remember Me-->
                        </label>

                </div>
            </div>
            <div class="footer">
<!--                <button type="submit" class="btn bg-olive btn-block">Sign me in</button>-->
                <?php echo CHtml::submitButton('Sign me in',array('class'=>'btn bg-olive btn-block')); ?>


<!--                <p><a href="#">I forgot my password</a></p>-->
<!---->
<!--                <a href="register.html" class="text-center">Register a new membership</a>-->
                <?php echo CHtml::link('Register a new membership', $this->createAbsoluteUrl('user/register'),array('class'=>'text-center')); ?>

            </div>


        <div class="margin text-center">
            <span>Sign in using social networks</span>
            <br/>
            <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
            <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
            <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

        </div>
<?php $this->endWidget(); ?>
</div><!-- form -->
</div>

