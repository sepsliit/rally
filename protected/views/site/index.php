<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<!--project add form  ------>
<div class="modal fade" id="myModalproject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 800px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Project</h4>
            </div>
            <div class="modal-body">
                <div id="tsuccess-alert" class="alert alert-success alert-dismissable">

                </div>

                <form id="myprojectForm" class="form-horizontal my_modal" >
                    <!--Task name -->
                    <div class="form-group">
                        <label id="lblname" class="col-sm-2 control-label">Project Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="proname" name="name" placeholder="Project Name">

                        </div>
                    </div>


                    <!--Description -->

                    <div class="form-group">
                        <label id="lbldes" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">

                            <textarea cols="50" placeholder="Description"   id="prodescription" name="description" rows="5"  maxlength="500" class="form-control" ></textarea>


                        </div>
                    </div>




                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="save_project" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!--end project add form  -->






<div class=" col-xs-12">
    <section class="content-header">
        <h1>
            <span id="proj_title"><?php echo Yii::app()->user->getState("project_n");?></span>
            <small>Control panel</small>

        </h1>
        <ol class="breadcrumb col-xs-12 ">
            <li class="active">Dashboard</li>
        </ol>
    </section>
</div>

<div class=" row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-5">
                <button id="proj" class="btn bg-maroon margin" data-toggle="modal" data-target="#myModalproject"><span class="fa fa-lg fa-plus-circle"></span> Add Project</button>
            </div>
        </div>
        <div id="rowproject" class="row">

            <?php foreach($workspace as $row): ?>
                <?php $id = Yii::app()->user->getState("logId");
                if($row->workspaceid == $id){
                    ?>
                    <div id="<?php  echo $row->id; ?>" class="col-xs-3 project">
                        <div class="box box-solid bg-green hbg-navy ">
                            <div class="box-header headerp">
                                <?php $name = Workspace::model()->findByPk($row->workspaceid);?>
                                <h3 id="<?php  echo $name->id; ?>" class="head box-title"><?php $pname = $name->name;echo $pname; ?></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn bg-light-blue btn-sm delete" style="background-color: transparent;" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body parap">

                                <p class="para" data-id="<?php  echo $name->id; ?>" id="para<?php  echo $name->id; ?>"><?php  echo $name->des; ?> </p>

                            </div>
                        </div>
                    </div>
                <?php }else{ ?>

                    <div id="<?php  echo $row->id; ?>" class="col-xs-3 project">
                        <div class="box box-solid bg-navy hbg-navy ">
                            <div class="box-header headerp">
                                <?php $name = Workspace::model()->findByPk($row->workspaceid);?>
                                <h3 id="<?php  echo $name->id; ?>" class="head box-title"><?php  echo $name->name; ?></h3>
                                <div class="box-tools pull-right">
                                    <button class="btn bg-light-blue btn-sm delete" style="background-color: transparent;" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body parap">

                                <p class="para" data-id="<?php  echo $name->id; ?>" id="para<?php  echo $name->id; ?>"><?php  echo $name->des; ?> </p>

                            </div>
                        </div>
                    </div>

                <?php  } endforeach; ?>

        </div>

        </di>

    </div>
    <script>
        $(document).ready(function(){
<!--            var name = '--><?php //echo $pname; ?><!--'-->
<!--            $('#proj_title').text(name)-->

        })

    </script>



