
<div class="col-xs-12">
    <section class="content-header">
        <h1>
            <span class="proj_title"><?php echo Yii::app()->user->getState("project_n");?></span>
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb col-xs-12 ">
            <li><a href="index.php?r=site/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"></li>
        </ol>
    </section>
</div>
<section class="content">


<div class="col-xs-12" >


<div class="box col-xs-12" >
<div class="row">
    <div class="col-xs-3" style="margin: 20px 0 20px 0;">
        <button class="btn btn-primary" id="addteam" data-toggle="modal" data-target="#myModal"><i class="fa fa-lg fa-plus-square-o"></i>  Add Defect</button>
    </div>
</div>
<!--            toggle div-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div>



            <div class="modal-body">
                <div id="successUser-alert" class="alert alert-success alert-dismissable">
                    <!--                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>-->
                    Saved
                    <!--                        <a class="alert-link" href="#">Alert Link</a>-->
                </div>

                <div class="form-box" id="login-box" style="margin-bottom: 60px; width:91%;">
                    <button type="button" class="close" style="margin: 3px 9px  0 0" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="header">Add Defect</div>
                    <form id="mydefect" class="bv-form">
                        <div class="body bg-gray " style="height:590px" >

                            <div class="form-group cus-margin">
                                <label id="lblname" class="col-sm-3 control-label">Name</label>

                                <div class="col-sm-7 cus-margin">
                                    <input type="text" class="form-control" id="dname" name="dname" placeholder="Defect Name"
                                           data-validation="required"
                                           style="border: 1px solid #ccc;border-radius: 4px;"/>
                                </div>
                            </div>

                            <div class="form-group cus-margin">
                                <label id="lbldes" class="col-sm-3 text-left control-label">Description</label>
                                <div class="col-sm-7 cus-margin">
                                    <textarea cols="50" id="description" id="dreason" name="dreason"  class="form-control"  name="description" rows="5" data-validation="required" maxlength="500"
                                              class="form-control">
                                    </textarea>

                                </div>
                            </div>

                            <div class="form-group cus-margin">
                                <label class="col-sm-3 text-left control-label">Owner</label>
                                <div class="col-sm-7 cus-margin" >
                                    <select id="ownerd" class="form-control" name="ownerd">

                                        <?php foreach ($user as $row): ?>
                                            <option value="<?php echo($row['username']); ?>"><?php echo($row['username']); ?> </option>
                                        <?php endforeach; ?>

                                        <?php foreach ($team as $row1): ?>
                                            <option value="<?php echo($row1['email']); ?>"><?php echo($row1['email']); ?> </option>
                                        <?php endforeach; ?>

                                    </select>

                                </div>
                            </div>

                            <div class="form-group cus-margin">
                                <label class="col-sm-3 text-left control-label">State</label>
                                <div class="col-sm-7 cus-margin" >
                                    <select id="state" class="form-control" name="state">
                                        <option value="Submitted">Submitted</option>
                                        <option value="Open"> Open</option>
                                        <option value="Fixed">Fixed</option>
                                        <option value="Closed">Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group cus-margin ">
                                <label class="col-sm-3 text-left control-label">Iteration</label>
                                <div class="col-sm-7 cus-margin">
                                        <select id="iteration" class="form-control" name="iteration">
                                            <!--                <option value="Unsheduled">Unsheduled</option>-->
                                            <?php foreach ($iterations as $row): ?>
                                                <option value="<?php echo($row['id']); ?>"><?php echo($row['name']); ?></option>
                                            <?php endforeach; ?>
                                        </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 text-left control-label">Priority</label>
                                <div class="col-sm-7 cus-margin">
                                    <select id="priority" class="form-control" name="priority">
                                        <option value="no_entry">«No Entry»</option>
                                        <option value="immediate">1 - Resolve Immediately</option>
                                        <option value="attention"> 2 - High Attention</option>
                                        <option value="normal">3 - Normal</option>
                                        <option value="low"> 4 - Low</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 text-left control-label">Stage</label>
                                <div class="col-sm-7 cus-margin">
                                    <select id="stage" class="form-control" name="stage">
                                        <option value="Defined">Defined</option>
                                        <option value="Inprogress"> Inprogress</option>
                                        <option value="Accepted">Accepted</option>
                                        <option value="Completed">Completed</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 text-left control-label">Userstory Name</label>
                                <div class="col-sm-7 cus-margin">
                                    <select id="usid" class="form-control drop_button" name="usid"
                                            style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                        <?php foreach ($storyid as $row): ?>
                                            <option value="<?php echo($row['id']); ?>"><?php echo($row['uname']); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label id="reason" class="col-sm-3 text-left control-label">Reason</label>
                                <div class="col-sm-7 cus-margin">

                                        <!--                                <textarea cols="50"    id="dreason" name="dreason" rows="5" data-validation="required" maxlength="500"data-validation-error-msg=" User Story description is required " class="form-control" ></textarea>-->
                                        <!--                                <input type="text"    id="dreason" name="dreason" class="form-control" placeholder="Reason">-->

                                        <input type="text" class="form-control" id="dreason" name="dreason" placeholder="Reason"
                                               data-validation="required"
                                               style="border: 1px solid #ccc;border-radius: 4px;"/>

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 text-left control-label">Plan Estimate(Hrs)</label>
                                <div class="col-sm-7 cus-margin">
                                        <input type="text" class="form-control" id="best" name="best" placeholder="Estimated hours"
                                               data-validation="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <button type="button" id="saveDefect" class="tt btn bg-olive btn-block">Add Defect</button>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
<!--            toggle div-->

<!--            toggle edit-->
<div class="modal fade" id="ttedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div>



            <div class="modal-body">
                <div id="esuccessUser-alert" class="alert alert-success alert-dismissable">
                    <!--                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>-->
                    Updated
                    <!--                        <a class="alert-link" href="#">Alert Link</a>-->
                </div>

                <div class="form-box" id="login-box" style="margin-bottom: 60px; width:91%;">
                    <button type="button" class="close" style="margin: 3px 9px  0 0" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="header">Edit Defect</div>
                    <form id="user-form-edit" class="bv-form">
                        <div class="body bg-gray " style="height:590px" >

                            <div class="form-group cus-margin">
                                <label id="lblname" class="col-sm-3 control-label">Name</label>

                                <div class="col-sm-7 cus-margin">
                                    <input type="text" class="form-control" id="edname" name="dname" placeholder="Defect Name"
                                           data-validation="required"
                                           style="border: 1px solid #ccc;border-radius: 4px;"/>
                                </div>
                            </div>

                            <div class="form-group cus-margin">
                                <label id="lbldes" class="col-sm-3 text-left control-label">Description</label>
                                <div class="col-sm-7 cus-margin">
                                    <textarea cols="50" id="edescription" id="edreason" name="dreason"  class="form-control"  name="description" rows="5" data-validation="required" maxlength="500"
                                              class="form-control">
                                    </textarea>

                                </div>
                            </div>

                            <div class="form-group cus-margin">
                                <label class="col-sm-3 text-left control-label">Owner</label>
                                <div class="col-sm-7 cus-margin" >
                                    <select id="eownerd" class="form-control" name="ownerd">

                                        <?php foreach ($user as $row): ?>
                                            <option value="<?php echo($row['username']); ?>"><?php echo($row['username']); ?> </option>
                                        <?php endforeach; ?>

                                        <?php foreach ($team as $row1): ?>
                                            <option value="<?php echo($row1['email']); ?>"><?php echo($row1['email']); ?> </option>
                                        <?php endforeach; ?>

                                    </select>

                                </div>
                            </div>

                            <div class="form-group cus-margin">
                                <label class="col-sm-3 text-left control-label">State</label>
                                <div class="col-sm-7 cus-margin" >
                                    <select id="estate" class="form-control" name="state">
                                        <option value="Submitted">Submitted</option>
                                        <option value="Open"> Open</option>
                                        <option value="Fixed">Fixed</option>
                                        <option value="Closed">Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group cus-margin ">
                                <label class="col-sm-3 text-left control-label">Iteration</label>
                                <div class="col-sm-7 cus-margin">
                                    <select id="eiteration" class="form-control" name="iteration">
                                        <!--                <option value="Unsheduled">Unsheduled</option>-->
                                        <?php foreach ($iter as $row): ?>
                                            <option value="<?php echo($row['id']); ?>"><?php echo($row['name']); ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 text-left control-label">Priority</label>
                                <div class="col-sm-7 cus-margin">
                                    <select id="epriority" class="form-control" name="priority">
                                        <option value="no_entry">«No Entry»</option>
                                        <option value="immediate">1 - Resolve Immediately</option>
                                        <option value="attention"> 2 - High Attention</option>
                                        <option value="normal">3 - Normal</option>
                                        <option value="low"> 4 - Low</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 text-left control-label">Stage</label>
                                <div class="col-sm-7 cus-margin">
                                    <select id="estage" class="form-control" name="stage">
                                        <option value="Defined">Defined</option>
                                        <option value="Inprogress"> Inprogress</option>
                                        <option value="Accepted">Accepted</option>
                                        <option value="Completed">Completed</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 text-left control-label">Userstory Name</label>
                                <div class="col-sm-7 cus-margin">
                                    <select id="eusid" class="form-control drop_button" name="usid"
                                            style="font-size: 10px; width: 125px;margin-top: 5px; ">
                                        <?php foreach ($storyid as $row): ?>
                                            <option value="<?php echo($row['id']); ?>"><?php echo($row['uname']); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label id="reason" class="col-sm-3 text-left control-label">Reason</label>
                                <div class="col-sm-7 cus-margin">

                                    <!--                                <textarea cols="50"    id="dreason" name="dreason" rows="5" data-validation="required" maxlength="500"data-validation-error-msg=" User Story description is required " class="form-control" ></textarea>-->
                                    <!--                                <input type="text"    id="dreason" name="dreason" class="form-control" placeholder="Reason">-->

                                    <input type="text" class="form-control" id="edreason" name="dreason" placeholder="Reason"
                                           data-validation="required"
                                           style="border: 1px solid #ccc;border-radius: 4px;"/>

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 text-left control-label">Plan Estimate(Hrs)</label>
                                <div class="col-sm-7 cus-margin">
                                    <input type="text" class="form-control" id="ebest" name="best" placeholder="Estimated hours"
                                           data-validation="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <button type="button" id="update_defect" class="tt btn bg-olive btn-block">Update</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!--            toggle edit-->


<div class="box-body table-responsive">
    <div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
        <div class="row">


        </div>
        <table id="defect-table" class="table table-bordered table-striped dataTable" aria-describedby="example1_info" style="font-size: 12px;">
            <thead>
            <tr role="row">
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 189px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Defect Id</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 284px;" aria-label="Browser: activate to sort column ascending">Defect Name</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Description</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Reason</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Priority</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Estimated Hours</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Stage</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">State</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Owner</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Iteration</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Delete</th>
                <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 246px;" aria-label="Platform(s): activate to sort column ascending">Edit</th>


            </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php
            $i=0;
            foreach($defects as $row ): ?>

                    <tr class="even">
                        <td class=" sorting_1"><a id="<?php echo $row['id']; ?>kk" name='<?php echo $row['id']; ?>'><?php echo $row['id']; ?></a></td>
                        <td class=" "><?php echo $row['dname']; ?></td>
                        <td class=" "><?php echo $row['dis']; ?></td>
                        <td class=" "><?php echo $row['breason']; ?></td>
                        <td class=" "><?php echo $row['priority']; ?></td>
                        <td class=" "><?php echo $row['est']; ?></td>
                        <td class=" "><?php echo $row['stage']; ?></td>
                        <td class=" "><?php echo $row['state']; ?></td>
                        <td class=" "><?php echo $row['owner']; ?></td>
                        <td class=" "><?php echo $row['sstate0']->name; ?></td>
                        <td class="center"><button class="btn btn-outline btn-default delete-defect"><i class="fa fa-trash-o"></i></button></td>
                        <td class="center"><button class="btn btn-outline btn-default edit-defect" data-toggle="modal" data-target="#ttedit"><i class="fa fa-edit"></i></button></td>

                    </tr>


                <?php endforeach; ?>

            </tbody>
        </table>
        <div class="row">
        </div>
    </div>
</div>

</div>

</section>