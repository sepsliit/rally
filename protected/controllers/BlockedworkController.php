<?php

class BlockedworkController extends Controller
{
    public function actionIndex()
    {


        $logid = Yii::app()->user->getState('logId');


        $connection = Yii::app()->db;
        $block = $connection->createCommand("select * from user_stories where breason!='' and login_workspaceid='" . $logid . "' ");
        $p = $block->query();


        $this->render('block', array(
            'model8' => $p
        ));
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}