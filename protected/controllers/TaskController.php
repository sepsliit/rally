<?php

class TaskController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','savetask','deleteTask',),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print','charts','drawcharts'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


    //display chart
    public  function actionCharts()
    {
        $this->render('charts');
    }
    //display chart  ends here

    //draw charts
    public  function actionDrawcharts()
    {


        $usid=$_POST['usid'];
        $dcolor=$_POST['dcolor'];
        $ccolor=$_POST['ccolor'];
        $acolor=$_POST['acolor'];
        $bcolor=$_POST['bcolor'];
        $icolor=$_POST['icolor'];

        if($dcolor==null)
        {
            $dcolor='#000000';
        }

        if($ccolor==null)
        {
            $ccolor='#000000';
        }
        if($acolor==null)
        {
            $acolor='#000000';
        }
        if($icolor==null)
        {
            $icolor='#000000';
        }
        if($bcolor==null)
        {
            $bcolor='#000000';
        }


        $null=null;


        if($usid!='null')
        {
            $allTask="SELECT COUNT(*) FROM task where user_storiesid='".$usid."' ";
            $allTaskCount = Yii::app()->db->createCommand($allTask)->queryScalar();
            $definedts = "SELECT COUNT(*) FROM task where state='defined' AND user_storiesid='".$usid."' AND breason='".$null."' ";
            $definedTask = Yii::app()->db->createCommand($definedts)->queryScalar();
            $inprogressts = "SELECT COUNT(*) FROM task where state='In progress' AND user_storiesid='".$usid."' AND breason='".$null."' ";
            $inprogressTask = Yii::app()->db->createCommand($inprogressts)->queryScalar();
            $acceptedts = "SELECT COUNT(*) FROM task where state='Accepted' AND user_storiesid='".$usid."' AND breason='".$null."' ";
            $acceptedTask = Yii::app()->db->createCommand($acceptedts)->queryScalar();
            $completedts= "SELECT COUNT(*) FROM task where state='Completed' AND user_storiesid='".$usid."' AND breason='".$null."' ";
            $completedTask = Yii::app()->db->createCommand($completedts)->queryScalar();
            $blockts= "SELECT COUNT(*) FROM task where breason!='".$null." 'AND user_storiesid='".$usid."' ";
            $blockTask = Yii::app()->db->createCommand($blockts)->queryScalar();

            if($allTaskCount==0)
            {
                $allTaskCount==1;
            }

            $data = array('dTask' => $definedTask, 'iTask' => $inprogressTask ,'aTask'=>$acceptedTask,'cTask'=>$completedTask,'bTask'=>$blockTask,'allTask'=>$allTaskCount
            ,'dcolor'=>$dcolor ,'icolor'=>$icolor,'acolor'=>$acolor,'ccolor'=>$ccolor,'bcolor'=>$bcolor
            );

            $encode= CJSON::encode($data);
            // $decode=CJSON::decode($encode);
            print_r($encode);


        }
        else
        {
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode('error');
            Yii::app()->end();
        }


    }



    public function actionPrint()
    {
        $invoice_items = array();


        $logid = Yii::app()->user->getState("logId");
        $name = Yii::app()->user->getState("username");
        $dataProvider=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$logid));



        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
            'P', 'cm', 'A5', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);


        $pdf->AddPage();

        $html = " Details of User Story ";
        $pdf->SetFont('times', 'B,U', 12);

        $pdf->SetFillColor(255, 255, 127);

        $pdf->MultiCell(13,0, $html, 0, 'C', 0, 0, '', '', true);

        $pdf->Ln(2);

//invoice
        $pdf->SetFont('times', '', 12);
        $html="User";
        $pdf->SetFillColor(255, 255, 127);

        $pdf->MultiCell(2, 0, $html, 1, 'C', 0, 2, '12', '2', true, 0, false, true, 40, '');

        $pdf->Ln(0);

        $pdf->SetFont('times', 'B', 9);
// set color for text
        $pdf->SetTextColor(0, 63, 127);
        $html="Rally Agile Software
Tel.     : 0112957070
Email : test@gmail.com";
        $pdf->SetFillColor(255, 255, 127);

        $pdf->MultiCell(6, 0, $html, 0, 'L', 0, 2, '1', '2.9', true, 0, false, true, 40, '');

        $pdf->Ln(0);

//date
        $pdf->SetFont('times', 'B', 9);
// set color for text
        $pdf->SetTextColor(0, 0, 0);
        $html="Date";
        $pdf->SetFillColor(196, 211, 214);

        $pdf->MultiCell(3, 0, $html,0 , 'L', 1, 2, '11', '3', true, 0, false, true, 40, '');

        $pdf->Ln(0);

// set font
        $pdf->SetFont('times', '', 9);
        $html=date("Y-m-d");
        $pdf->SetFillColor(255, 255, 127);

        $pdf->MultiCell(3, 0, $html, 0, 'L', 0, 2, '11', '3.5', true, 0, false, true, 40, '');

        $pdf->Ln(0);

//invoice #
        $pdf->SetFont('times', 'B', 9);
        $html="Work Space #";
        $pdf->SetFillColor(196, 211, 214);

        $pdf->MultiCell(3, 0, $html, 0, 'L', 1, 2, '11', '4', true, 0, false, true, 40, '');

        $pdf->Ln(0);

// set font
        $pdf->SetFont('times', '', 9);
        $html=$logid;
        $pdf->SetFillColor(255, 255, 127);

        $pdf->MultiCell(3, 0, $html, 0, 'L', 0, 2, '11', '4.5', true, 0, false, true, 40, '');

        $pdf->Ln(0);


//customer
        $pdf->SetFont('times', 'B', 9);
        $html="User Name";
        $pdf->SetFillColor(196, 211, 214);

        $pdf->MultiCell(3, 0, $html, 0, 'L', 1, 2, '11', '5', true, 0, false, true, 40, '');

        $pdf->Ln(2);

// set font
        $pdf->SetFont('times', '', 9);
        $html=$name;
        $pdf->SetFillColor(255, 255, 127);

        $pdf->MultiCell(3, 0, $html, 0, 'L', 0, 1, '11', '5.5', true, 0, false, true, 40, '');

        $pdf->Ln(0);



//table






        $html = '<table id="items"  style="border: 1px solid #cbcbcb;" >
          <tr style="background-color: #c4d3d6; border-bottom: 1px solid #000000;">
		      <th>User Story ID</th>
		      <th >User Story Name</th>
		      <th>Description</th>
		      <th>Owner</th>
		      <th>State</th>
		      <th>Stage</th>


		  </tr>

		 ';

//$html .=
//$pdf->SetFillColor(255, 255, 127);
//$pdf->SetFont('times', 'B', 10);
//
//$pdf->SetFillColor(255, 255, 127);
//// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
//
//
//
//$pdf->MultiCell(13, 0, $html, 1, 'C', 0, 2, '', '7', true, 0, true, false, 40, 'M');
//
//$pdf->Ln(0);

        $total =0;$html1='';
        foreach( $dataProvider as $i){

                $html1 .= ' <tr class="item-row" style=" border: 1px solid #000000;">
		      <td>'.$i->id.'</td>
		      <td>'.$i->uname.'</td>
		      <td>'.$i->dis.'</td>
		      <td>'.$i->owner.'</td>
		      <td>'.$i->state.'</td>
		      <td>'.$i->stage.'</td>


		  </tr>';



        }

        $html .= $html1.'



		</table>





';

        $pdf->SetTextColor(11, 11, 11);
        $pdf->SetFont('times', '', 11);

        $pdf->SetFillColor(255, 255, 127);
// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)



        $pdf->MultiCell(13, 0, $html,0, 'C', 0, 2, '', '7', true, 0, true, true, 40, '');

        $pdf->Ln(0);
//        $total = number_format((float)$total, 2, '.', '');
////        $html ='<span>Total : <span style="font-size: 9px;">Rs. </span>'.$total.'</span>';
//
//        $pdf->SetTextColor(11, 11, 11);
////$pdf->SetFont('times', '', 12);
//
//        $pdf->SetFillColor(255, 255, 127);
//// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
//// set cell margins
//        $pdf->setCellMargins(0, 1, 1, 1);


//        $pdf->MultiCell(4, 0, $html,1, 'C', 0, 2, '9.9', '', true, 0, true, true, 40, '');

//        $pdf->Ln(0);
//
//        $html ='<hr/>';
//
//        $pdf->SetTextColor(11, 11, 11);
//        $pdf->SetFont('times', '', 5);
//
//        $pdf->SetFillColor(255, 255, 127);
//// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
//// set cell margins
//        $pdf->setCellMargins(0, 0, 0, 0);





//        $pdf->MultiCell(13, 0, $html,0, '', 0, 0, '', '14',false, 0, true, false, 0, '');
//
//        $pdf->Ln(0);
//
//        $html ='Solution By SLIIT Rally ';
//
//        $pdf->SetTextColor(11, 11, 11);
//        $pdf->SetFont('times', '', 5);

//$pdf->SetFillColor(255, 255, 127);
// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
// set cell margins
//        $pdf->setCellMargins(0, 0, 0, 0);
//
//
//        $pdf->MultiCell(13, 0, $html,0, 'R', 0, 0, '', '18',false, 0, true, false, 0, '');
//
//        $pdf->Ln(0);



//$html='<p style="background-color: #00a65a;">sdaasda</p>';
//// output the HTML content
//$pdf->writeHTML($html, true, true, true, true, '20','1');



// reset pointer to the last page
//$pdf->lastPage();




//$pdf->SetFont("times", "BI", 20);
//$pdf->Cell(0,10,"Example 002",1,1,'C');
        $pdf->lastPage();
        ob_clean();

        $pdf->Output("UserStories.pdf", "I");
        Yii::app()->end();
    }

    public  function actionSavetask()
    {
        // $uname=$_POST['uname'];
        $uid=$_POST['uid'];
        $name= $_POST['name'];
        $des= $_POST['description'];
        $owner= $_POST['owner'];
        $state= $_POST['state'];
        $iteration= $_POST['iteration'];
        $block= $_POST['reason'];
        $hrs= $_POST['estimate'];
        $todo=$_POST['todo'];

        $model=new Task;


        //$personId = Yii::app()->user->getState("u");
        // $wid = Workspace::model()->findByAttributes(array('id'=>));

        $model->user_storiesid=$uid;
        $model->tname=$name;
        $model->dis=$des;
        $model->owner=$owner;
        $model->est=$hrs;
        $model->stage=$iteration;
        $model->state=$state;
        $model->todo=$todo;
        $model->breason=$block;



        if(!empty($name)  && !empty($des) && !empty($owner))
        {
            if($model->save()){


                // $decode=CJSON::decode($encode);
                // print_r($encode);
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode($model);
                Yii::app()->end();

            }
            else
            {
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode("error");

            }



        }
        else
        {
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode("error");

        }

    }




    public  function  actionDeleteTask()
    {
        $taskid = Yii::app()->request->getPost('tid');
        $model=Task::model()->findByPk($taskid);
        if($model->delete()){


            header('Content-type: application/json');
            echo CJavaScript::jsonEncode("success");
            Yii::app()->end();
        }
        else
        {
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode("error");
            Yii::app()->end();

        }
    }


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Task;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Task']))
		{
			$model->attributes=$_POST['Task'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{

        $task=$_POST['id'];
        $uid=$_POST['uid'];
        $name= $_POST['name'];
        $des= $_POST['description'];
        $owner= $_POST['owner'];
        $state= $_POST['state'];
        $iteration= $_POST['iteration'];
        $block= $_POST['reason'];
        $hrs= $_POST['estimate'];
        $todo=$_POST['todo'];

        $model=Task::model()->findByPk($task);


        //$personId = Yii::app()->user->getState("u");
        // $wid = Workspace::model()->findByAttributes(array('id'=>));

        $model->user_storiesid=$uid;
        $model->tname=$name;
        $model->dis=$des;
        $model->owner=$owner;
        $model->est=$hrs;
        $model->stage=$iteration;
        $model->state=$state;
        $model->todo=$todo;
        $model->breason=$block;



        if(!empty($name)  && !empty($des) && !empty($owner))
        {
            if($model->save()){


                // $decode=CJSON::decode($encode);
                // print_r($encode);
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode($model);
                Yii::app()->end();

            }
            else
            {
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode("error2");

            }



        }
        else
        {
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode("error1");

        }




    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		$dataProvider=new CActiveDataProvider('Task');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
//
//        $criteria = new CDbCriteria;
//        $criteria->order = 'index ASC';
//
//        $criteria=new CDbCriteria;
//        $criteria->select='*';  // only select the 'title' column
//        $criteria->condition='postID=:postID';
//        $criteria->params=array(':postID'=>10);


        $logid = Yii::app()->user->getState("logId");
        $dataProvider=UserStories::model()->findAll(array(
            'condition'=> 'login_workspaceid="'.$logid.'"',
            'order'=>'index_id ASC',
        ));
        $iteration=Iteration::model()->findAllByAttributes(array('login_workspaceid'=>$logid));

        $this->render('index',array(
            'dataProvider'=>$dataProvider,'iteration'=>$iteration
        ));


	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Task('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Task']))
			$model->attributes=$_GET['Task'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Task the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Task::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Task $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='task-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
