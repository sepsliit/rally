<?php

class MyDefectController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'defectsForm', 'addDefects','EditDefect', 'SetFields', 'defect', 'calender'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'delete_defect'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {

        $id = Yii::app()->user->getState("userId");
        $logid = Yii::app()->user->getState("logId");

        if (isset($_GET['id'])) {

            $id1 = $_GET['id'];

            $noitfi = NotificationUpdate::model()->find(array(
                'condition' => 'notificationid = :nid AND userid = :userid',
                'params' => array(':nid' => $id1, ':userid' => $id)
            ));

            if($noitfi != null)
            {
                $noitfi->lastviewed = new CDbExpression('CURRENT_TIMESTAMP');
                $noitfi->save(false);
            }
            else {
                $notification = new NotificationUpdate();
                $notification->lastviewed = new CDbExpression('CURRENT_TIMESTAMP');
                $notification->notificationid =$id1;
                $notification->userid = $id;
                $notification->save(false);
            }


        }


        $criteria = new CDbCriteria;
        $criteria->addCondition('t.login_workspaceid = :id');
        $criteria->params = array(':id' => $logid);

        $iterations = Iteration::model()->findAll($criteria);
        $defect = Defect::model()->with('sstate0')->findAll();


        //$defectId = $_POST['id'];
        //$stosid=Yii::app()->user->getState("project_n");
        //$defects = Defect::model()->with('sstate0')->findByPk($defectId);

        $wid = Yii::app()->user->getState("logId");
        $defect=Defect::model()->findAllByAttributes(array('wid'=>$wid));
        //$userstories=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$wid),array('login_workspaceid'=>$wid));



        /*
         *Form data
         */



        $user = Login::model()->findAllByAttributes(array('userid' => $id));

        $iter = Iteration::model()->findAllByAttributes(array('login_workspaceid' => $logid));
        $team = Team::model()->findAllByAttributes(array('uid' => $id));

        $condition = new CDbCriteria();
        $condition->condition = "login_workspaceid = :logid and iterationid IS NOT NULL";
        $condition->params = array(":logid" => $logid);

        $storyid = UserStories::model()->findAll($condition);

        $this->render('Defect', array(
            'defects' => $defect, 'iterations' => $iterations, 'user' => $user, 'team' => $team, 'storyid' => $storyid, 'iter' => $iter,
        ));

    }


    public function actionDefectsForm()
    {
        /*
        $id = Yii::app()->user->getState("userId");
        $logid = Yii::app()->user->getState("logId");

        $user = Login::model()->findAllByAttributes(array('userid' => $id));
        $iter = Iteration::model()->findAllByAttributes(array('login_workspaceid' => $logid));
        $team = Team::model()->findAllByAttributes(array('uid' => $id));
        $storyid = UserStories::model()->findAllByAttributes(array('login_workspaceid' => $logid));


        $this->render('defectsForm', array(

            'user' => $user, 'iter' => $iter, 'team' => $team, 'storyid' => $storyid,
        ));

        */

    }

    public function actionEditDefect()
    {
        $id = $_POST['id'];
        $name = $_POST['dname'];
        $description = $_POST['description'];
        $ownerd = $_POST['ownerd'];
        $state = $_POST ['state'];
        $iteration = $_POST['iteration'];
        $priority = $_POST['priority'];
        $stage = $_POST['stage'];
        $best = $_POST['best'];
        $reason = $_POST['reason'];
        $usid = $_POST['usid'];


        $model = Defect::model()->findByPk($id);
        $model->dname = $name;
        $model->dis = $description;
        $model->breason = $reason;
        $model->est = $best;
        $model->priority = $priority;
        $model->owner = $ownerd;
        $model->state = $state;
        $model->stage = $stage;
        $model->sstate = $iteration;
        $model->user_storiesid = $usid;


        if (!empty($description) && !empty($name) && !empty($reason) && !empty($best)) {
            if (is_numeric($best)) {
                if ($model->save()) {
                    $story = UserStories::model()->findByPk($model->user_storiesid);
                    $iter = Iteration::model()->findByPk($story->iterationid);

                    $details = array(
                        'id' => $model->id,
                        'dname' => $model->dname,
                        'breason' => $model->breason,
                        'dis' => $model->dis,
                        'est' => $model->est,
                        'priority' => $model->priority,
                        'stage' => $model->priority,
                        'owner' => $model->owner,
                        'state' => $model->state,
                        'stage' => $model->stage,
                        'sstate' => $model->sstate,
                        'user_storiesid' => $model->user_storiesid,
                        'iteration' => $iter->name
                    );

                    header('Content-type: application/json');
                    echo CJavaScript::jsonEncode($details);
                    Yii::app()->end();

                }
            } else {
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode('error1');
                Yii::app()->end();

            }
        } else {
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode('error');
            Yii::app()->end();
        }
    }

    public function  actionAddDefects()
    {

        $errMsg = "";
        $errMsg2 = "";


        $logid = Yii::app()->user->getState("logId");
        $name = $_POST['dname'];
        $description = $_POST['description'];
        $ownerd = $_POST['ownerd'];
        $state = $_POST ['state'];

        $iteration = $_POST['iteration'];
        $priority = $_POST['priority'];
        $stage = $_POST['stage'];

        $best = $_POST['best'];
        $reason = $_POST['reason'];
        $usid = $_POST['usid'];


        $defect = new Defect;
        $defect->dname = $name;
        $defect->dis = $description;
        $defect->breason = $reason;
        $defect->est = $best;
        $defect->priority = $priority;
        $defect->owner = $ownerd;
        $defect->state = $state;
        $defect->stage = $stage;
        $defect->sstate = $iteration;
        $defect->user_storiesid = $usid;
        $defect->wid = $logid;

        if (!empty($description) && !empty($name) && !empty($reason) && !empty($best)) {
            if (is_numeric($best)) {
                if ($defect->save()) {

                    $story = UserStories::model()->findByPk($defect->user_storiesid);
                    $iter = Iteration::model()->findByPk($story->iterationid);
                    $wid = Yii::app()->user->getState("logId");

                    $details = array(
                        'id' => $defect->id,
                        'dname' => $defect->dname,
                        'breason' => $defect->breason,
                        'dis' => $defect->dis,
                        'est' => $defect->est,
                        'priority' => $defect->priority,
                        'stage' => $defect->priority,
                        'owner' => $defect->owner,
                        'state' => $defect->state,
                        'stage' => $defect->stage,
                        'sstate' => $defect->sstate,
                        'user_storiesid' => $defect->user_storiesid,
                        'iteration' => $iter->name,
                        'wid'=>$defect->wid,
                    );


                    $notification = new NotificationTable();
                    $notification->notifi_type = 2;
                    $notification->img = 'imgurl here';
                    $notification->headertext = 'New Defect';
                    $notification->subtext = 'New defect has been added '.$details["breason"];
                    $notification->subj_id = $details["id"];
                    $notification->added_date = new CDbExpression('CURRENT_TIMESTAMP');
                    $notification->wid = $details["wid"];
                    $notification->save();


                    header('Content-type: application/json');
                    echo CJavaScript::jsonEncode($details);
                    Yii::app()->end();

                }
            } else {
                header('Content-type: application/json');
                $errMsg = 'Enter numeric value for estimaied hours';
                echo CJavaScript::jsonEncode($errMsg);

            }
        } else {
            header('Content-type: application/json');
            $errMsg2 = 'Please fill all the feilds';
            echo CJavaScript::jsonEncode($errMsg2);
        }

    }


    public function actionCalender()
    {
        $this->render('calender', array());
    }


    public function actionDelete_defect()
    {

        $id = $_POST['id'];
        $model = Defect::model()->findByPk($id);
        if ($model->delete()) {


            header('Content-type: application/json');
            echo CJavaScript::jsonEncode("success");
            Yii::app()->end();
        }
    }

    public function actionSetFields()
    {
        $defectId = $_POST['id'];
        $defects = Defect::model()->with('sstate0')->findByPk($defectId);


        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($defects);
        Yii::app()->end();


    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}