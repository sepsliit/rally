<?php

class MailController extends Controller
{
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view',),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('sendmail'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete','sendmail'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
	public function actionIndex()
	{
		$this->render('index');
	}
    public function actionSendMail()
    {
        $message            = new YiiMailMessage;
        //this points to the file test.php inside the view path
        $to = $_POST['to'];
        $subject = $_POST['subject'];
        $content = $_POST['content'];
        $message->view = "test";
//        $sid                 = 1;
//        $criteria            = new CDbCriteria();
//        $criteria->condition = "studentID=".$sid."";
//        $studModel1          = Student::model()->findByPk($sid);
        $params              = array('myMail'=>$content);

        $message->subject    = $subject;
        $message->setBody($params, 'text/html');
        $message->addTo($to);
        $message->from = 'nanayakkaraoffice@gmail.com';
        Yii::app()->mail->send($message);

        header('Content-type: application/json');
        echo CJavaScript::jsonEncode('success');
        Yii::app()->end();
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}