<?php

class WorkspaceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','addUserStory','saveStory','editStory','userstorydelete' ,'setFields','myTask','dView' ,'lView','track','excel','usImport','changeState','delete'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','changename'
                ,'addproject','delete','change','calender'
                ,'calenderdrop','getevent','eventdrop'
                ,'eventdelete','release','empty','updateui','nameedit','getiteration','sms','getnode',
                'create_node'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionSms(){

        Yii::app()->skebbySms->send(array('to'=>'+94778530274','message'=>'hello world'));

    }

    public function actionGetnode(){

        $workspaceid = Yii::app()->user->getState("logId");
        $userlog = Yii::app()->user->getState("userlog");
        $projects=array();

        $workspaces=LoginWorkspace::model()->findAllByAttributes(array('loginid'=>$userlog));

        foreach($workspaces as $row){
            $project=array();
            $workspace=Workspace::model()->findByPk($row->workspaceid);
            if($userlog==$workspace->ownerid){
                $project['id']= $workspace->id;
                $project['text']= $workspace->name;
                $project['type']= 'owner';
            }else{
                $project['id']= $workspace->id;
                $project['text']= $workspace->name;
                $project['type']= 'assign';
            }

            $projects[]=$project;
        }

        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($projects);
        Yii::app()->end();


    }

    public function actionCreate_node(){
        $userlog = Yii::app()->user->getState("userlog");
        $name = $_GET['text'];
        $wid=$_GET['id'];
        $ids = array();

        if($wid!=0){
            $id = array();

            $workspace = Workspace::model()->findByPk($wid);
            $workspace->name=$name;
            $id['id'] = $workspace->id;

            if($workspace->save()){
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode($id);
                Yii::app()->end();
            }

        }else{

            $id = array();
            $workspace = new Workspace;
            $workspace->name=$name;
            $workspace->ownerid=$userlog;
            if($workspace->save()){
                $worklog = new LoginWorkspace;
                $worklog->loginid=$userlog;
                $worklog->workspaceid=$workspace->id;
                $id['id'] = $workspace->id;

                if($worklog->save()){
                    header('Content-type: application/json');
                    echo CJavaScript::jsonEncode($id);
                    Yii::app()->end();
                }
            }

        }



    }

    public function actionGetiteration(){
        $id = $_POST['id'];

        $iteration = UserStories::model()->findAllByAttributes(array('iterationid'=>$id));

        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($iteration);
        Yii::app()->end();

    }
    public function actionNameedit(){

        $id=$_POST['id'];
        $content =$_POST['des'];

        $release = Release::model()->findByPk($id);
        $release->name=$content;
        $release->save();

    }
    public function actionUpdateui(){

        $drop = array();

        $drop = $_POST['drop'];

        foreach($drop as $val){

            $iteration = Iteration::model()->findByPk($val['id']);

            $iteration->relid = $val['main'];
            $iteration->save();

        }

    }
    public function actionEmpty(){

        $drop = array();

        $drop = $_POST['drop'];

        foreach($drop as $val){

            if($val['id']!=0){
                $iteration = Iteration::model()->findByPk($val['id']);

                $iteration->relid = null;


            }


        }
        if($iteration->save()){




            header('Content-type: application/json');
            echo CJavaScript::jsonEncode('success');
            Yii::app()->end();
        }



    }
    public function actionRelease(){
        $logid = Yii::app()->user->getState("logId");

        $iteration = Iteration::model()->findAllByAttributes(array('login_workspaceid'=>$logid));
        $releases = Release::model()->findAllByAttributes(array('wid'=>$logid));
        $this->render('release',array('iteration'=>$iteration,'release'=>$releases));
    }
    public function actionEventdelete(){
        $id = $_POST['id'];


        $release = Release::model()->findByPk($id);

        if($release->delete()){
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode('successful');
            Yii::app()->end();
        }


    }
    public function actionEventdrop(){
        $name = $_POST['title'];
        $sdate = $_POST['sdate'];
        $edate = $_POST['edate'];
        $id = $_POST['id'];
        $logid = Yii::app()->user->getState("logId");
        $color = $_POST['color'];

        $release = Release::model()->findByPk( $id);

        $release->name = $name;
        $release->sdate = $sdate;
        $release->edate = $edate;
        $release->color =$color;
        $release->wid =$logid;

        if($release->save()){
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode($release);
            Yii::app()->end();
        }


    }
    public function actionCalenderDrop(){
        $name = $_POST['title'];
        $date = $_POST['date'];
        $logid = Yii::app()->user->getState("logId");
        $color = $_POST['color'];

        $release = new Release;

        $release->name = $name;
        $release->sdate = $date;
        $release->edate = $date;
        $release->color =$color;
        $release->wid =$logid;

        if($release->save()){
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode($release);
            Yii::app()->end();
        }


    }

    public function actionCalender(){
        $logid = Yii::app()->user->getState("logId");

        $release = Release::model()->findAllByAttributes(array('wid'=>$logid));

        $this->render('workcalander',array('release'=>$release));


    }
    public function actionGetEvent(){
        $logid = Yii::app()->user->getState("logId");

        $release = Release::model()->findAllByAttributes(array('wid'=>$logid));

        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($release);
        Yii::app()->end();



    }
    public function actionChange(){

        $id = $_POST['id'];

        $name= Workspace::model()->findByPk($id);
        Yii::app()->user->setState("project_n", $name->name);
        Yii::app()->user->setState("logId", $id);

        header('Content-type: application/json');
        echo CJavaScript::jsonEncode("success");
        Yii::app()->end();

    }

    public function actionAddproject(){

        $name = $_POST['name'];
        $description = $_POST['des'];
        $uid = $_POST['uid'];

        $model = new Workspace;
        $loginwork = new LoginWorkspace;

        $login = Login::model()->findByAttributes(array('userid'=>$uid));
        $model->name = $name;
        $model->des = $description;
        $model->ownerid = $uid;

        if($model->save()){

            $loginwork->loginid =$login->id;
            $loginwork->workspaceid =$model->id;
            if($loginwork->save()){
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode($model);
            Yii::app()->end();

        }
        }


    }
//change name------------------------------------------------

    public function actionChangename(){
       $filter = $_POST['filter'];
        $id = $_POST['id'];
        if($filter=='name'){

        $name = $_POST['name'];

        $model = Workspace::model()->findByPk($id);

        $model->name = $name;

        if($model->save()){
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode("success");
            Yii::app()->end();

        }
        }else if($filter=='des'){
            $description = $_POST['des'];

            $model = Workspace::model()->findByPk($id);

            $model->des = $description;

            if($model->save()){
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode("success");
                Yii::app()->end();

            }



        }

    }

// end of change name----------------------------------------
    //-----------------userstories-------------------
    public  function actionAddUserStory()
    {

        $id = Yii::app()->user->getState("userId");
        if (isset($_GET['id'])) {

            $id1 = $_GET['id'];

            $noitfi = NotificationUpdate::model()->find(array(
                'condition' => 'notificationid = :nid AND userid = :userid',
                'params' => array(':nid' => $id1, ':userid' => $id)
            ));

            if($noitfi != null)
            {
                $noitfi->lastviewed = new CDbExpression('CURRENT_TIMESTAMP');
                $noitfi->save(false);
            }
            else {
                $notification = new NotificationUpdate();
                $notification->lastviewed = new CDbExpression('CURRENT_TIMESTAMP');
                $notification->notificationid =$id1;
                $notification->userid = $id;
                $notification->save(false);
            }


        }


        $wid = Yii::app()->user->getState("logId");
        $iteration=Iteration::model()->findAllByAttributes(array('login_workspaceid'=>$wid));
        $userstories=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$wid));
        $users=LoginWorkspace::model()->findAllByAttributes(array('workspaceid'=>$wid));
        $this->render('index',array(
            'userstories'=>$userstories,
            'iteration'=>$iteration,
            'users'=>$users,

        ));
    }


    public  function actionSaveStory()
    {

        $id = Yii::app()->user->getState("userId");
        $wid = Yii::app()->user->getState("logId");
        $name= $_POST['name'];
        $des= $_POST['description'];
        $owner= $_POST['owner'];
//        $block= $_POST['reason'];
        $hrs= $_POST['estimate'];
        $iterationid=$_POST['iterationid'];

        $model=new UserStories;
        $model->uname= $name;
        $model->owner=$owner;
        $model->dis=$des;
        $model->est=$hrs;
        $model->state='Defined';
//        $model->breason=$block;
        $model->iterationid=$iterationid;
        $model->login_workspaceid=$wid;
         //$model->iterationid=$iterationModel->id;


        if(!empty($name)  && !empty($des) && !empty($owner))
        {
            $iterationName="";
            if($model->save()){
                if($model->iterationid==null)
                {
                    $iterationName='Unsheduled';
                }
                else{
                    $iterationName=$model->iteration->name;
                }
                $details=array(
                    'id'=>$model->id,
                    'uname'=>$model->uname,
                    'dis'=>$model->dis,
                    'owner'=>$model->owner,
                    'state'=>$model->state,
                    'stage'=>$iterationName,
                    'est'=>$model->est);

                $notification = new NotificationTable();
                $notification->notifi_type = 1;
                $notification->img = 'imgurl here';
                $notification->headertext = 'User story been added';
                $notification->subtext = $name .': New user story been added please refer the story page';
                $notification->subj_id =$details["id"];
                $notification->added_date = new CDbExpression('CURRENT_TIMESTAMP');
                $notification->wid=$wid;
                $notification->save();

                header('Content-type: application/json');
                echo CJavaScript::jsonEncode($details);
                Yii::app()->end();

            }
            else
            {
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode('error');

            }



        }
        else
        {
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode('error');

        }

    }




    public  function  actionEditStory()
    {


        $id= $_POST['id'];
        $name= $_POST['name'];
        $des= $_POST['description'];
        $owner= $_POST['owner'];
        $state= $_POST['state'];
        $iteration= $_POST['iteration'];
        $block= $_POST['reason'];
        $hrs= $_POST['estimate'];


        $model=UserStories::model()->findByPk($id);

        $model->uname= $name;
        $model->owner=$owner;
        $model->dis=$des;
        $model->est=$hrs;
        $model->iterationid=$iteration;
        $model->state=$state;
        $model->breason=$block;

        if(!empty($name)  && !empty($des) && !empty($owner))
        {
            $iterationName="";
            if($model->iterationid==null)
            {
                $iterationName='Unsheduled';
            }
            else{
                $iterationName=$model->iteration->name;
            }

            if($model->save()){

                $details=array('id'=>$model->id,'uname'=>$model->uname,'dis'=>$model->dis,'owner'=>$model->owner,'state'=>$model->state,'stage'=>$iterationName,'est'=>$model->est);

                header('Content-type: application/json');
                echo CJavaScript::jsonEncode($details);
                Yii::app()->end();

            }
            else
            {

                header('Content-type: application/json');
                echo CJavaScript::jsonEncode('error1');
                Yii::app()->end();

            }

        }
        else
        {
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode('error');
           Yii::app()->end();
        }
    }




    public  function  actionUserstorydelete()
    {

        $id = $_POST['id'];
        $model=UserStories::model()->findByPk($id);
        if($model->delete()){


            header('Content-type: application/json');
            echo CJavaScript::jsonEncode("success");
            Yii::app()->end();


        }

    }


    public function actionSetFields()
    {
        $usid=$_POST['id'];
        $userstories=UserStories::model()->findByPk($usid);


        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($userstories);
        Yii::app()->end();


    }

    public function actionMyTask()
    {
        $id=Yii::app()->user->getState('logId');
        $user=Yii::app()->user->getState('username');

        $iterations=Iteration::model()->findAllByAttributes(array('login_workspaceid'=>$id));
        $tasks=Task::model()->findAllByAttributes(array('owner'=>$user));

        $this->render('asignedToMe',array(
            'iteration'=>$iterations,
            'task'=>$tasks,
        ));

    }

    //-------------end of userstories----------------



    /*track iteration starts here*/


    public function actionDView()
    {
        $stage=null;
        $id=Yii::app()->user->getState('logId');
        $stage =(Yii::app()->getRequest()->getParam('stage'));
        // $connection=Yii::app()->db;   // assuming you have configured a "db" connection

        if($stage==null)
        {
            $stage='';
        }
// If not, you may explicitly create a connection:
// $connection=new CDbConnection($dsn,$username,$password);

        $defined=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$id,'iterationid'=>$stage,'state'=>'Defined'));
        $inprogress=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$id,'iterationid'=>$stage,'state'=>'In progress'));
        $completed=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$id,'iterationid'=>$stage,'state'=>'Completed'));
        $accepted=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$id,'iterationid'=>$stage,'state'=>'Accepted'));

        $this->render('dView',array(

            'defined'=>$defined,
            'inprogress'=>$inprogress,
            'completed'=>$completed,
            'accepted'=>$accepted,

        ));
        //$this->redirect('workspace/dView&stage=1st+iteration' );
    }


    public function actionLview()
    {
        $stage=null;
        $id=Yii::app()->user->getState('logId');

        $stage = Yii::app()->getRequest()->getParam('stage');
        if($stage==null)
        {
            $stage='';
        }

        $userstories=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$id,'iterationid'=>$stage));

        $this->render('lView',array(

            'userstories'=> $userstories));
    }



    public  function actionTrack()
    {
        $id=Yii::app()->user->getState('logId');
        $iteration=Iteration::model()->findAllByAttributes(array('login_workspaceid'=>$id));

        $this->render('track',array(
            'iteration'=>$iteration,
        ));
    }


    public function actionExcel()
    {
        $stage=null;
        //  $id=Yii::app()->user->getState('logId');
        $stage = Yii::app()->getRequest()->getParam('stage');
        // $connection=Yii::app()->db;   // assuming you have configured a "db" connection

//        if($stage==null)
//        {
//            $stage='';
//        }
        Yii::import('ext.phpexcel.XPHPExcel');
        $objPHPExcel= XPHPExcel::createPHPExcel();
// $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet();
//$num=$objPHPExcel->setHighestRow()->$numClients;

// $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&HUser stories!');
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'User Story Name')
            ->setCellValue('C1', 'Description')
            ->setCellValue('D1', 'Owner')
            ->setCellValue('E1', 'State')
            ->setCellValue('F1', 'Est(Hrs)')
            ->setCellValue('G1', 'Iteration');

        //  $connection=Yii::app()->db;
//mysql_select_db('newrally');


        // assuming you have configured a "db" connection
        $id=Yii::app()->user->getState('logId');



        // $connection=Yii::app()->db;   // assuming you have configured a "db" connection
// If not, you may explicitly create a connection:
        $db_host = "localhost";
        $db_name = "newrally";
        $db_user = "root";
        $db_pass = "";
        $db = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query="select id, uname ,dis, owner,state, est,iterationid from user_stories where  login_workspaceid='".$id."'and iterationid='".$stage."' ";

//        $result1=UserStories::model()->findAllByAttributes(array('login_workspaceid'=>$id,'iterationid'=>$stage));
//        $result1->iteration->name;

        $result = $db->query($query);
        if($result === FALSE) {
            die(mysql_error()); // TODO: better error handling
        }

        $row = 2; // 1-based index
        while($row_data = $result->fetch(PDO::FETCH_ASSOC)) {
            $col = 0;
            foreach($row_data as $value) {
                if($col==6){
                    $iteration = Iteration::model()->findByPk($value);
                    $value = $iteration->name;
                }
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value);
                $col++;
            }
            $row++;
        }





//FILL With DATA


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="01simple.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        Yii::app()->end();

    }



    public function actionUsImport()
    {

        function generateRandomString($length = 5) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $randomString;
        }






//        $username=Yii::app()->user->getState('username');
//        $myid=Yii::app()->user->getState('myid');
        $wid=Yii::app()->user->getState('logId');

        $Members=LoginWorkspace::model()->findAllByAttributes(array('workspaceid'=>$wid));


        //  $dt= Team::with('Login')->find('uid')->username;
//        $team=Team::model()->ByAttributes(array('uid'=>$id)findAll);
//        $admin=Login::model()->findAllByAttributes(array('userid'=>$id));


        //spl_autoload_unregister(array('YiiBase','autoload'));
        $error = "";
        $msg = "";
        $errDisplay="";

        $ready=false;
        //   $member=false;

        $fileElementName = 'us';
        if(!empty($_FILES[$fileElementName]['error']))
        {
            switch($_FILES[$fileElementName]['error'])
            {

                case '1':
                    $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2':
                    $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3':
                    $error = 'The uploaded file was only partially uploaded';
                    break;
                case '4':
                    $error = 'No file was uploaded.';
                    break;

                case '6':
                    $error = 'Missing a temporary folder';
                    break;
                case '7':
                    $error = 'Failed to write file to disk';
                    break;
                case '8':
                    $error = 'File upload stopped by extension';
                    break;
                case '999':
                default:
                    $error = 'No error code avaiable';
            }
        }elseif(empty($_FILES['us']['tmp_name']) || $_FILES['us']['tmp_name'] == 'none')
        {
            $error = 'No file was uploaded..';
        }else
        {



            //$msg .= " File Name: " . $_FILES['us']['name'] . ", ";
//            $msg .= " File Size: " . @filesize($_FILES['us']['tmp_name']);
            Yii::import('ext.phpexcel.XPHPExcel');
            $phpExcel = XPHPExcel::createPHPExcel();
            $inputFileName=$_FILES['us']['name'];
            $info = pathinfo($_FILES['us']['name']);
            $ext = $info['extension']; // get the extension of the file
            $new=generateRandomString().".";
            $newname = $new.$ext;


            $target = 'C:/Users/Samsung/Desktop/images/'.$newname;

            //exec("mkdir -w " . Yii::app()->basePath ."/uploads");
            // $target=Yii::app()->request->baseUrl.'/upload/'.$newname;
            // move_uploaded_file( $_FILES['us']['tmp_name'], $target);

            if($ext=='csv')
            {
                if( move_uploaded_file( $_FILES['us']['tmp_name'], $target))
                {
                    $logid = Yii::app()->user->getState("logId");
                    Yii::import('ext.phpexcel.XPHPExcel');
                    $phpExcel = XPHPExcel::createPHPExcel();
                    $inputFileName=$newname;

                    $objPHPExcel1 = PHPExcel_IOFactory::load($target);

                    $objPHPExcel=$objPHPExcel1->getActiveSheet();


                    $highestRow = $objPHPExcel->getHighestRow(); // e.g. 10
                    $highestColumn = $objPHPExcel->getHighestColumn(); // e.g 'F'
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
                    //  $msg .= " File Name: " .$ext. "";
                    //  @unlink($_FILES['us']);

                    /*checking conditions*/

                    for ($row = 2; $row <= $highestRow; $row++){
                        //  Read a row of data into an array
                        $rowData = $objPHPExcel->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                            NULL,
                            TRUE,
                            FALSE);

                        //  Insert row data array into your database of choice herepr

                        $owner=$rowData[0][2];



                        //  foreach($team as $team_user):
                        foreach($Members as $result):
                            $teamMembers=$result->login->username;
                            if($owner==$teamMembers)
                            {
                                $member=true;
                                break;
                            }
                            else
                            {

                                //  $error="We are sorry but there was a problem with your import.The file contains the following error(s) at line".$row." Owner could not be found (".$owner.").";
                                $member=false;
                                // $ready=false;
                                break;


                            }
                        endforeach;

                        if($member==false)
                        {
                            $error="We are sorry but there was a problem with your import.The file contains the following error(s) at line".($row)." Owner could not be found (".$rowData[0][2].").";

                            break;
                        }

                        // endforeach;

//                        if($member==false)
//                        {
////                            e contains the following error(s) at line".($row)." Owner could not be found (".$rowData[0][2].").";
//
//                            break;
//                        }



                        if($member==true){
                            for ($row = 2; $row <= $highestRow; $row++){
                                //  Read a row of data into an array
                                $rowData = $objPHPExcel->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);

                                //  Insert row data array into your database of choice herepr
                                $uname=$rowData[0][0];
                                $udes=$rowData[0][1];
                                $owner=$rowData[0][2];
                                $state=$rowData[0][3];
                                $est=$rowData[0][4];
                                $block=$rowData[0][5];

                                if($member==true)
                                {
                                    if(is_numeric($est))
                                    {

                                        if($block=='FALSE' ||$block=='TRUE')
                                        {




                                            if($state=='Defined' || $state=='In progress' || $state=='Completed' || $state=='Accepted')
                                            {
//
                                                $ready=true;




                                            }else
                                            {
                                                $error="We are sorry but there was a problem with your import.The file contains the following error(s) at line".$row." State field does not match (".$state.").";
                                                $ready=false;
                                                $member=false;
                                                // break;

                                            }

                                        }else
                                        {
                                            $error="We are sorry but there was a problem with your import.The file contains the following error(s) at line".$row." Blocked should be either TRUE or FALSE";
                                            $ready=false;
                                            $member=false;
                                            // break;
                                        }
                                    }else
                                    {
                                        $error="We are sorry but there was a problem with your import.The file contains the following error(s) at line".$row." Plan Estimate should be a numeric value";
                                        $ready=false;
                                        $member=false;
                                        // break;
                                    }
                                }

                            }
                        }

                        //insert values to the db

                        if($ready==true)
                        {
                            for ($row = 2; $row <= $highestRow; $row++){
                                //  Read a row of data into an array
                                $rowData = $objPHPExcel->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);

                                //  Insert row data array into your database of choice herepr
                                $uname=$rowData[0][0];
                                $udes=$rowData[0][1];
                                $owner=$rowData[0][2];
                                $state=$rowData[0][3];
                                $est=$rowData[0][4];
                                $block=$rowData[0][5];
                                $model =new UserStories;


                                if($block=='FALSE')
                                {
                                    $block="";
                                }

                                $model->uname= $uname;
                                $model->owner=$owner;
                                $model->dis=$udes;
                                $model->est=$est;
                                $model->iterationid=$_POST['stage'];
                                $model->state=$state;
                                $model->breason=$block;
                                // $model->loginid="01";
                                $model->login_workspaceid=$logid;


                                if($model->save())
                                {
                                    // $response="Successfully Inserted";
                                    $msg=$_POST['stage'];

                                }


                            }

                        }


                    }





                }

            }else
            {
                $msg .= " Please select a .csv file";

            }



        }



        echo "{";
        echo				"error: '" . $error . "',\n";
        echo				"msg: '" . $msg . "'\n";
        echo "}";

    }


    public  function actionChangeState()
    {
        $state=$_POST['uState'];
        $uid=$_POST['uID'];


        $model=UserStories::model()->findByPk($uid);
        $model->state=$state;
        $model->save();



    }



    /*track iteration ends here*/
    
    
    
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Workspace;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Workspace']))
		{
			$model->attributes=$_POST['Workspace'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Workspace']))
		{
			$model->attributes=$_POST['Workspace'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
        $id = $_GET['id'];
//		$this->loadModel($id)->delete();

        $workspace = Workspace::model()->findByPk($id);

        $workspace->delete();
        header('Content-type: application/json');
        echo CJavaScript::jsonEncode('success');
        Yii::app()->end();


	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Workspace');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Workspace('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Workspace']))
			$model->attributes=$_GET['Workspace'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Workspace the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Workspace::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Workspace $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='workspace-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
