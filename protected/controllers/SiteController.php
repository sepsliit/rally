<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */

    public function actionRegister(){

       // if (Yii::app()->user->isGuest) {
            //$this->redirect(array('user/register'));
//        } else {
//            $this->render('index');
//        }

    }
    public function actionTimeline(){
        
    }

    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        if (Yii::app()->user->isGuest) {
            $this->redirect(array('site/login'));
        } else {

            $workspaceArray = array();
            $wid = Yii::app()->user->getState("userlog");
            $id=Yii::app()->user->getState('logId');

            $name= Workspace::model()->findByPk($id);
            Yii::app()->user->setState("project_n", $name->name);


          if(Yii::app()->user->getState("usertype") == "team"){
//                $uname = Yii::app()->user->getState("username");
//              $team = Team::model()->findAllByAttributes(array('email'=>$uname));
//              $workspace = new WorkAttr;
//              foreach($team as $row){
//                  $logwork = LoginWorkspace::model()->findAllByAttributes(array('workspaceid'=>$row->wid));
//                  foreach($logwork as $row1){
//                     $workspace->id = $row1->id;
//                     $workspace->loginid = $row1->loginid;
//                     $workspace->workspaceid = $row1->workspaceid;
//
//                  }
////
//              }
              $teamid=Yii::app()->user->getState('userId');
               $workspace = LoginWorkspace::model()->findAllByAttributes(array('teamid'=>$teamid));

          }else{
              $workspace = LoginWorkspace::model()->findAllByAttributes(array('loginid'=>$wid));
          }

            $this->render('index',array('workspace'=>$workspace,'name'=>$name));
        }
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{


		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
        $this->layout = '//layouts/blank';
		$model=new LoginForm;

        if (Yii::app()->user->isGuest) {
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));

        }else{
            $this->redirect(array('site/index'));
        }
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    public function actionAouthLogin()
    {

        /**
         * @var apiPlusService $service
         */
        $plus = Yii::app()->GoogleApis->serviceFactory('Plus');;
        /**
         * @var apiClient $client
         */
        $client = Yii::app()->GoogleApis->client;
        try {
            if (!isset(Yii::app()->session['auth_token'])
                || is_null(Yii::app()->session['auth_token'])
            ) {
                // You want to use a persistence layer like the DB for storing this along
                // with the current user
                Yii::app()->session['auth_token'] = $client->authenticate();
            } else {
                $activities = '';
            }
            $client->setAccessToken(Yii::app()->session['auth_token']);
            $oauthUser = $plus->people->get('me');//get profile details

            $emails = $oauthUser['emails'];//get emails to array
            $account_email = null;
            foreach($emails as $email)
            {
                if($email['type'] == 'account')
                {
                    $account_email = $email['value'];
                }
            }

            $login = Login::model()->findAll(array(
                'condition' => 'username = :email',
                'group' => 'username',
                'params' => array(':email' => $account_email)
            ));

            if(count($login) > 0)
            {
                $login = $login[0];
                $loginForm = new LoginForm();
                $loginForm->rememberMe = true;
                $loginForm->username = $login->attributes['username'];
                $loginForm->utype = $login->attributes['utype'];
                $loginForm->password = $login->attributes['password'];

                if ($loginForm->validate() && $loginForm->login())
                {
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }
            else {
                $userinfo = new stdClass();
                $userinfo->displayname = $oauthUser['displayName'];
                $userinfo->email = $account_email;
                Yii::app()->user->setState('userinfo', $userinfo);

                $this->redirect(array('user/register'));
            }

        } catch (Exception $e) {
            // This needs some love as not every exception means that the token
            // was invalidated
            Yii::app()->session['auth_token'] = null;
            throw $e;
        }

    }
}