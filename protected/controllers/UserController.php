<?php

class UserController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('register', 'regSave'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'emails', 'regTeam', 'delete_user', 'index', 'view', 'regSaveteam'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionRegSaveteam()
    {
        $fname = $_POST['fname'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $dept = $_POST['dept'];
        $office = $_POST['office'];
        $pass = $_POST['pass'];
        $title = $_POST['title'];
        $userid = $_POST['userid'];
        $wkid = $_POST['wkid'];

        $model = new User;
        $modelLog = new Login;
        $loginworkspace = new LoginWorkspace;

        $model->deptt = $dept;
        $model->disn_name = $fname;
        $model->officeLoc = $office;
        $model->phone = $phone;
        $model->role = 'admin';
        $model->img = $title;

        if ($model->save()) {
            $modelLog->username = $email;
            $modelLog->password = $pass;
            $modelLog->perm = "full";
            $modelLog->utype = 'user';
            $modelLog->userid = $model->id;
            if ($modelLog->save()) {

                $loginworkspace->loginid = $modelLog->id;
                $loginworkspace->workspaceid = $wkid;
                if ($loginworkspace->save()) {
                    header('Content-type: application/json');
                    echo CJavaScript::jsonEncode("success");
                    Yii::app()->end();
                }
            }
        }
    }

    public function actionEmails()
    {
        $mail = array();
        $team = Login::model()->findAll();

        foreach ($team as $row) {
            $mail[] = $row->username;
        }

        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($mail);
        Yii::app()->end();

    }

    public function actionDelete_user()
    {
        $id = $_POST['id'];
        $model = Team::model()->findByPk($id);
        if ($model->delete()) {
            header('Content-type: application/json');
            echo CJavaScript::jsonEncode("success");
            Yii::app()->end();
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
//save register information
    public function actionRegSave()
    {

        $fname = $_POST['fname'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $dept = $_POST['dept'];
        $office = $_POST['office'];
        $pass = $_POST['pass'];
        $title = $_POST['title'];

        $model = new User;
        $modelLog = new Login;
        $workspace = new Workspace;
        $loginworkspace = new LoginWorkspace;

        $model->deptt = $dept;
        $model->disn_name = $fname;
        $model->officeLoc = $office;
        $model->phone = $phone;
        $model->role = 'admin';
        $model->img = $title;

        if ($model->save()) {

            $modelLog->username = $email;
            $modelLog->password = $pass;
            $modelLog->perm = "full";
            $modelLog->utype = 'user';
            $modelLog->userid = $model->id;

            try {
                if ($modelLog->save()) {
                    $workspace->name = "Default";
                    $workspace->des = "Default";
                    $workspace->ownerid = $modelLog->id;

                    if ($workspace->save()) {
                        $loginworkspace->loginid = $modelLog->id;
                        $loginworkspace->workspaceid = $workspace->id;

                        if ($loginworkspace->save()) {
                            header('Content-type: application/json');
                            echo CJavaScript::jsonEncode("success");
                            Yii::app()->end();
                        }
                    }
                }
            } catch (Exception $e) {
                header('Content-type: application/json');
                echo CJavaScript::jsonEncode("email");
                Yii::app()->end();
            }
        }
    }


    public function actionRegTeam()
    {
        $id = Yii::app()->user->getState("userId");
        $wid = Yii::app()->user->getState("logId");
        $user = User::model()->findByPk($id);
        $workspace = Workspace::model()->findByPk($wid);
        $email = $_POST['email'];
        $message = new YiiMailMessage;

        $login = Login::model()->findByAttributes(array('username' => $email));

        if ($login != "") {
            $url = "http://localhost/seprally/index.php?r=site/login";
            $to = $login->username;
            $subject = 'Notification Of Rally Developers';
            $content = 'You are assented to project of' . $workspace->name . ' by ' . $user->disn_name;
            $message->view = "notify";

            $params = array('myMail' => $content, 'url' => $url);
            $message->subject = $subject;
            $message->setBody($params, 'text/html');
            $message->addTo($to);
            $message->from = 'nanayakkaraoffice@gmail.com';
            Yii::app()->mail->send($message);

            header('Content-type: application/json');
            echo CJavaScript::jsonEncode('Successfully send the notification');
            Yii::app()->end();

        } else {

            $url = "http://localhost/seprally/index.php?r=team/register&id=$id&wid=$wid&email=$email";


            //this points to the file test.php inside the view path
            $to = $email;
            $subject = 'Notification Of Rally Developers';

            $message->view = "test";

            $params = array('myMail' => $url);
            $message->subject = $subject;
            $message->setBody($params, 'text/html');
            $message->addTo($to);
            $message->from = 'nanayakkaraoffice@gmail.com';
            Yii::app()->mail->send($message);

            header('Content-type: application/json');
            echo CJavaScript::jsonEncode('Successfully sent Registration Link');
            Yii::app()->end();
        }
    }


//render register
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionRegister()
    {
        $this->layout = '//layouts/blank';
        $this->render('register');
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new User;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate()
    {


        $utype = "a";
        $id = $_POST['id'];
        $uname = $_POST['uname'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $dept = $_POST['dept'];
        $office = $_POST['office'];
        $role = $_POST['role'];
        $title = $_POST['title'];
        if ($role == 'admin') {
            $utype = 'user';
        }


        if ($role == 'admin') {
            $user = User::model()->findByPk($id);
            $login = Login::model()->findByAttributes(array('userid' => $id));

            $user->disn_name = $uname;
            $login->username = $email;
            $user->deptt = $dept;
            $user->officeLoc = $office;
            $user->img = $title;
            $user->phone = $phone;
            //$user->phone = $phone;
            if ($user->save()) {

                if ($login->save()) {
                    $admin = new UserDTO();

                    $admin->email = $email;
                    $admin->deptt = $dept;
                    $admin->dis_name = $uname;
                    $admin->id = $id;
                    $admin->office = $office;
                    $admin->role = $role;
                    $admin->utype = $utype;
                    $admin->title = $title;
                    $admin->phone = $phone;

                    header('Content-type: application/json');
                    echo CJavaScript::jsonEncode($admin);
                    Yii::app()->end();


                } else {
                    echo('error1');
                }

            } else {
                echo('error2');
            }


        } else {
            $team = Team::model()->findByPk($id);

            $team->disn_name = $uname;
            $team->email = $email;
            $team->deptt = $dept;
            $team->officeLoc = $office;
            $team->phone = $phone;
            $team->img = $title;

            if ($team->save()) {

                header('Content-type: application/json');
                echo CJavaScript::jsonEncode($team);
                Yii::app()->end();

            }


        }


    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $id = Yii::app()->user->getState("userId");
        $wid = Yii::app()->user->getState("logId");

        if (isset($_GET['notifi'])) {

            $id1 = $_GET['notifi'];

            $noitfi = NotificationUpdate::model()->find(array(
                'condition' => 'notificationid = :nid AND userid = :userid',
                'params' => array(':nid' => $id1, ':userid' => $id)
            ));

            if ($noitfi != null) {
                $noitfi->lastviewed = new CDbExpression('CURRENT_TIMESTAMP');
                $noitfi->save(false);
            } else {
                $notification = new NotificationUpdate();
                $notification->lastviewed = new CDbExpression('CURRENT_TIMESTAMP');
                $notification->notificationid = $id1;
                $notification->userid = $id;
                $notification->save(false);
            }


        }

        $users = LoginWorkspace::model()->findAllByAttributes(array('workspaceid' => $wid));
        $userDetails = array();


        foreach ($users as $row) {
            $user = array();
            $login = Login::model()->findByPk($row->loginid);
            $userTb = User::model()->findByPk($login->userid);
            $user['email'] = $login->username;
            $user['deptt'] = $userTb->deptt;
            $user['name'] = $userTb->disn_name;
            $user['loginId'] = $login->id;
            $user['title'] = $userTb->img;
            $user['phone'] = $userTb->phone;
            $user['office'] = $userTb->officeLoc;

            $userDetails[] = $user;
        }


        $ownerid = Workspace::model()->findByPk($wid);

        $this->render('index', array(
            'userdetails' => $userDetails, 'ownerid' => $ownerid->ownerid));


//        $this->render('index');//,array(
////			'dataProvider'=>$dataProvider,'login'=>$login
////		));

    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new User('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
