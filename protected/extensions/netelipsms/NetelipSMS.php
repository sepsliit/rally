<?php

/**
 * Extension to send SMSs using www.netelip.com accounts
 * Based on Pavel Voronin code <http://www.yiiframework.com/user/3935> for yii-littlesms
 *
 * @author aleksdj <aleksdj@gmail.com>
 * @license MIT
 * @version 1.0
 */

/**
 * How to use
 *
 * You at least need PHP 5.3+ adn cURL.
 *
 * Change your config/main.php to add a new component:
 *
 * 'components' => array
 *(
 *     'sms' => array
 *    (
 *         'class'    => 'application.extensions.netelipsms.NetelipSMS',
 *         'login'     => 'user to login', // User to web login 
 *         'password'   => 'password to login',     // password to web login
 *         'testMode' => true            // If you gonna only test
 *     )
 * )
 *
 * Now you can use the extension like this:
 *
 * Yii::app()->sms->messageSend( array( '+34600000000', '+01555000000' ), 'Hello!' );
 *
 */
class NetelipSMS extends CApplicationComponent
{
	const REQUEST_SUCCESS = true;
	const REQUEST_ERROR = false;

	const RESPONSE_CODE_SUCCESS = 200;
	const RESPONSE_CODE_UNAUTHORIZED = 401;
	const RESPONSE_CODE_PAYMENT_REQUIRED = 402;
	const RESPONSE_CODE_PRECONDITION_FAILED = 412;
	const RESPONSE_CODE_PARAMETERS_ERROR = 103;
	const RESPONSE_CODE_REQUIRED_PARAMETERS_ERROR = 109;

	/**
	 * User to login
	 * @var string
	 */
	public $login;

	/**
	 * User Password
	 * @var string
	 */
	public $password;

	/**
	 * To test.
	 * @var boolean
	 */
	public $testMode = false;

	/**
	 * If you want to use SSL(HTTPS)
	 * @var boolean
	 */
	public $useSSL = false; //not working by now

	/**
	 * Api URL
	 * Api Url to call without 'http'
	 * @var string
	 */
	public $url = 'sms.netelip.com/api.php';

	/**
	 * Server Response
	 * @var array
	 */
	protected $response;

	/**
	 * Server Response CODE
	 * @var array
	 */
	protected $responseCode;

	/**
	 * Initialization
	 *
	 */
	public function init( )
	{
		if( ! function_exists( 'curl_init' ) )
			throw new CException( 'You must to install cURL' );

		parent::init( );

		Yii::trace( 'Extension initializating', 'NetelipSMS' );
	}

	/**
	 * List of API calls
	 *
	 * Format:
	 *
	 * array
	 *(
	 *     ...
	 *     'component/function' => array
	 *    (
	 *         'required' => array( 'paramName1', 'paramName2' ), // ​​Required parameters
	 *         // 'required' => 'paramName1',                      // Can not be an array
	 *         'optional' => array( 'paramName3', 'paramName4' ), // ​​Optional Parameters
	 *         'fixed' => array( 'paramName4' => 'value4' ),      // Fixed parameters
	 *         'returnKey' => 'keyName' // Object key response, the value of which is returned by default
	 *         'returnType' => 'float' //  type , which provides a return value
	 *     )
	 *     ...
	 * )
	 *
	 * @return array
	 */
	protected function calls( )
	{
		return array
		(
			'message/send' => array( 'returnKey' => 'status_code', 'required' => array('destination', 'message' ), 'optional' => array( 'from' ), 'fixed' => array( 'test' => $this->testMode ) ),
		);
	}

	/**
	 * Process API calls
	 *
	 * Basic usage component
	 *
	 * Yii::app()->sms->messageSend(
	 *     array
	 *    (
	 *         // Use always International code numbers
	 *         'destination' => array( '+346000000', '+34030000000' ),
	 *         // You can use comma separator
	 *         // 'destination' => '+3460000000,+34030000000',
	 *         'message' => 'Hello, World!'
	 *     )
	 * );
	 *
	 * You can use optional keys like this
	 *
	 * Yii::app()->sms->messageSend( '+7(926)000-00-00, 8-903-000-0000', 'Hello, World!', 'Sender');
	 *
	 *
	 * @param string $call Call name in camelCase
	 * @param array $args Arguments to convert the API call array
	 * @return mixed Response to API call in accordance with returnKey and type NetelipSMS.call()
	 */
	public function __call( $call, $args )
	{
		$calls = $this->calls( );

		// call beautifer to accomply API methods
		$callName = preg_replace_callback( '#[A-Z]#', function( $a ) { return '/' . mb_convert_case( $a[0], MB_CASE_LOWER ); }, $call );

		if( ! isset( $calls[$callName] ) )
			throw new CException( 'Unknown call' );

		$params = $calls[$callName];

		$params['required'] = isset( $params['required'] ) ?(array) $params['required'] : array( );
		$params['optional'] = isset( $params['optional'] ) ?(array) $params['optional'] : array( );
		$params['fixed'] = isset( $params['fixed'] ) ? $params['fixed'] : array( );

		$args = $this->populate( $args, $params['required'], $params['optional'] );
		$args = array_merge( $args, $params['fixed'] );

		$response = $this->request( $callName, $args );

		if( $response['status'] == self::REQUEST_SUCCESS )
			if( isset( $params['returnKey'] ) )
				if( isset( $response[$params['returnKey']] ) )
				{
					if( isset( $params['returnType'] ) )
						if( ! settype( $response[$params['returnKey']], $params['returnType'] ) )
							throw new CException( 'Type is not supported' );

					return $response[$params['returnKey']];
				}
				else
					throw new CException( 'Key not found' );

		return $response['status'] == self::REQUEST_SUCCESS;
	}

	/*
	* Formation of an array of call parameters of the arguments
	* @ Param mixed $ args The arguments passed to the method call
	* @ Param array $ required array parameters required
	* @ Param array $ optional array of additional parameters
	* @ Return array An array of call parameters
	**/
	protected function populate( $args, $required, $optional )
	{
		$allParams = array_merge( $required, $optional );

		// Special case
		if( count( $args ) === 1 )
			// Check if the first argument is an array
			if( is_array( $args[0] ) )
				// ...And if all keys - line
				if( array_reduce( array_keys( $args[0] ), function( $prev, $curr ) { return $prev && is_string( $curr ); }, true ) )
				{
					foreach( $required as $requiredParam )
						if( ! isset( $args[0][$requiredParam] ) )
							throw new CException( 'It lacks some parameters' );

					return $args[0];
				}

		if( count( $args ) < count( $required ) )
			throw new CException( 'It lacks some required parameteres' );

		if( count( $args ) > count( $allParams ) )
			throw new CException( 'Too much parameteres' );

		$params = array( );

		foreach( $args as $id => $arg )
			$params[$allParams[$id]] = $arg;

		return $params;
	}

	/**
	 * API Call
	 * @param string $call name to call
	 * @param array $params call parameteres
	 * @return array server response
	 */
	protected function request( $call, array $params = array( ) )
	{
		$params = array_map( function( $val ) { return join( ',',(array) $val ); }, $params );
		$params = array_merge( $params, array( 'login' => $this->login, 'password' => $this->password ) );

		$url =( $this->useSSL ? 'https://' : 'http://' ) . $this->url . '/' . $call;
		$post = http_build_query( $params, '', '&' );
		$ch = curl_init( $url );

		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $post );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

		Yii::trace( 'calling: ' . $url . ':' . PHP_EOL . print_r( $params, true ), 'NetelipSMS' );

		$response = curl_exec( $ch );

		$this->responseCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

		switch($this->responseCode) { 
			case self::RESPONSE_CODE_SUCCESS:
				Yii::trace("Message sent succesfully");
				break;
			case self::RESPONSE_CODE_UNAUTHORIZED:
				Yii::trace("Unauthorized request");
				break;
			case self::RESPONSE_CODE_PAYMENT_REQUIRED:
				Yii::trace("Out of credit");
				break;
			case self::RESPONSE_CODE_PRECONDITION_FAILED:
				Yii::trace("Request malformed");
				break;
			case self::RESPONSE_CODE_PARAMETERS_ERROR:
				Yii::trace("Parameters error");
				break;
			case self::RESPONSE_CODE_REQUIRED_PARAMETERS_ERROR:
				Yii::trace("Required parameters missed");
				break;
		}

		curl_close( $ch );

		return $this->response = json_decode( $response, true );
	}

	/**
	 * Return the server response to the last request
	 * @return array
	 */
	public function getResponse( )
	{
		return $this->response;
	}

	/**
	 * Return the server response to the last request
	 * @return array
	 */
	public function getResponseCode( )
	{
		return $this->responseCode;
	}
}
