<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
    private $_id;
    private $_username;
    private $_utype;
	public function authenticate()
	{
        $users=array(
            // username => password
//			'demo'=>'demo',
//			'admin'=>'admin',
        );
//        if($this->utype=='1'){
//           $type='user';
//        }else{
//            $type='team';
//        }

        $utype =  Yii::app()->user->getState("usertype");

        $user = Login::model()->findByAttributes(array('username'=>$this->username,'password'=>$this->password,'utype'=>$utype));
        if($user!=null){
            $users[$user->username] = $user->password;
        }

        if(!isset($users[$this->username]))
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($users[$this->username]!==$this->password)
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else{
            $this->errorCode=self::ERROR_NONE;

            $this->_id = $user->id;
//            $this->_name = $user->name;
            $this->_username = $user->username;
            $this->_utype = $user->utype;
//            $auth = Yii::app()->authManager;
//            if (!$auth->isAssigned($user->role, $this->_id)) {
//                if ($auth->assign($user->role, $this->_id)) {
//                    Yii::app()->authManager->save();
//                }
//            }
            $workspace="";
            $workspace = Workspace::model()->findByAttributes(array('ownerid'=>$user->id));

//            if($workspace->rating==null)
//            {
//                $workspace=LoginWorkspace::model()->findByAttributes(array('loginid'=>$user->id));
//            }

            if($user->utype=="user"){
                $this->setState("userId",$user->userid);
                $this->setState("logId",$workspace->id);
                $this->setState("userlog",$user->id);
                $this->setState("username",$user->username);
                $this->setState("level",$user->perm);

            }elseif($user->utype=="team"){
                $team = Team::model()->findByPk($user->teamid);
                $this->setState("userId",$user->teamid);
                $this->setState("username",$user->username);
                $this->setState("logId",$team->wid);
                $this->setState("level",$user->perm);
                $this->setState("userlog",$user->id);

            }

        }
		return !$this->errorCode;
	}
}